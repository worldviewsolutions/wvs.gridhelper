# WVS.GridHelper

[TOC]

## Overview

![Diagram](diagram.png)

* WVS.GridHelper.Front.JQGrid.MVC / MVCCore
	* MVC: ```JQGridPartial.cshtml``` **→** ```RenderJQGrid``` via RazorGenerator
	* MVCCore: ```JQGridRender.cshtml``` **→** ```JQGridRenderViewComponent.Render``` via Microsoft.Extensions.FileProviders.Embedded
* WVS.GridHelper.Front.JQGrid
	* **```IJQGrid```** jqGrid viewmodel
	* ViewModels for columns, actions, pagingmodel..
* WVS.GridHelper.Front.JQGrid.Static
	* ```jqGridRender.ts``` **→** ```jqGridRender.js```
	* ```_Models.tst``` renders classes with ```[TypeScript]``` attribute
	* ```JQGrid.css```
* WVS.GridHelper
	* **```IGrid```** grid model
	* **```GridBuild<T>()```** configuration, columns, format.
	* ```LinqFilter``` encapsulates most filtering.
		* ```Filter```
		* ```FilterList```
		* ```TypeFilterer.Filter()``` Transform certain Filter target into LINQ expression.
		* ```IQueryable<T>```**```.ApplyFilters()```** Transform FilterList into single LINQ expression.
	* **```Project()```** Select anonymous type with grid data.
	* **```ExpressionHelpers```** Lots of of expression wrangling helpers.
	* ```PagingModel```
* WVS.GridHelper.Back.EF6 / EFCore
	* **```GetGridData()```**
		* EF-specific IQueryable construction with AsNoTracking(), CountAsync(), ToListAsync()
	* ```LinqFilterEF6```
		* EF-specific filtering, such as using DbObject.TruncateTime for datetime comparison by date.
		* EF Core 2.0 doesn't yet support EF.Functions.TruncateTime

## Demo

Run ```Demo``` or ```DemoCore``` projects to get a sandbox / integration test of a grid.

## Installation

#### .NET Standard 1.3+

* WVS.GridHelper
* WVS.GridHelper.Front.JQGrid

#### .NET Framework 4.6+

* WVS.GridHelper.Front.JQGrid.MVC
* WVS.GridHelper.Back.EF6 (optional)

#### .NET Core 2.0+

* WVS.GridHelper.Front.JQGrid.MVCCore
* WVS.GridHelper.Back.EFCore (optional)

## Implementation

#### Step 1: Configure grid

###### Define grid (Controller)
```csharp
/*
	use [JQ]GridBuild to define grid's content.
	declare grid configuration factory around <T>
	<T> is the IQueryable<T> entity you are displaying, filtering against, and querying.
	<T> can be root EF table/view.
	<T> can be POCO if using custom query projection, AutoMapper.ProjectTo projection, or using in-memory source.
*/
internal static IJQGrid ManureImportedGridFactory() => new JQGridBuild<ManureImported>()
	// always include a primary key.
	.KeyCol(x => x.Id) // same as .Col(x => x.ID, new ColumnMetadata { IsKey = true, Visible = false })
	// include columns in order.
	.Col(x => x.TypeOfStructure) // automatically sets title to "Type Of Structure", names are spaced out if no title is defined.
	.Col(x => x.BMPInstance.Practice.Code) // nest into entities to create joins, only selected columns can be filtered/queried.
	.Col(x => x.ManureConsistency.Name, "Consistency") // shortcut to define column title inline.
	.Col(x => x.ManureConsistency.ManureConsistencyUnitOfMeasure.UnitOfMeasure.PluralName, "UoM") // nest as deep as you want into the graph. null paths will return null, for filtering and projection.
	.Col(x => x.TestDate) // datetimes are automatically formatted.
	.Col(x => x.DueBy, "Planned Date", new ColumnMetadata {Width = 140}) // to set fixed width by pixels.
	.Col(x => x.NH4N, "NH₄-N", new ColumnMetadata { DecimalPlaces = 2 }) // truncate to 2 decimal places.
	// hidden columns can be added via column picker.
	.Col(x => x.P2O5, "P₂O₅", new ColumnMetadata { DecimalPlaces = 2, Visible = false })
	.Col(x => x.K2O, "K₂O", new ColumnMetadata { DecimalPlaces = 2, Visible = false })
	.Build(); // return a grid configuration instance.
```

###### ViewModel creation (Controller)
```csharp
public ActionResult MyControllerAction()
{
	var grid = ManureImportedGridFactory(); // get grid configuration.
	return PartialView(new MyViewModel { Grid = grid });
}
```

###### ViewModel configuration (Controller or Razor)
```csharp
	// specify POST endpoint that serves up [JQ]PagingModel.
	Model.Grid.DataUrl = Url.Action("ManureImportedGridData", new {planId = Model.Plan_ID});

	// specify containing div html-id to match width.
	Model.Grid.HtmlIdContainer = "myGridDivId";

	// actions show up as icons in a column on the right.
	Model.Grid.Actions = new List<GridAction>
	{
		GridAction.Delete("someUniqueNamespace.deleteRow"), // static shortcut for defining delete action with confirmation dialog.
		new GridAction { /* or get fancy with custom actions */ },
	};

	// action-columns show up as separate columns with a link to a javascript callback.
	Model.Grid.ActionColumns = new List<GridColumnAction>
	{
		new GridActionColumn {
			Title = "Link And Column Title",
			JavascriptCallback = "nameOfCallbackFunction", // callback will get rowId as parameter.
		},
	};

	// callback fired with id of row clicked.
	Model.Grid.OnRowSelect = "someUniqueNamespace.onRowSelect"; // callback will get rowId as parameter.
```

#### Step 2: Render grid

###### View rendering (Razor)
```html
<!-- use a div around the grid to control size. -->
<div id="myGridDivId">
	<!-- render custom filters here, if any. -->
	<!-- render the grid. pass instance of the ViewContext (this). -->
	@Model.myGrid.Render(this)
</div>
```
~passing 'this' is an artifact of using RazorGenerator, passing ViewContext to the static helper function~

No script is needed to initialize the grid.

#### Step 2.5: Script custom grid behavior

###### View scripting (JavaScript)

Access ```$grid``` in script by reading properties of IJQGrid thru Razor.

```html
<!-- using inline script -->
<script>
	$(function(){
		var $grid = $('#@Model.Grid.HtmlIdGrid');
		$grid.jqGrid(custom jqgrid manipulation stuff);
	})
</script>
```
or
```html
<!-- using external .js file -->
<script>
	$(function(){
		var gridId = '@Model.Grid.HtmlIdGrid';
		someUniqueNamespace.init(gridId);
	});
</script>
```
```js
//# sourceURL=some.script.file.js
var someUniqueNamespace = (function () {
	var $grid;
	function init(gridId) {
		$grid = $('#' + gridId);
	}
	function doStuff() {
		$grid.jqGrid(/*stuff*/);
	}
	return {
		init: init
		, onRowSelect: onRowSelect
		, deleteRow: deleteRow
		, doStuff: doStuff
	};
}());
```

#### Step 3: Get grid data

###### ```GetGridData()``` (Controller or Manager or DAL)

```csharp
[HttpPost]
public async Task<JsonResult> MyGridDataAction(int rows = 25, int page = 1, string sidx = null, string sord = "asc")
{
	var orderBy = string.IsNullOrEmpty(sidx)
		? nameof(MyEntity.ID) // default sort.
		: sidx;
	var orderByAscending = sord.ToLower().StartsWith("asc");

	// get grid configuration.
	IJQGrid grid = MyGridFactory();

	// get grid data query.
	IQueryable dataQuery = new MyManager()
		.GetDataQuery(); // manager can inject security filters.

	// hit database to query, filter, count, project.
	var data = await dataQuery
		.GetGridData(grid, sidx, orderByAscending, rows, page/*, filters*/);

	// convert to JQGrid model.
	var jqGridModel = data.ToJQGridFormat(grid); // pass grid configuration.

	// render to JSON.
	return Json(jqGridModel);
}
```

or use GetGridDataJson helper to query, filter, count, project, and json all in one.

```csharp
[HttpPost]
public async Task<JsonResult> MyGridDataAction(int rows = 25, int page = 1, string sidx = null, string sord = "asc")
{
	var orderBy = string.IsNullOrEmpty(sidx)
		? nameof(MyEntity.ID)
		: sidx;
	var orderByAscending = sord.ToLower().StartsWith("asc");
	var grid = MyGridFactory();

	// query, filter, count, project, json.
	return await dbContext.MyEntity
		.GetGridDataJson(grid, orderBy, orderByAscending, rows, page/*, filters*/);
}
```

## Developing GridHelper

#### Spec / Version

All NuGet package information is taken from a project's ```Properties/AssemblyInfo.cs``` (Framework) and/or the csproj.

Versions are shared across all libraries in the solution, so change to one will propogate new versions for all affected nugets.

Version is calculated by gitversion and handled automatically by the CI.

#### Publishing Test Builds / Updates

WVS-0009-PC.wvs has a TeamCity build/nuget/symbol server that builds and locally publishes any branches pushed to the WVS.GridHelper repository.

Just add this server as a package source, push a branch, and you can install your build as a pre-release package.

Push master and tag it to generate a point release package that you can move to the WVS server.

To push to WVS server do this: ```nuget push .\WVS.filename.of.nupkg -Source http://nuget.wvs/WVSNuGetServer/ topSecretNugetServerPassword!``` ([Password is in the WV wiki](https://worldviewsolutions.atlassian.net/wiki/spaces/WIKI/pages/1572896/Nuget))

#### TypeScript / Typewriter

```Front.JQGrid.Static``` generates and contains static js/css resources for the viewmodel libraries to consume.

```Content\JQGrid.css``` is style included with the grid render.

```jqGridRender.ts``` is the view code that instantiates and manages the jqGrid.

Typings are auto-generated from any class in any project with a [TypeScript] attribute attached.

To update typings, install the [TypeWriter VS Extension](https://marketplace.visualstudio.com/items?itemName=frhagn.Typewriter). Typings templated is in ```TypeScript\Models\_Models.tst```. Select this and ```Run Custom Tool``` to generate changes and/or enable automatic generation.

#### Using RazorGenerator

Front.JQGrid.MVC.View uses RazorGenerator to pre-compile the JQGridPartial.cshtml template into the assembly instead of at runtime.

Download [Razor Generator VS Extension](https://marketplace.visualstudio.com/items?itemName=DavidEbbo.RazorGenerator) (not nuget) to update pre-compiled MVC.

There are some quirks with pre-compiling, like broken intellisense and no lambda expression support. Do any serious processing in the Front.JQGrid library anyway to keep the MVC template lean.

## The Problem (before)

* copypasta grids.
* duplicated configuration:
	* specify columns multiple times across js/c# files.
	* difficult to make changes.
	* not strongly typed, uses magic strings and untyped javascript.
	* breaks on refactoring.
* specify columns multiple places:
	* colNames
	* colModel
	* filter columns (still a problem)
	* filter logic
	* select from database
	* project to jqGrid form
* inefficient queries
	* ToList, AsEnumerable.. query everything and sort it out in memory.
	* querying all columns when only using a few (including heavy SQLGeometry/Geography/blob data).
	* using AutoMapper.Map instead of .ProjectTo (needs v4+).
* repeated and inconsistent sort/page/count/select patterns.
* copypasta filter logic, unique for each grid and column. filtering code only produced queries for SQL/Dapper.

## Goals

* consolidate configuration. eliminate duplication of configuration. make it easy to use and change any grid.
* eliminate need for copypasta. consolidate functionality effort; adding features should benefit all implimentations.
* consolidate filter logic
	* filter by Expressions. for EF6, EF Core, NHibernate 3+
	* filter by SQL for Dapper/other is possible, not imported from DCR.
* allow targeting different grid rendering with the same engine:
	* mvc jqGrid.
	* angular grid.
	* export to xlsx.
