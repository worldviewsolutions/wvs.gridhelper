﻿using System;
using System.Web.Mvc;
using System.Web.WebPages;

namespace WVS.GridHelper.Front.JQGrid.MVC
{
    using JQGrid;
    using Static;

    public static class JQGridRender {
        /// <summary>
        /// Render JQGrid to MVC
        /// </summary>
        /// <param name="gridConfiguration">Grid configuration.</param>
        /// <param name="pageContext">Pass in the render context ('this').</param>
        /// <param name="renderScript">Render required static initialization scripts, inline with the grid component. (Does not render jqGrid or other dependencies).</param>
        /// <param name="renderStyles">Render required static css styles, inline with the grid component.</param>
        /// <returns><see cref="System.Web.IHtmlString"/></returns>
        /// <exception cref="ArgumentNullException"><paramref name="gridConfiguration"/> is <see langword="null"/></exception>
        public static HelperResult RenderJQGrid(IJQGrid gridConfiguration, WebViewPage pageContext, bool renderScript = true, bool renderStyles = true)
        {
            if (null == gridConfiguration)
                throw new ArgumentNullException(nameof(gridConfiguration));
            if (null == pageContext)
                throw new ArgumentNullException(nameof(pageContext));

            var css = renderStyles ? RawResource.GetCSS() : null;
            var js = renderScript ? RawResource.GetJavascript() : null;

            // call the static pre-generated helper code, pass in page context and resources
            var v = new Views.Shared.JQGridPartial_ { ViewContext = pageContext.ViewContext };
            return v.RenderJQGrid(pageContext, gridConfiguration, css, js);
        }

        [Obsolete("Prefer JQGridRender.RenderJQGrid(grid, this);")]
        public static HelperResult Render(this IJQGrid gridConfiguration, WebViewPage pageContext) =>
            RenderJQGrid(gridConfiguration, pageContext);
    }
}
