﻿using System.Reflection;
using System.Runtime.CompilerServices;
using System.Runtime.InteropServices;

// General Information about an assembly is controlled through the following
// set of attributes. Change these attribute values to modify the information
// associated with an assembly.
[assembly: AssemblyTitle("WVS.GridHelper.Front.JQGrid.MVC")]
[assembly: AssemblyDescription("Render")]
[assembly: AssemblyConfiguration("")]
[assembly: AssemblyCompany("WorldView Solutions")]
[assembly: AssemblyProduct("WVS.GridHelper.Front.JQGrid.MVC")]
[assembly: AssemblyCopyright("Copyright ©  2017")]
[assembly: AssemblyTrademark("")]
[assembly: AssemblyCulture("")]

// Setting ComVisible to false makes the types in this assembly not visible
// to COM components.  If you need to access a type in this assembly from
// COM, set the ComVisible attribute to true on that type.
[assembly: ComVisible(false)]

// The following GUID is for the ID of the typelib if this project is exposed to COM
[assembly: Guid("9f532167-19cc-4990-b9a8-7c8587f50eda")]

// Set via GitVersion during build.
[assembly: AssemblyVersion("0.0.0.0")]
[assembly: AssemblyFileVersion("0.0.0.0")]
