﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Diagnostics.CodeAnalysis;
using System.Linq;
using System.Net;
using System.Runtime.InteropServices.ComTypes;
using System.Threading.Tasks;
using System.Web.Mvc;
using Demo.Models;
using DemoShared.DAL;
using WVS.GridHelper;
using WVS.GridHelper.Back.EF6;
using WVS.GridHelper.Front.JQGrid;
using WVS.GridHelper.Front.JQGrid.MVC;
using WVS.GridHelper.Interfaces;

namespace Demo.Controllers
{
    [RoutePrefix("Demo")]
    public class DemoController : Controller
    {
        public DemoController(TestDAL effortDbContext, IList<TestEntity> listDb) {
            EffortDb = effortDbContext;
            ListDb = listDb;
        }

        public ActionResult Index(int flavor = (int)JQGridFlavor.DCRJQGrid, BackendFlavor backend = BackendFlavor.List) {
            var isLocal = backend == BackendFlavor.Local;
            const bool useSubGrids = true;

            var grid = GridFactory();
            if (!isLocal) {
                grid.DataUrl = Url.Action("GridData", new { backend });
                grid.DataMethod = DataMethodType.Post;
            }

            string subGridUrl = null;
            if (useSubGrids) {
                const int testParameter = 123;
                grid.HasSubGrid = true;
                subGridUrl = Url.Action("CreateSubGrid", new { backend, testParameter });
            }

            string editRowSaveUrl = Url.Action("SaveEditRow");

            var vm = new DemoViewModel {
                Grid = grid,
                Flavor = flavor,
                Backend = (int) backend,
                componentGridUrl = subGridUrl,
                editRowSaveUrl = editRowSaveUrl,
            };
            return View(vm);
        }

        public PartialViewResult CreateSubGrid(BackendFlavor backend, int testParameter)
        {
            return PartialView("SubGridPartial");
        }

        [HttpPost]
        public async Task<ActionResult> SaveEditRow(TestEntityUpdateModel model) {
            // note jqGrid will send only the properties that are editable.
            // other properties get default values and should not be used to update the entity.

            TestEntity entity = null;
            var flavor = model.backendFlavor;
            switch (flavor) {
                case BackendFlavor.Effort:
                    entity = await EffortDb.TestEntity.FindAsync(model.Id);
                    if (entity == null)
                        return HttpNotFound();

                    entity.String = model.String;
                    entity.Integer = model.Integer;
                    await EffortDb.SaveChangesAsync();
                    break;

                case BackendFlavor.List:
                    entity = ListDb.FirstOrDefault(x => x.Id == model.Id);
                    if (entity == null)
                        return HttpNotFound();
                    entity.String = model.String;
                    entity.Integer = model.Integer;
                    break;

                default:
                    throw new InvalidOperationException($"Invalid backend: {flavor}");
            }

            return Json(entity);
        }

        [SuppressMessage("ReSharper", "InconsistentNaming")]
        [SuppressMessage("ReSharper", "UnusedMember.Global")]
        public class TestEntityUpdateModel : TestEntity {
            public int rowId { get; set; }
            public BackendFlavor backendFlavor { get; set; }
        }

        [AcceptVerbs(HttpVerbs.Get | HttpVerbs.Post)]
        public async Task<ActionResult> GridData(
            BackendFlavor backend,
            string sidx, string sord, int? rows, int? page,
            string searchField = null, string searchString = null, string searchOper = null) {

            var (orderBy, orderAsc, pageNum) = JQGridFilters.TransformParams(sidx, sord, page, nameof(TestEntity.Id));
            var grid = GridFactory();

            var filter = JQGridFilters.TransformFilter(searchField, searchOper, searchString);
            var filters = new FilterList(FilterListType.And);
            if (null != filter)
                filters.Add(filter);

            IQueryable<TestEntity> query;

            switch (backend) {
                case BackendFlavor.Effort:
                    query = EffortDb.TestEntity;
                    break;
                case BackendFlavor.List:
                    query = ListDb.AsQueryable();
                    break;
                default:
                    throw new NotImplementedException();
            }

            var pagingModel = await query.GetGridData(grid, orderBy, orderAsc, rows ?? 25, pageNum, filters);
            return pagingModel.ToJQGridFormatJSONResult(grid);
        }

        public async Task<ActionResult> SubGridData(
           BackendFlavor backend, int testParameter, int Id,
           string sidx, string sord, int? rows, int? page,
           string searchField = null, string searchString = null, string searchOper = null)
        {

            var (orderBy, orderAsc, pageNum) = JQGridFilters.TransformParams(sidx, sord, page, nameof(TestEntity.Id));
            var grid = GridFactory();

            var filter = JQGridFilters.TransformFilter(searchField, searchOper, searchString);
            var filters = new FilterList(FilterListType.And);
            if (null != filter)
                filters.Add(filter);

            IQueryable<TestEntity> query;

            switch (backend)
            {
                case BackendFlavor.Effort:
                    query = EffortDb.TestEntity;
                    break;
                case BackendFlavor.List:
                    query = ListDb.AsQueryable();
                    break;
                default:
                    throw new NotImplementedException();
            }

            var pagingModel = await query.GetGridData(grid, orderBy, orderAsc, rows ?? 25, pageNum, filters);
            return pagingModel.ToJQGridFormatJSONResult(grid);
        }

        public enum JQGridFlavor
        {
            FreeJQGgrid,
            DCRJQGrid
        }

        public enum BackendFlavor {
            List,
            Effort,
            Local,
        }

        public IJQGrid GridFactory() => TestEntity.GridConfiguration;

        /*
        private static TestDAL EffortDb {
            get {
                if (_effortDb == null)
                    _effortDb = TestDAL.TestDb;
                _effortDb.InitTestData();
                return _effortDb;
            }
        }
        */

        private TestDAL EffortDb { get; }
        private IList<TestEntity> ListDb { get; }
    }
}
