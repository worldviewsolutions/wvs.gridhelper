﻿using WVS.GridHelper.Front.JQGrid;

namespace Demo.Models
{
    public class DemoViewModel
    {
        public IJQGrid Grid;
        public IJQGrid subGrid;
        public int Flavor;
        public int Backend;
        public string componentGridUrl;
        public string editRowSaveUrl;
    }
}
