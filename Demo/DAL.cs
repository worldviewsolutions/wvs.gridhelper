﻿using System;
using System.Collections.Generic;
using System.Data.Common;
using System.Data.Entity;
using System.Linq;
using System.Web;
using DemoShared.DAL;
using Effort.DataLoaders;

namespace Demo
{
    public class TestDAL : DAL
    {
        public TestDAL(DbConnection connection)
            : base(connection) {
        }

        public void InitTestData()
        {
            TestEntity.AddRange(TestEntityFactory.TestDataFactory());
            SaveChanges();
        }

        public static TestDAL TestDb => new TestDAL(Effort.DbConnectionFactory.CreatePersistent(instanceId));
        const string instanceId = "ThisIsTheInstanceName";
    }

    public class DAL : DbContext
    {
        public DbSet<TestEntity> TestEntity { get; set; }

        public DAL(DbConnection connection)
            : base(connection, true)
        {
        }
    }
}
