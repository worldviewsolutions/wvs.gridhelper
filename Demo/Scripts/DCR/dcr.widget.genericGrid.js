﻿/*

This file is intended to be a multi-purpose wrapper for the jqgrid with
common functionality built in such as adding buttons and urls.  It is not intended
to replace jqgrid for specific implementations, but can be for general purpose grids.
Many of jQuery's properties have been hard coded for simplicity of use.

built-in
* The pager is automatically generated on init.
* The HTTP Method is post
* The data type is JSON

options:
	url     : the URL used to get the grid data  Either this or data is required.
	data    : an array of data passed to the grid.  Either this or url is required.
	columns : an object with colNames and colModel properties or a function that 
			returns an object with colNames and colModel properties.  The function has no arguments.
	subgrid : the url or function used to load the subgrid. The function has two arguments: the first is the 
			row's data, the second is the subgrid dom object. If undefined, the subgrid option will not show up.
	buttons : either jQuery object, a string, or a function that returns a jQuery object or string.  
			The function has one argument of the row's data. If undefined, the buttons will not show up.
	onEmpty: a text string or a function that returns a text string that displays in the grid when there are no results	

other options (passed through from underlying jqgrid):
	width   : the width of the page in pixels, defaults to 950
	mtype  : the method of the request, defaults to "post"
	caption : the jqgrid caption, optional, no default.
	postData: data sent to the web service as post data, optional, no default.
	datatype: the data type returned by the server, defaults to "json"
	sortname: the default sort column
	sortorder: the default sort order
    loadComplete: event fires after the grid is loaded 
    
*/
(function ($) {
    $.widget("dcr.genericGrid", {
        options: {
            altRows: false,
            matchWidth: null,
            //buttons: undefined,
            //columns: undefined,
            //jsonReader: { repeatitems: false },
            datatype: 'json',
            mtype: "POST",
            rowNum: 25,
            rowList: [25, 50, 100],
            multiselect: false,
            viewrecords: true,
            width: 950,
            height: "100%",
            onEmpty: "No records to view",
            subGridOptions: {
                plusicon: "ui-icon-triangle-1-e",
                minusicon: "ui-icon-triangle-1-s",
                openicon: "ui-icon-arrowreturn-1-e"
            },
            subGridLocalData: null,
            subGridColNames: null,
            subGridColModel: null,
            subGridOnSelectRow: null,
            subGridRowExpanded: undefined

        },
        _getReloadableGridOptions: function () {
            return {
                url: $.isFunction(this.options.url) ? this.options.url(this) : this.options.url,
                postData: $.isFunction(this.options.postData) ? this.options.postData(this) : this.options.postData
            };
        }, 
        _getGridOptions: function () {
            var self = this;
            // extend options into a new options obect, and set up proxy methods for certain events
            var gridOptions = {
                subGrid: typeof this.options.subGrid === "boolean" ? this.options.subGrid : typeof this.options.subGrid != "undefined",
                pager: this.options.pager == false ? undefined : this.options.pager,
                beforeSelectRow: function (rowid, e) { return self._beforeSelectRow(rowid, e); },
                afterInsertRow: function (rowid, rowdata, rowelem) { return self._afterInsertRow(rowid, rowdata, rowelem); },
                loadError: function (xhr, status, error) { return self._loadError(xhr, status, error); },
                loadComplete: function (data) { return self._loadComplete(data); },
                subGridRowExpanded: this.options.subGridRowExpanded !== undefined
                    ? this.options.subGridRowExpanded
                    : function (subgrid_id, row_id) { return self._subGridRowExpanded(subgrid_id, row_id); },
                onSelectRow: function (rowid, status, e) { return self._onSelectRow(rowid, status, e); }
            };
            return gridOptions;
        },
        _getColumnOptionsAsync: function () {
            var self = this;
            var columnOptions = {};

            var dfd = $.Deferred();

            // if the colmodel and colnames is coming through the columns option, add it afterward
            if (this.options.columns) {
                var columnsDfd;
                if (typeof this.options.columns === "string") {
                    columnsDfd = $.ajax({
                        url: this.options.columns,
                        type: "get",
                        dataType: "json"
                    });
                } else if ($.isFunction(this.options.columns)) {
                    columnsDfd = $.when(this.options.columns(this));
                } else {
                    columnsDfd = $.when(this.options.columns);
                }
                columnsDfd.done(function (columns) {
                    // work the columns into jqGrid colNames and colModel
                    if (columns.colModel && columns.colNames) {
                        columnOptions.colNames = columns.colNames;
                        columnOptions.colModel = columns.colModel;
                    } else {
                        gridOptions.colNames = $.map(columns, function (item) {
                            return item.displayName || item.name;
                        });
                        columnOptions.colModel = columns;
                    }

                    dfd.resolve(columnOptions);
                });
            } else {
                dfd.resolve(columnOptions);
            }

            dfd.done(function () {
                // if there are buttons but no colModel for it, add one
                if (self.options.buttons && !columnOptions.colModel.any(function () {
                    return this.name == "buttons";
                })) {
                    columnOptions.colModel.push({ name: "buttons", sortable: false });
                    columnOptions.colNames.push("Actions");
                }
            });

            return dfd;
        },
        _init: function () {
            // let's set up an alias for when callbacks are in a different scope
            var self = this;

            // columns can load asyncronously, so load them in a deferred and merge them into the grid options
            this._getColumnOptionsAsync().done(function (columnOptions) {

                var gridSelector = "#" + $(self.element).attr("id");

                // unload must happen first
                $(gridSelector).jqGrid("GridUnload");

                // if the pager doesn't exist, create it.
                if (self.options.pager == undefined) {
                    var pagerID = self._createID();
                    var pager = $("<div ></div>").attr("id", pagerID);
                    $(gridSelector).after(pager);
                    self.options.pager = "#" + pagerID;
                }
                var gridOptions = $.extend({},
                    self.options,
                    self._getGridOptions(),
                    self._getReloadableGridOptions(),
                    columnOptions);

                // create the grid
                self.element = $(gridSelector).jqGrid(gridOptions).addClass("isGenericGrid");

                $(gridSelector).jqGrid('navGrid', '#' + self.options.pager.attr('id'), {
                    add: false,
                    edit: false,
                    del: false,
                    search: false,
                    refresh: false
                });

                // add the column chooser button
                $(gridSelector).jqGrid('navButtonAdd', '#' + self.options.pager.attr('id'), {
                    caption: "Columns",
                    title: "Reorder Columns",
                    onClickButton: function () {
                        //alert('test');
                        $(gridSelector).jqGrid('columnChooser', {
                            done: function (perm) {
                                if (perm) {
                                    //reorder columns manually since we're overiding the done event..
                                    $(gridSelector).jqGrid("remapColumns", perm, true);
                                    //resize the grid....
                                    $(gridSelector).setGridWidth(self.options.matchWidth.width() - 0.5);
                                }
                            }
                        });
                    }
                });

                var grid = $(gridSelector);
                var gridID = grid[0].id;
                if (self.options.exportUrl) {
                    var exportHTML = '<form id="' + gridID + '_form" action="' + self.options.exportUrl + '" method="post"><input type="hidden" name="export" value="true" /><div class="editor-block"><div class="editor-label"><label for="format">Output Format:</label></div><div class="editor-field"><select id="format" name="format" class="form-control"><option value="excel">Excel</option><option value="PDF">PDF</option></select></div></div>';
                    exportHTML += '<div class="editor-block"><div class="editor-label"><label for="' + gridID + '_include">Included Columns:</label></div><div class="editor-field"><select id="' + gridID + '_include" name="' + gridID + '_include" class="form-control"><option value="all">All</option><option value="visible">Visible</option></select></div></div></form>';
                    // add the export button
                    $(gridSelector).jqGrid('navButtonAdd', self.options.pager, {
                        caption: "Export",
                        title: "Export Results",
                        onClickButton: function () {
                            bootbox.confirm(exportHTML, function () {
                                var include = $('#' + gridID + '_include').val();

                                var colModel = grid.jqGrid('getGridParam', 'colModel');
                                // get the column names of either ALL or VISIBLE columns
                                var colNames = $.grep(colModel, function (n, i) {
                                    return include == 'all' || !n.hidden;
                                })
                                    .map(function (x) {
                                        return x.name;
                                    });

                                var form = $('#' + gridID + '_form');
                                $.each(colNames, function (i, o) {
                                    $('<input>').attr({
                                        type: 'hidden',
                                        name: "columns",
                                        value: o
                                    }).appendTo(form);
                                });

                                var postData = grid.jqGrid('getGridParam', 'postData');

                                // Try to parse..could be stringified at this point based on user actions
                                try {
                                    postData = colNames.parse(postData);
                                } catch (err) { }

                                for (var key in postData) {
                                    $('<input>').attr({
                                        type: 'hidden',
                                        name: key,
                                        value: ($.isPlainObject(postData[key]) ? JSON.stringify(postData[key]) : postData[key])
                                    }).appendTo(form);
                                }
                                form.submit();
                            });
                        }
                    });
                }

                //Resize Grid
                $(window).bind('resize', function () {
                    if (self.options.matchWidth.is(":visible")) {
                        //console.log("generic grid resize", self.options.matchWidth)
                        $(gridSelector).setGridWidth(self.options.matchWidth.width() - 0.5);

                        // allow header to wrap
                        // get the header row which contains
                        var headerRow = $(gridSelector).closest("div.ui-jqgrid-view")
                            .find("table.ui-jqgrid-htable>thead>tr.ui-jqgrid-labels");

                        // increase the height of the resizing span
                        var resizeSpanHeight = 'height: ' + headerRow.height() + 'px !important; cursor: col-resize;';
                        headerRow.find("span.ui-jqgrid-resize").each(function () {
                            this.style.cssText = resizeSpanHeight;
                        });

                        // set position of the div with the column header text to the middle
                        var rowHight = headerRow.height();
                        headerRow.find("div.ui-jqgrid-sortable").each(function () {
                            var ts = $(this);
                            ts.css('top', (rowHight - ts.outerHeight()) / 2 + 'px');
                        });
                    }

                }).trigger('resize');
            });
        },
        _getWidget: function () {
            if (self.widget == undefined)
                self.widget = $(this.element).parents(".ui-widget.ui-jqgrid:first");
            return self.widget;
        },
        _createID: function (prefix) {
            var id = prefix || "";
            for (var i = 0; i < 10; i++)
                id += String.fromCharCode(Math.round(Math.random() * 25) + 65)
            return id;
        },
        _invoke: function (method, args) {
            if (method && $.isFunction(method)) {
                args.push(this); // add the widget as the final argument
                return method.apply(this.element, args);
            } else
                return false;
        },
        _afterInsertRow: function (rowid, rowdata, rowelem) {

            // add buttons
            if (this.options.buttons) {
                var buttons = this.options.buttons;
                // get the cell for the buttons
                var cell = $("#" + rowid + " [aria-describedby$=buttons]", this.element);
                // determine what type of content the buttons are coming as, and add them to the cell
                if (typeof buttons == "string") {
                    cell.html(buttons);
                } else if ($.isFunction(buttons)) {
                    var buttonToAdd = buttons.apply(this.element, [rowid, rowdata, rowelem]);
                    if (isJquery(buttonToAdd))
                        cell.empty().append(buttonToAdd);
                    else
                        cell.html(buttonToAdd);
                } else if (isJquery(buttons)) {
                    cell.append(buttons);
                } else if ($.isPlainObject(buttons)) {
                    for (n in buttons) {
                        $("<button type='button'>" + n + "</button>")
                            .button()
                            .on("click", {
                                name: n,
                                rowelem: rowelem,
                                rowid: rowid,
                                rowdata: rowdata
                            }, function (event) {
                                buttons[event.data.name](event, event.data.rowid, event.data.rowdata, event.data.rowelem);
                            }).appendTo(cell);
                    }
                }
            }
            this._invoke(this.options.afterInsertRow, [rowid, rowdata, rowelem]);
        },
        _onSelectRow: function (rowid, status, e) {
            if (e.type != 'contextmenu')
                this._invoke(this.options.onSelectRow, [rowid, status, e]);
        },
        _beforeSelectRow: function (rowid, e) {
            var $this = $(this.element);

            // Prevent row selection behavior for the "No records to view" message row
            if (rowid == null || rowid == "") return false;

            if (!e.ctrlKey && !e.shiftKey) {
                $this.resetSelection();
            } else if (e.shiftKey) {
                var initialRowSelect = $this.jqGrid('getGridParam', 'selarrrow')[0];
                $this.resetSelection();
                $this.setSelection(initialRowSelect, false);
                var CurrentSelectIndex = $this.getInd(rowid, false);
                var InitialSelectIndex = $this.getInd(initialRowSelect, false);
                var startID = "";
                var endID = "";
                if (CurrentSelectIndex > InitialSelectIndex) {
                    startID = initialRowSelect; endID = rowid;
                } else {
                    startID = rowid; endID = initialRowSelect;
                }
                var AllRows = $this.getDataIDs();
                var SelectRow = false;
                for (var i = 0; i < AllRows.length; i++) {
                    if (SelectRow == true && AllRows[i] != endID) {
                        $this.setSelection(AllRows[i], false);
                    } else if (AllRows[i] == startID) {
                        SelectRow = true;
                    } else if (AllRows[i] == endID) {
                        SelectRow = false;
                    }
                }
            }
            if (this.options.beforeSelectRow && $.isFunction(this.options.beforeSelectRow)) {
                return this._invoke(this.options.beforeSelectRow, [rowid, e]);
            }
            return true;
        },
        _loadComplete: function (data) {
            var $this = $(this.element);
            var self = this;

            // remove "empty" row
            $(".onEmpty", $this).parent().remove();

            this._disableSelection($this);

            if (!data || !data.records)
                this._fireOnEmpty();

            if (this.options.row)
                $this.setSelection(options.row);

            var rowid = $("body").data("generic-grid-reload-rowid");

            if (rowid && rowid.length) {
                for (var i = 0; i < rowid.length; i++) {
                    $this.setSelection(parseInt(rowid[i]));
                }
            }
            $("body").removeData("generic-grid-reload-rowid");

            if (this.options.multiselect && this.options.hideCheckbox) {
                var hideCheckbox = this.options.hideCheckbox;
                if ($.isFunction(hideCheckbox)) {
                    hideCheckbox = hideCheckbox();
                }
                if (hideCheckbox) {
                    $this.hideCol('cb') // hide the checkbox colums.
                }
            }

            $(".ui-jqgrid-ftable td,.ui-jqgrid tr.jqgfirstrow td,.ui-jqgrid .ui-jqgrid-htable th", this._getWidget()).each(function () {
                var self = $(this);
                var w = self.width();
                self.width(Math.max(0, w - 1));
            });

            this._invoke(this.options.loadComplete, [data]);

            // Context Menu
            if (self.options.hasOwnProperty('newTab') && self.options.newTab) {
                $.contextMenu({
                    selector: '#grid tr.jqgrow'
                    , callback: function (key, options) {
                        var m = "clicked: " + key;
                        window.console && console.log(m) || alert(m);
                    }
                    , items: {
                        "newTab": {
                            name: 'Open in new tab'
                            , callback: function (key, opt) {
                                var url = self.options.detailsUrl + opt.$trigger.attr('id');
                                var win = window.open(url, '_blank');
                            }
                        }
                    }
                });
            }



        },
        _loadError: function (xhr, status, error) {
            // try to invoke loadError, otherwise see if we can raise the generic error
            if (!this._invoke(this.options.loadError) && typeof onAjaxError != "undefined")
                window.onAjaxError(xhr);
            // the grid will be empty, display an error message.
            this._fireOnEmpty();
        },
        _subGridRowExpanded: function (subgrid_id, row_id) {
            //if there is subGridData it is local and had to be used as a new grid.            
            if (this.options.subGridLocalData) {
                var subgridTableId = subgrid_id + "_t";
                $("#" + subgrid_id).html("<table id='" + subgridTableId + "'></table>");
                $("#" + subgridTableId).jqGrid({
                    datatype: 'local',
                    data: this.options.subGridLocalData[row_id],
                    colNames: this.options.subGridColNames,
                    colModel: this.options.subGridColModel,
                    height: "100%",
                    width: 650,
                    onSelectRow: this.options.subGridOnSelectRow
                });
                //       $("#" + subgridTableId).closest("div.ui-jqgrid-view")
                //.children("div.ui-jqgrid-hdiv")
                //.hide();
            } else {
                var rowData = $(this.element).jqGrid("getRowData", row_id);
                var subgrid = this.options.subGrid;
                var subgridJq = $('#' + subgrid_id, this.element);
                subgridJq.empty().append('<div style="hight:200px; text-align:center; background-color:#E1E1E1;"><img src="' + page.applyRoot("/Content/images/ajax-loader.gif") + '" alt="Loading" /></div>');
                if ($.isFunction(subgrid)) {
                    var subgridResult = subgrid(this, subgrid_id, row_id);
                    if (isJquery(subgridResult))
                        subgridJq.empty().append(subgridResult);
                    else if (typeof subgridResult == 'string') {
                        if (subgridResult.indexOf("<") == -1 && subgridResult.indexOf("/") > -1)
                            subgridJq.load(subgridResult);
                        else
                            subgridJq.empty().append(subgridResult.outerHTML);
                    } else if (subgridResult != undefined)
                        alert('Subgrid type not supported.');
                    // else do nothing
                } else if (typeof subgrid === "string") {
                    subgridJq.load(subgrid);
                } else {
                    this._invoke(this.options.subGridRowExpanded, [subgrid_id, row_id]);
                }
            }
        },
        _disableSelection: function (target) {
            target = $(target).get(0);
            if (target) {
                if (typeof target.onselectstart != "undefined") //IE route
                    target.onselectstart = function () { return false }
                else if (typeof target.style.MozUserSelect != "undefined") //Firefox route
                    target.style.MozUserSelect = "none"
                else //All other route (ie: Opera)
                    target.onmousedown = function () { return false }
                target.style.cursor = "default"
            }
        },
        _fireOnEmpty: function () {
            var $this = $(this.element);
            var onEmpty = this.options.onEmpty;
            if (onEmpty) {
                //var count = $(".jqgfirstrow td", $this).length;
                var cell = $("<td style='width: 100%;text-align:center;line-height:13px;padding:20px;height:100%;' class='onEmpty'></td>");
                var row = $("<tr tabindex='-1'class='ui-widget-content jqgrow ui-row-ltr'></tr>");

                if ($.isFunction(onEmpty)) {
                    var onEmptyResult = onEmpty();
                    if (isJquery(onEmptyResult))
                        cell.append(onEmptyResult);
                    else
                        cell.html(onEmptyResult);
                }
                else {
                    cell.html(onEmpty);
                }

                // hard code 16 columns, same as jqGrid does internally
                cell.attr("colspan", 16).appendTo(row);
                $("tbody", $this).append(row);
            }
        },
        reload: function () {
            var self = this;
            var gridOptions = this._getReloadableGridOptions();
            var $this = $(self.element);
            var rowid = $this.getGridParam('selarrrow');
            $("body").data("generic-grid-reload-rowid", rowid);
            $(self.element).jqGrid("setGridParam", gridOptions);
            $this.trigger("reloadGrid");
        },
        loading: function () {
            $(this.element).parents(".ui-widget:first").find(".loading").show();
        },
        loaded: function () {
            $(this.element).parents(".ui-widget:first").find(".loading").hide();
        },
        setGridParam: function (object) {
            $(this.element).jqGrid("setGridParam", object);
        }
    });
})(jQuery);

if (window.isJquery === undefined) {
    window.isJquery = function (obj) {
        var result = obj && obj.hasOwnProperty && obj instanceof jQuery;
        return result;
    };
}
