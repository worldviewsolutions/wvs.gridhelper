﻿
DCR.gridUtils.createGenericLocalGrid = function (grid, pager, matchWidth, colNames, colModel, gridData, onRowSelectCallBackMethod) {
    return grid.genericGrid({
        matchWidth: matchWidth,
        data: gridData,
        datatype: 'local',
        colNames: colNames,
        colModel: colModel,
        pager: pager,
        sortname: 'Name',
        sortorder: "asc",
        height: 'auto',
        gridview: true,
        viewrecords: true,
        sortable: true,
        multiselect: false,
        onSelectRow: onRowSelectCallBackMethod
    });
}

DCR.gridUtils.createGenericJsonGrid = function (grid, pager, matchWidth, colNames, colModel, url, postData, onRowSelectCallBackMethod, defer) {
    return grid.genericGrid({
        url: url,
        mtype: 'POST',
        matchWidth: matchWidth,
        colNames: colNames,
        colModel: colModel,
        pager: pager,
        sortname: 'ID',
        sortorder: "asc",
        height: 'auto',
        gridview: true,
        viewrecords: true,
        sortable: true,
        multiselect: false,
        postData: postData,
        onSelectRow: onRowSelectCallBackMethod,
        beforeRequest: function () {
            return !defer;
        }
    });
}
DCR.gridUtils.createGenericSoilsJsonGrid = function (grid, pager, matchWidth, colNames, colModel, url, postData, onRowSelectCallBackMethod, defer) {
    return grid.genericGrid({
        url: url,
        mtype: 'POST',
        matchWidth: matchWidth,
        colNames: colNames,
        colModel: colModel,
        pager: pager,
        sortname: 'Field',
        sortorder: "asc",
        height: 'auto',
        gridview: true,
        viewrecords: true,
        sortable: true,
        multiselect: false,
        postData: postData,
        onSelectRow: onRowSelectCallBackMethod,
        beforeRequest: function () {
            return !defer;
        }
    });
}

