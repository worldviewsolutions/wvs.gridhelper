﻿//# sourceURL=dcr.common.js

var DCR = DCR || {};

DCR.namespace = function (ns_string) {

    var parts = ns_string.split('.'),
        parent = DCR,
        i;

    if (parts[0] === "DCR") {
        parts = parts.slice(1);
    }

    for (i = 0; i < parts.length; i += 1) {
        if (typeof parent[parts[i]] === "undefined") {
            parent[parts[i]] = {};
        }
        parent = parent[parts[i]];
    }
    return parent;
};

DCR.namespace("gridUtils");
DCR.namespace("utils");
DCR.namespace("TabHelpers");
DCR.namespace("ko.models");
DCR.namespace("models");
DCR.namespace("workflow");
DCR.namespace("bmp");
DCR.namespace("soils");
DCR.namespace("inspections");
DCR.namespace("mapping");
DCR.namespace("landUnits");
DCR.namespace("schedule");
DCR.namespace("spatiallyrelatedbmp");
DCR.namespace("complaints");
DCR.namespace("nmp"); // added for dcr.nmp.common.js
DCR.namespace("conversion"); // added for dcr.common.conversion.js


/*****************************
 Globals
 ****************************/

$(document).ready(function () {

    if (typeof datepicker == "function") {
        $(".datepicker").datepicker({
            dateFormat: "m/d/yy",
            changeYear: true,
            changeMonth: true,
            onSelect: function (dateText, inst) {
                $(this).parent().focus();
            }
        });
    }

    //disable caching all ajax request
    $.ajaxSetup({ cache: false });

    //force all browsers to allow cross-site scripting
    $.support.cors = true;

});

if (!String.prototype.startsWith) {
    // see below for better implementation!
    String.prototype.startsWith = function (str) {
        return this.indexOf(str) == 0;
    };

if (!location.origin)
    location.origin = location.protocol + "//" + location.host;

}

/*****************************
ko models
****************************/

DCR.ko.models.participantAddress = function (args) {
    var self = this;

    this.id = ko.observable(args.Id) || ko.observable();
    this.address = ko.observable(args.Address) || ko.observable();
    this.address2 = ko.observable(args.Address2) || ko.observable();
    this.city = ko.observable(args.City) || ko.observable();
    this.state = ko.observable(args.State) || ko.observable();
    this.zip = ko.observable(args.Zip) || ko.observable();

    this.phone = ko.observable(args.Phone) || ko.observable();
    this.phone2 = ko.observable(args.Phone2) || ko.observable();
    this.fax = ko.observable(args.Fax) || ko.observable();
}

DCR.ko.models.participant = function () {
    var self = this;

    this.id = ko.observable();
    this.name = ko.observable();
    this.role = ko.observable();
    this.roleName = ko.computed(function () {
        return $("#participantForm div[display!='hidden'] option[value='" + self.role() + "']").first().text();
    });
    this.businessName = ko.observable();
    this.firstName = ko.observable();
    this.middleInitial = ko.observable();
    this.lastName = ko.observable();
    this.email = ko.observable();
    this.street1 = ko.observable();
    this.street2 = ko.observable();
    this.city = ko.observable();
    this.state = ko.observable();
    this.nonState = ko.observable();
    this.stateName = ko.computed(function () {
        if (!self.state())
            return;

        if (self._getSelectOptionValue(self.state()))
            return self._getSelectOptionValue(self.state());

        // If it gets this far just return the value
        return self.state();
    });
    this.zip = ko.observable();
    this.primaryPhone = ko.observable();
    this.secondaryPhone = ko.observable();
    this.faxNumber = ko.observable();
    this.addresses = ko.observableArray([]);
    this.selectedAddressId = ko.observable();

    this._getSelectOptionValue = function (value) {
        return $("#participantAddressForm option[value='" + value + "']").text();
    }

    // Lurks in the back updating the review page
    this._updateParticipantAddress = ko.computed(function () {

        if (!self.selectedAddressId())
            return;

        var address = $.grep(self.addresses(), function (address) {
            return address.id() == self.selectedAddressId();
        })[0];

        if (address) {
            self.street1(address.address());
            self.street2(address.address2());
            self.city(address.city());

            // Since address.state() doesn't have to exist in the select it results
            // in a null value on setting if not a state
            if (!self._getSelectOptionValue(address.state())) {
                // Not a valid state value set non state value
                self.nonState(address.state());
            } else {
                self.state(address.state());
            }

            self.zip(address.zip());
            self.primaryPhone(address.phone());
            self.secondaryPhone(address.phone2());
            self.faxNumber(address.fax());

        }
    }, this);

};

/*****************************
 models
****************************/

DCR.models.plan = function (args) {

    var self = this;
    this.id = args.id;
    this.area = args.area;
    this.district = args.district;
    this.currentStep = args.currentStep;
    this.currentStepCode = args.currentStepCode;
    this.currentPhase = args.currentPhase;
    this.nextStep = args.nextStep;
    this.nextPhase = args.nextPhase;
    this.canEdit = args.canEdit;
    this.showDataTab = function () {
        DCR.TabHelpers.SetTabDirty("#landUnitsTab");
        DCR.TabHelpers.SetTabDirty("#soilsTab");
        DCR.TabHelpers.SetTabDirty("#bmpTab");
        DCR.TabHelpers.SetTabDirty("#spatiallyRelatedBmpTab");
        DCR.TabHelpers.SetTabDirty("#inventoriesTab");
        $("#showDataTabBtn").hide();
        $("#showMapTabBtn").show();
        $("#planMapArea").hide();
        $("#planTabs").show();
    };
    this.showMapTab = function () {
        $("#showMapTabBtn").hide();
        $("#showDataTabBtn").show();
        $("#planTabs").hide();
        $("#planMapArea").show();

        if (typeof require === "undefined") {
            if (!DCR.mapping.MapApiRequestMade) {
                DCR.mapping.LoadMapAPI(this.showMapTab);
                DCR.mapping.MapApiRequestMade = true;
            }
            return;
        }

        DCR.mapping.ShowPlanMap();
    };
};

/*****************************
Utils
****************************/

DCR.utils.formToObject = function (form) {
    var config = config || {};
    form.serializeArray().map(function (item) {
        config[item.name] = item.value;
    });
    return config;
};

DCR.utils.wizard = function (args) {
    var self = this;

    this.urls = args.urls || [];
    this.stepContainer = [];
    this.urlIndex = args.urlIndex || 0;

    this.previousButton = args.previousButton;
    this.beforePrevious = args.beforePrevious || function () { return true; };
    this.previousCallback = args.previousCallback || function () { };

    this.nextButton = args.nextButton;
    this.beforeNext = args.beforeNext || function () { return true; };
    this.nextCallback = args.nextCallback || function () { };

    this.submitButton = args.submitButton;
    this.submitCallback = args.submitCallback || function () { };

    this.dataAgreementButton = args.dataAgreementButton || null;
    this.dataAgreementLabel = args.dataAgreementLabel || null;

    this.loadComplete = args.loadComplete || function () { };

    this.setButtonVisibility = function () {
        var stepList = $('.wizardStepList');
        var numSteps = stepList.find('li').length;
        var showSubmit = stepList.find('li').last().hasClass('current');
        var showNext = !showSubmit;
        var showPrevious = !stepList.find('li').first().hasClass('current');

        if (showPrevious)
            this.previousButton.show();
        else
            this.previousButton.hide();

        if (showNext)
            this.nextButton.show();
        else
            this.nextButton.hide();

        if (showSubmit){            
            this.submitButton.show();
            if (this.dataAgreementButton) {
                this.dataAgreementButton.show();
                this.dataAgreementLabel.show();

            }
        } else {
            this.submitButton.hide();
            if (this.dataAgreementButton) {
                this.dataAgreementButton.hide();
                this.dataAgreementLabel.hide();
            }
        }
    }

    this.reset = function () {
        var steps = $('.wizardStepList li');

        // Get current index
        var current = $('.wizardStepList li.current');
        var index = steps.index(current);

        current.removeClass('current');
        $(steps[0]).addClass('current');

        this.previousButton.hide();
        this.submitButton.hide();
        this.nextButton.show();

        $(this.stepContainer[this.urlIndex]).hide();
        $('#wizardContent #stepHeader').html($(steps[0]).data('title'));
        $('#wizardContent form').clearValidation();
        $(this.stepContainer[0]).show();
        this.urlIndex = 0;
    }

    this.next = function () {
        if (this.beforeNext()) {

            var steps = $('.wizardStepList li');
            var numSteps = $('.wizardStepList').find('li').length;

            // Get current index
            var current = $('.wizardStepList li.current');
            var index = steps.index(current);

            if (index < numSteps - 1) {
                current.removeClass('current');
                $(steps[index + 1]).addClass('current');

                $('#wizardContent #stepHeader').html($(steps[index + 1]).data('title'));

                $(this.stepContainer[this.urlIndex]).hide();
                $(this.stepContainer[++this.urlIndex]).show();
                self.setButtonVisibility();

                this.nextCallback();
            }
        }
    };

    this.previous = function () {

        if (this.beforePrevious()) {
            var steps = $('.wizardStepList li');

            // Get current index
            var current = $('.wizardStepList li.current');
            var index = steps.index(current);

            if (index > 0) {
                current.removeClass('current');
                $(steps[index - 1]).addClass('current');

                $('#wizardContent #stepHeader').html($(steps[index - 1]).data('title'));

                $(this.stepContainer[this.urlIndex]).hide();
                $(this.stepContainer[--this.urlIndex]).show();
                self.setButtonVisibility();

                this.previousCallback();
            }
        }
    };

    this.submit = function () {
        // Submit the form
        this.submitCallback();
    }

    this.loadContent = function () {
        // Request and store
        for (var i = 0; i < this.urls.length; i++) {
            $.ajax({
                url: this.urls[i]
            , async: false
            }).done(function (response) {
                var div = $('<div style="display: none;">').append(response)
                self.stepContainer.push(div);
                $('#wizardContent').append(div);
            });
        }

        // Show first step


        $('#wizardContent #stepHeader').html($($('.wizardStepList li')[this.urlIndex]).data('title'));
        $(this.stepContainer[this.urlIndex]).show();

        // Apply validators to forms
        var forms = $('#wizardContent').find('form');
        forms.removeData('validator');
        forms.find('form').removeData('unobtrusiveValidation');
        $.validator.unobtrusive.parse(forms);

        this.loadComplete();
    }

    this.loadContent();
    this.setButtonVisibility();

    return this;
}

DCR.utils.ShowLoadingMessage = function (selector) {
    var jqElement = $(selector);
    jqElement.html("<div class=\"loading\"><div class=\"loadingText\">Loading</div><div class=\"loadingThrobber\"></div></div>");
};

DCR.utils.initEditableForm = function () {
    // set up "live" events    
    $("body")
        // numeric text boxes
        .on("keydown", "input.numeric,.numeric input", function (e) {
            if (e.ctrlKey)
                return true;

            var key = e.charCode || e.keyCode || 0;

            var keys = [8, 9, 46, 17];
            var val = $(this).val();
            if (val.indexOf(".") == -1) {
                keys.push(110); // decimal point
                keys.push(190); // period
            }
            if (val.length == 0) {
                keys.push(189); // dash
                keys.push(109); // dash
                keys.push(173); // ??? (dash)
            }

            var result =
                    keys.contains(key) ||
                    (key >= 35 && key <= 40) ||
                    (key >= 48 && key <= 57) ||
                    (key >= 96 && key <= 105);
            return result;
        })
        // integer text boxes
        .on("keydown", "input.integer,.numeric integer", function (e) {
            if (e.ctrlKey)
                return true;

            var key = e.charCode || e.keyCode || 0;

            var keys = [8, 9, 46, 17];
            var val = $(this).val();

            if (val.length == 0) {
                keys.push(189); // dash
                keys.push(109); // dash
                keys.push(173); // ??? (dash)
            }

            var result =
                    keys.contains(key) ||
                    (key >= 35 && key <= 40) ||
                    (key >= 48 && key <= 57) ||
                    (key >= 96 && key <= 105);
            return result;
        });

    $(".datepicker").datepicker({
        dateFormat: "m/d/yy",
        changeYear: true,
        changeMonth: true,
        onSelect: function (dateText, inst) {
            $(this).change(); // allow jquery change to trigger by datepickers.
            $(this).parent().focus();
        }
    });

    $("form").each(function () {
        $.validator.unobtrusive.parse($(this));
    });
}

/*****************************
Tab Helpers
****************************/

// Cache tab bodies to prevent AJAX request on every tab select 
DCR.TabHelpers.BeforeLoadCache = function (event, ui) {
    if (ui.tab.data("loaded")) {
        event.preventDefault();
        return;
    } else {
        DCR.utils.ShowLoadingMessage(ui.panel);
    }
    ui.jqXHR.success(function () {
        ui.tab.data("loaded", true);
    });
};

//Will set the tab to dirty and refresh the tab if its currently active
//Parameter is the tab li selector
DCR.TabHelpers.SetTabDirty = function (selector) {

    //set data loaded to false
    $(selector).data("loaded", false);

    //refersh the tab if its selected
    if ($(selector).index() > -1 &&
        $(selector).index() === $(selector).parent().parent().tabs('option', 'active')) {
        $(selector).parent().parent().tabs('load', $(selector).index());
    }
};

// Append hash to url to enable bookmarking of tabs 
DCR.TabHelpers.ActivateTabSetHash = function (event, ui) {
    var urlLessHash = window.location.href.split("#")[0];

    if (ui.newTab[0].id.length > 0) {
        window.location.replace(urlLessHash + "#tab-" + ui.newTab[0].id)
    }
    else
        window.location.replace(urlLessHash + "#");
}

// Get default tab index based on location hash
DCR.TabHelpers.GetTabIndexFromHash = function () {
    var tabIndex = 0;
    if (window.location.hash.substr(0, 5) == "#tab-") {
        var jqLi = $("#" + window.location.hash.substr(5));
        if (jqLi.length > 0) tabIndex = jqLi.prevAll().length;
    }
    return tabIndex;
}


/*****************************
Misc
****************************/

// jQuery clear validation extension
$.fn.clearValidation = function () { var v = $(this).validate(); $('[name]', this).each(function () { v.successList.push(this); v.showErrors(); }); v.resetForm(); v.reset(); };

/**
 * Selects the first option of a SELECT
 * @param {type} selector
 */
DCR.utils.selectFirst = function (selector) {
    $(selector).val($("#" + selector[0].id + " option:first").val());
};




/**
 * Gets the base url value from the <base> tag.  This url will contain a trailing forward slash.  
 * @returns {string} 
 */
DCR.utils.getBaseUrl = function () {
    return window.baseUrl;
    //return $('base').attr('href');
}




/*****************************
Knockout bindings
****************************/

ko.bindingHandlers.datepicker = {
    init: function (element, valueAccessor, allBindingsAccessor) {
        //initialize datepicker with some optional options
        var options = allBindingsAccessor().datepickerOptions || {},
            $el = $(element);

        $el.datepicker(options);

        //handle the field changing
        ko.utils.registerEventHandler(element, "change", function () {
            var observable = valueAccessor();
            observable($el.datepicker("getDate"));
        });

        //handle disposal (if KO removes by the template binding)
        ko.utils.domNodeDisposal.addDisposeCallback(element, function () {
            $el.datepicker("destroy");
        });

    },
    update: function (element, valueAccessor) {
        var value = ko.utils.unwrapObservable(valueAccessor()),
            $el = $(element);

        //handle date data coming via json from Microsoft
        if (String(value).indexOf('/Date(') == 0) {
            value = new Date(parseInt(value.replace(/\/Date\((.*?)\)\//gi, "$1")));
        }

        var current = $el.datepicker("getDate");

        if (value - current !== 0) {
            $el.datepicker("setDate", value);
        }
    }
};

/*****************************
Mapping
****************************/

DCR.mapping.MapApiRequestMade = false;

DCR.mapping.LoadMapAPI = function (callbackMethod) {

    var aPISsource = settings.apiSource ? settings.apiSource : "//js.arcgis.com/3.16/init.js";
    console.log("MapAPIRequestMade src = " + aPISsource);

    var scriptTag = document.createElement('script');
    scriptTag.type = 'text/javascript';
    scriptTag.async = true;
    scriptTag.onload = scriptTag.onreadystatechange = function () {
        if (!this.readyState || this.readyState === "loaded" || this.readyState === "complete") {

            // Handle memory leak in IE
            scriptTag.onload = scriptTag.onreadystatechange = null;
            console.log("APILoaded");
            if (callbackMethod) {
                callbackMethod();
            }
        }
    };
    scriptTag.src = aPISsource;
    console.log("Appending ESRI JS Script Reference");
    document.getElementsByTagName('head')[0].appendChild(scriptTag);
}

/*****************************
Schedule
****************************/

DCR.schedule.onSuccessCallback = function () {
    DCR.TabHelpers.SetTabDirty("#scheduleTab");
    $('#scheduleDialog').dialog('close');
    $.NotificationBar({
        message: 'Schedule update successful.',
        type: 'confirm'
    });
}

DCR.schedule.onFailureCallback = function () {
    DCR.TabHelpers.SetTabDirty("#scheduleTab");
    $('#scheduleDialog').dialog('close');
    $.NotificationBar({
        message: 'An error occured while updating the schedule.',
        type: 'error'
    });
}


/*****************************
 Custom Validators
 ****************************/

if ($.validator) {
    $.validator.addMethod('monthYearDate', function (value, element) {
        return value.match(/\d+\/\d{4}/g);
    }, 'Date must have format M/YYYY');
}



/*****************************
 Browser Diagnostics
 ****************************/

function getChromeVersion() {
    var raw = navigator.userAgent.match(/Chrom(e|ium)\/([0-9]+)\./);

    return raw ? parseInt(raw[2], 10) : false;
}

function getJQVersion() {
    return jQuery.fn.jquery;
}


/**
 * Gets the browser name or returns an empty string if unknown. 
 * This function also caches the result to provide for any 
 * future calls this function has.
 *
 * @returns {string}
 */
var browser = function () {
    // Return cached result if avalible, else get result then cache it.
    if (browser.prototype._cachedResult)
        return browser.prototype._cachedResult;

    // Opera 8.0+
    var isOpera = (!!window.opr && !!opr.addons) || !!window.opera || navigator.userAgent.indexOf(' OPR/') >= 0;

    // Firefox 1.0+
    var isFirefox = typeof InstallTrigger !== 'undefined';

    // Safari 3.0+ "[object HTMLElementConstructor]" 
    var isSafari = /constructor/i.test(window.HTMLElement) || (function (p) { return p.toString() === "[object SafariRemoteNotification]"; })(!window['safari'] || safari.pushNotification);

    // Internet Explorer 6-11
    var isIE = /*@cc_on!@*/false || !!document.documentMode;

    // Edge 20+
    var isEdge = !isIE && !!window.StyleMedia;

    // Chrome 1+
    var isChrome = !!window.chrome && !!window.chrome.webstore;

    // Blink engine detection
    var isBlink = (isChrome || isOpera) && !!window.CSS;

    return browser.prototype._cachedResult =
        isOpera ? 'Opera' :
        isFirefox ? 'Firefox' :
        isSafari ? 'Safari' :
        isChrome ? 'Chrome' :
        isIE ? 'IE' :
        isEdge ? 'Edge' :
        "Don't know";
};


function logVersionInfo() {
    console.log('Chrome Version: ' + getChromeVersion())
    console.log('JQuery Version: ' + getJQVersion())

    if (typeof dojo !== "undefined") {
        console.log('Dojo Version: ' + dojo.version.toString());
    }

    if (typeof esri !== "undefined") {
        console.log('EsrJs Version: ' + esri.version.toString());
    }

    if (typeof ko !== "undefined") {
        console.log('Knockout.Js Version: ' + ko.version.toString());
    }

    console.log(browser());


}


logVersionInfo();

/*****************************
 Math Functions
 ****************************/

/** * Correctly rounds decimals values (use instead of Number((1.005).toFixed(2)) or Math.round(1.005*100)/100) * @value {Number} The value to be rounded * @decimals {Number} The number of decimal places to round * @return {Number} rounded value* http://www.jacklmoore.com/notes/rounding-in-javascript/ */
function round(value, decimals) {
    return Number(Math.round(value + 'e' + decimals) + 'e-' + decimals);
}



