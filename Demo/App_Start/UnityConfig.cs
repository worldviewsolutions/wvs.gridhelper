using System.Collections;
using System.Collections.Generic;
using System.Linq;
using System.Web.Mvc;
using DemoShared.DAL;
using Unity;
using Unity.Lifetime;
using Unity.Mvc5;

namespace Demo
{
    public static class UnityConfig
    {
        public static void RegisterComponents()
        {
            var container = new UnityContainer();

            // e.g. container.RegisterType<ITestService, TestService>();

            var db = TestDAL.TestDb;
            db.InitTestData();
            container.RegisterInstance(db);

            var list = TestEntityFactory.TestDataFactory().ToList();
            container.RegisterInstance<IList<TestEntity>>(list);

            DependencyResolver.SetResolver(new UnityDependencyResolver(container));
        }
    }
}
