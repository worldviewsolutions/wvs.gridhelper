﻿using System.Web.Optimization;

namespace Demo
{
    internal static class BundleConfig
    {
        public static void RegisterBundles(BundleCollection bundleCollection)
        {
            bundleCollection.Add(new StyleBundle("~/App_Themes/DCR/bundle").Include(
                "~/App_Themes/DCR/StyleSheet.css"
                , "~/App_Themes/DCR/tabs.css"
                , "~/App_Themes/DCR/Ribbons.css"
                , "~/App_Themes/DCR/BMPInstance.css"
                , "~/App_Themes/DCR/jquery-ui.css"
                , "~/App_Themes/DCR/jquery-ui.structure.css"
                , "~/App_Themes/DCR/jquery-ui.theme.css"
                , "~/App_Themes/DCR/ui.multiselect.css"
                , "~/App_Themes/DCR/jquery.tokenize.css"
                , "~/App_Themes/DCR/ui.jqgrid.css"
                , "~/App_Themes/DCR/Plans.css"
                , "~/App_Themes/DCR/ASA.css"
            ));

            bundleCollection.Add(new ScriptBundle("~/bundles/jquery").Include(
                "~/Scripts/jquery-{version}.js"
                , "~/Scripts/jquery-migrate-{version}.js"
                ));

            bundleCollection.Add(new ScriptBundle("~/bundles/DCR").Include(
                "~/Scripts/knockout-{version}.js"
                , "~/Scripts/jquery.unobtrusive-ajax.js"
                , "~/Scripts/jquery.validate.js"
                , "~/Scripts/jquery.validate.unobtrusive.js"
                , "~/Scripts/jquery-ui-{version}.js"
                , "~/Scripts/DCR/jquery.tokenize.js"
                , "~/Scripts/DCR/ui.multiselect.js"
                , "~/Scripts/DCR/grid.locale-en.js"
                , "~/Scripts/DCR/jquery.jqGrid.js"
                , "~/Scripts/DCR/dcr.common.js"
                , "~/Scripts/DCR/dcr.widget.genericGrid.js"
                , "~/Scripts/DCR/app.grids.js"
            ));
        }
    }
}
