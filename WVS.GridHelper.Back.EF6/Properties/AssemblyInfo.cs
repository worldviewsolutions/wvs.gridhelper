﻿using System.Reflection;
using System.Runtime.InteropServices;

// General Information about an assembly is controlled through the following
// set of attributes. Change these attribute values to modify the information
// associated with an assembly.
[assembly: AssemblyTitle("WVS.GridHelper.EF6")]
[assembly: AssemblyDescription("GetGridData, LinqFilterEF6")]
[assembly: AssemblyConfiguration("")]
[assembly: AssemblyCompany("WorldView Solutions")]
[assembly: AssemblyProduct("WVS.GridHelper.EF6")]
[assembly: AssemblyCopyright("Copyright ©  2017")]
[assembly: AssemblyTrademark("")]
[assembly: AssemblyCulture("")]

// Setting ComVisible to false makes the types in this assembly not visible
// to COM components.  If you need to access a type in this assembly from
// COM, set the ComVisible attribute to true on that type.
[assembly: ComVisible(false)]

// The following GUID is for the ID of the typelib if this project is exposed to COM
[assembly: Guid("a0a8c97e-47e0-486d-a270-bb2a15bbe913")]

// Set via GitVersion during build.
[assembly: AssemblyVersion("0.0.0.0")]
[assembly: AssemblyFileVersion("0.0.0.0")]
