﻿using System;
using System.Data.Entity;
using System.Linq;
using System.Linq.Expressions;
using System.Reflection;
using WVS.GridHelper;
using WVS.GridHelper.Interfaces;
using static WVS.GridHelper.ExpressionHelpers;

namespace WVS.GridHelper.Back.EF6
{
    public class LinqFilterEF6 : LinqFilter
    {
        protected override TypeFilterer DateTimeFilterer { get; set; } = new FilterDateTimesEF6();
    }

    public class FilterDateTimesEF6 : FilterDateTimes
    {
        public override Expression<Func<T, bool>> Filter<T>(IFilter filter)
        {
            var predicate = False<T>(); // dummy to start expression chain.
            var name = filter.Attribute;

            if (!DateTime.TryParse(filter.Values.First(), out var dateTimeParsed))
                return null; // bad date, skip filter.
            DateTime? dateTime = dateTimeParsed;

            switch (filter.Operator)
            {
                case FilterOperator.ON:
                    predicate = DateTimeOn(predicate, name, dateTime);
                    return predicate;
                case FilterOperator.BEFORE:
                    predicate = DateTimeBeforeAfter(predicate, name, dateTime, before: true);
                    return predicate;
                case FilterOperator.AFTER:
                    predicate = DateTimeBeforeAfter(predicate, name, dateTime, before: false);
                    return predicate;
                case FilterOperator.BETWEEN:
                    if (filter.Values.Count < 2)
                        return null; // no 2nd date, skip filter.
                    DateTime? dateTime2 = null;
                    if (!DateTime.TryParse(filter.Values.Skip(1).First(), out var dateTime2Parsed))
                        return null; // bad 2nd date, skip filter.
                    dateTime2 = dateTime2Parsed;
                    return DateTimeBetween(predicate, name, dateTime, dateTime2);
            }
            return null; // other operators not supported, skip filter.
        }

        private static Expression<Func<T, bool>> DateTimeOn<T, TValue>
            (Expression<Func<T, bool>> predicate, string memberPath, TValue? value)
            where TValue : struct
        {
            // x
            var param = predicate.Parameters.Single();

            // DbFunctions.TruncateTime(x.Member)
            var truncateMemberInfo = DbFunctionsTruncateTimeMethodCallForMember<T, TValue>(predicate, memberPath);

            // DbFunctions.TruncateTime(value)
            var truncateTimeOfValue = DbFunctionsTruncateTimeMethodCallForConstant(value);

            // DbFunctions.TruncateTime(x.Member) == DbFunctions.TruncateTime(value)
            var comparison = Expression.Equal(truncateMemberInfo.truncate, truncateTimeOfValue);

            // don't explode if something in the path is null, just return false.
            var safeComparison = SafeExpression(truncateMemberInfo.nullCheckExpression, comparison);

            // x => DbFunctions.TruncateTime(x.Member) == DbFunctions.TruncateTime(value)
            return Expression.Lambda<Func<T, bool>>(safeComparison, param);
        }

        private static Expression<Func<T, bool>> DateTimeBeforeAfter<T, TValue>
            (Expression<Func<T, bool>> predicate, string memberPath, TValue? value, bool before)
            where TValue : struct
        {
            // x
            var param = predicate.Parameters.Single();

            // DbFunctions.TruncateTime()
            var truncateMemberInfo = DbFunctionsTruncateTimeMethodCallForMember<T, TValue>(predicate, memberPath);

            // value as Nullable
            var constantValue = Expression.Constant(value, typeof(TValue?));

            var comparison = before
                // DbFunctions.TruncateTime(x.Member) <= value
                ? Expression.LessThanOrEqual(truncateMemberInfo.truncate, constantValue)
                // DbFunctions.TruncateTime(x.Member) >= value
                : Expression.GreaterThanOrEqual(truncateMemberInfo.truncate, constantValue);

            // don't explode if something in the path is null, just return false.
            var safeComparison = SafeExpression(truncateMemberInfo.nullCheckExpression, comparison);

            // x => DbFunctions.TruncateTime(x.Member) <=/>= value
            return Expression.Lambda<Func<T, bool>>(safeComparison, param);
        }

        private static Expression<Func<T, bool>> DateTimeBetween<T, TValue>
            (Expression<Func<T, bool>> predicate, string memberPath, TValue? min, TValue? max)
            where TValue : struct
        {
            // x
            var param = predicate.Parameters.Single();

            // DbFunctions.TruncateTime()
            var truncateInfo = DbFunctionsTruncateTimeMethodCallForMember<T, TValue>(predicate, memberPath);

            // min
            var constantMin = Expression.Constant(min, typeof(TValue?));

            // max
            var constantMax = Expression.Constant(max, typeof(TValue?));

            // min <= DbFunctions.TruncateTime(x.Member)
            var leftComparison = Expression.LessThanOrEqual(constantMin, truncateInfo.truncate);

            // DbFunctions.TruncateTime(x.Member) <= max
            var rightComparison = Expression.LessThanOrEqual(truncateInfo.truncate, constantMax);

            // min <= DbFunctions.TruncateTime(x.Member) && DbFunctions.TruncateTime(x.Member) <= max
            var comparison = Expression.AndAlso(leftComparison, rightComparison);

            // don't explode if something in the path is null, just return false.
            var safeComparison = SafeExpression(truncateInfo.nullCheckExpression, comparison);

            // x => (min <= DbFunctions.TruncateTime(x.Member) && DbFunctions.TruncateTime(x.Member) <= max)
            return Expression.Lambda<Func<T, bool>>(safeComparison, param);
        }

        private static (MethodCallExpression truncate, Expression nullCheckExpression) DbFunctionsTruncateTimeMethodCallForMember<T, TValue>
            (Expression<Func<T, bool>> predicate, string memberPath)
            where TValue : struct
        {
            // x
            var param = predicate.Parameters.Single();

            // x.Member
            var memberAccessInfo = GetMemberExpressionFromPath<T>(param, memberPath);

            var nullableMember = Expression.Convert(memberAccessInfo.memberExpression, typeof(TValue?)); // box into nullable value if not already. DbFunctions.TruncateTime smartly ONLY works with nullable values.

            // DbFunctions.TruncateTime()
            var truncateTime = DbFunctionsTruncateTimeMethodInfo<TValue>();

            // DbFunctions.TruncateTime(x.Member)
            var methodCall = Expression.Call(truncateTime, nullableMember);

            return (methodCall, memberAccessInfo.nullCheckExpression);
        }

        /// <summary>
        /// DbFunctions.TruncateTime taking a constant value parameter. Turns into native SQL date functions.
        /// </summary>
        /// <typeparam name="TValue">Type constant value to pass into DbFunction method. Generic type to work with DateTime as well as DateTimeOffset as well as their nullable versions.</typeparam>
        /// <param name="value">Constant value parameter to truncate.</param>
        private static MethodCallExpression DbFunctionsTruncateTimeMethodCallForConstant<TValue>(TValue? value)
            where TValue : struct
        {
            // DbFunctions.TruncateTime()
            var truncateTime = DbFunctionsTruncateTimeMethodInfo<TValue>();

            // DbFunctions.TruncateTime(value)
            return Expression.Call(truncateTime, Expression.Constant(value, typeof(TValue?)));
        }

        /// <summary>
        /// Select a specific DbFunctions.TruncateTime method. TruncateTime has lots of overloads, for DateTime and DateTimeOffset and nullable versions.
        /// </summary>
        private static MethodInfo DbFunctionsTruncateTimeMethodInfo<T>()
        {
            // DbFunctions.TruncateTime()
            return typeof(DbFunctions).GetMethod(nameof(DbFunctions.TruncateTime), new[] { typeof(T) });
        }
    }

}
