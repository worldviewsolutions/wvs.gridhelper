﻿using System;
using System.Collections.Generic;
using System.Data.Entity;
using System.Data.Entity.Core.Objects;
using System.Diagnostics;
using System.Linq;
using System.Threading.Tasks;
using System.Linq.Dynamic.Core;
using WVS.GridHelper.Interfaces;

namespace WVS.GridHelper.Back.EF6
{
    public static class GridData
    {
        public static async Task<PagingModel> GetGridData<T>
            (this IQueryable<T> query, IGrid gridConfiguration, string orderBy, bool orderAscending, int rowsPerPage, int page, IFilterList filters = null, IFilterWhitelist whiteList = null, IFilterList scopeFilters = null)
            where T : class
        {
            var itemInfo = FilterSortCountWindow(query, orderBy, orderAscending, rowsPerPage, page, filters, whiteList, scopeFilters);

#if DEBUG
            var sw = new Stopwatch();
            //Trace.WriteLine($"Query count: {itemInfo.filteredQuery.ToTraceString()}");
            sw.Restart();
#endif

            // execute count.
            int count;
            if (itemInfo.filteredQuery is EnumerableQuery)
                count = itemInfo.filteredQuery.Count(); // query against List doesn't support CountAsync.
            else
                count = await itemInfo.filteredQuery.CountAsync(); // execute count against database.

#if DEBUG
            Trace.WriteLine($"Query count executed in {sw.ElapsedMilliseconds}ms.");
            //Trace.WriteLine($"Query: {itemInfo.filteredWindowedQuery.ToTraceString()}");
            sw.Restart();
#endif

            // execute select.
            var pagingModel = await ToPagingModel(gridConfiguration, itemInfo.filteredWindowedQuery, count, rowsPerPage, page);

#if DEBUG
            Trace.WriteLine($"Query executed in {sw.ElapsedMilliseconds}ms.");
            sw.Reset();
#endif

            return pagingModel;
        }

        private static (IQueryable<T> filteredQuery, IQueryable<T> filteredWindowedQuery) FilterSortCountWindow<T>
            (IQueryable<T> originalQuery, string orderBy, bool orderAscending, int rowsPerPage, int page, IFilterList filters, IFilterWhitelist whiteList, IFilterList scopeFilters)
            where T : class
        {
            if (string.IsNullOrEmpty(orderBy))
                throw new ArgumentNullException(nameof(orderBy));

            // filter source collection.
            var srcUnfiltered = originalQuery
                .AsNoTracking();

            var src = new LinqFilterEF6()
                .ApplyFilters(srcUnfiltered, filters, whiteList, scopeFilters);

            // build query to retrieve row data.
            var ascendingOrDescending = orderAscending ? "asc" : "desc";
            var orderByExpression = $"@{orderBy} {ascendingOrDescending}";
            var items = src
                .OrderBy(orderByExpression)
                .Skip(rowsPerPage * (page - 1))
                .Take(rowsPerPage);

            return (src, items);
        }

        private static async Task<PagingModel> ToPagingModel<T>
            (IGrid gridConfiguration, IQueryable<T> query, int totalCount, int rowsPerPage, int page)
            where T : class
        {
            var anonQuery = query
                .AsNoTracking()
                .ProjectToGrid(gridConfiguration); // select only grid columns.

            var projectedType = anonQuery.ElementType;

            // execute query.
            // tolist to get count within window.
            List<object> items;
            if (anonQuery is EnumerableQuery)
                items = anonQuery.AsEnumerable().ToList(); // IList provider skips ToListAsync()
            else
                items = await anonQuery.ToListAsync();

            // return data, window count, total count.
            return new PagingModel
            {
                Page = page,
                RecordCount = items.Count,
                RowData = items,
                RowType = projectedType,
                RowsPerPage = rowsPerPage,
                TotalCount = totalCount,
            };
        }

        private static string ToTraceString<T>(this IQueryable<T> query) =>
            query is ObjectQuery<T> oq
                ? oq.ToTraceString()
                : string.Empty;
    }
}
