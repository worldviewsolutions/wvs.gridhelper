﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Linq.Expressions;

using Xunit;
using FluentAssertions;
using WVS.GridHelper.Interfaces;

namespace WVS.GridHelper.Tests
{
    public class LinqFilterTests
    {
        protected readonly IQueryable<TestEntity> _datas = new List<TestEntity>
        {
            TestEntity1NotNull,
            TestEntity2NotNull,
            TestEntity3NullNestedPropertyAndNullsEverywhere,
            TestEntity4HasNestedPropertyButNullsEverywhereElse,
        }.AsQueryable();

        protected static LinqFilter Filterer = new LinqFilter();

        #region Fixture Validation
        [Fact]
        public void FixtureWorks()
        {
            // data source has data.
            _datas.Should().NotBeNullOrEmpty();
            _datas.Should().AllBeOfType<TestEntity>();

            // test filters work.
            const FilterOperator op = FilterOperator.EQUALS;
            const string v = "test";
            var f1 = FilterFactory<TestEntity, string>(x => x.String, op, v);
            f1.Operator.ShouldBeEquivalentTo(op);
            f1.DataType.ShouldBeEquivalentTo("string");
            f1.Values.Should().Contain(new[] {v});
            f1.Attribute.ShouldBeEquivalentTo(nameof(TestEntity.String));
        }
        #endregion

        #region Filter Test Cases

        [Fact]
        public void WhiteListDoesItsJob()
        {
            var keyName = nameof(TestEntity.NumberByte);

            var whiteList = GetWhiteListFromEntity<TestEntity>();

            // whitelist construction / fixture validation
            var originalKeys = whiteList.ToList();
            originalKeys.Should().NotBeNullOrEmpty()
                .And.HaveCount(typeof(TestEntity).GetProperties().Length)
                .And.Contain(keyName);

            // filter by something in whitelist works.
            var filterList = new FilterList(FilterListType.And) {
                FilterFactory<TestEntity, byte>(x => x.NumberByte, FilterOperator.CONTAINS, Numbers),
            };
            var filteredData = Filterer.ApplyFilters(_datas, filterList, whiteList, scopeFilters: null).ToList();
            filteredData.Should().NotBeNullOrEmpty();

            // can't filter by something not in the whitelist. use same query, different whitelist.
            var restrictedWhiteList = GetWhiteListFromEntity<TestEntity>(exceptNames: new[] {keyName});
            restrictedWhiteList.Should().NotContain(keyName);
            Action badwhitelistmatch = () => Filterer.ApplyFilters(_datas, filterList, restrictedWhiteList, scopeFilters: null);
            badwhitelistmatch.ShouldThrow<Exception>();
        }

        [Fact]
        public void WhiteListDoesItsJobNestedToo() {

            var keyName = nameof(TestEntity.NumberByte);

            var whiteList = TestEntityWhiteList();

            // whitelist construction / fixture validation
            var originalKeys = whiteList.ToList();
            originalKeys.Should().NotBeNullOrEmpty()
                .And.HaveCount(typeof(TestEntity).GetProperties().Length * 2)
                .And.Contain(keyName)
                .And.Contain($"{nameof(TestEntity.Nested)}.{keyName}");

            // filter by something in whitelist works.
            var filterList = new FilterList(FilterListType.And) {
                FilterFactory<TestEntity, string>(x => x.String, FilterOperator.CONTAINS, "string"),
                FilterFactory<TestEntity, string>(x => x.Nested.String, FilterOperator.STARTS_WITH, "some"),
                FilterFactory<TestEntity, string>(x => x.Nested.String, FilterOperator.CONTAINS, "string"),
                FilterFactory<TestEntity, byte>(x => x.NumberByte, FilterOperator.CONTAINS, Numbers),
            };
            var filteredData = Filterer.ApplyFilters(_datas, filterList, whiteList, scopeFilters: null).ToList();
            filteredData.Should().NotBeNullOrEmpty();

            // can't filter by something not in the whitelist. use same query, different whitelist.
            var restrictedWhiteList = TestEntityWhiteList(exceptNames: new[] { keyName });
            restrictedWhiteList.Should().NotContain(keyName);
            Action badwhitelistmatch = () => Filterer.ApplyFilters(_datas, filterList, restrictedWhiteList, scopeFilters: null);
            badwhitelistmatch.ShouldThrow<Exception>();
        }

        [Fact]
        public void FilterParamsAreNotRequired()
        {
            Action noparams = () => Filterer.ApplyFilters(_datas, null, null, null);
            noparams.ShouldNotThrow();

            var filterList = new FilterList(FilterListType.And) {
                FilterFactory<TestEntity, int>(x => x.NumberInt, FilterOperator.EQUALS, 1),
            };
            Action nowhitelist = () => Filterer.ApplyFilters(_datas, filterList, whiteList: null, scopeFilters: null);
            nowhitelist.ShouldNotThrow();
        }

        /* still problems with nullable values, ListContainsExpression needs to pick Contains from the nullable type if applicable, or else you get byte?[].Contains(byte) and explod.
        [Fact]
        public void FiltersDontMindNullAlongPathsToProperties() {
            var numbersNullable = numbers.Select(x => (byte?) x).ToList();
            numbersNullable.Should().NotBeNullOrEmpty();
            _datas.Where(x => x.Nested == null).Should().NotBeNullOrEmpty();
            _datas.Where(x => x.Nested != null && x.Nested.NumberByteNullable == null).Should().NotBeNullOrEmpty();
            _datas.Where(x => x.NumberByteNullable.HasValue && numbers.Contains(x.NumberByteNullable.Value)).Should().NotBeNullOrEmpty();
            var filterList = new FilterList(FilterListType.And) {
                FilterFactory<TestEntity, byte?>(x => x.Nested.NumberByteNullable, FilterOperator.CONTAINS, numbersNullable)
            };
            var filteredData = filterer.ApplyFilters(_datas, filterList, TestEntityWhiteList(), scopeFilters: null).ToList();
            filteredData.Should().NotBeEmpty();
        }
        */

        [Fact]
        public void FiltersCanBeNullAndNothingWillExplode()
        {
            const string because = "nulls mean no filtering, include everything";
            var blah = Filterer.ApplyFilters(_datas, filters: null, whiteList: null, scopeFilters: null).ToList();
            blah.Should().NotBeNullOrEmpty()
                .And.HaveCount(_datas.Count(), because);

            var filters = new FilterList(FilterListType.And) {
                FilterFactory<TestEntity, int>(x => x.Id, FilterOperator.GREATER_THAN, 0), // dummy filter, should not exclude anything.
            };
            var scopeFilters = new FilterList(FilterListType.And) {
                FilterFactory<TestEntity, int>(x => x.Id, FilterOperator.GREATER_THAN, 0), // dummy filter, should not exclude anything.
            };
            Filterer.ApplyFilters(_datas, null, null, scopeFilters).ToList().Should().HaveCount(_datas.Count(), because);
            Filterer.ApplyFilters(_datas, filters, null, null).ToList().Should().HaveCount(_datas.Count(), because);
            Filterer.ApplyFilters(_datas, filters, null, scopeFilters).ToList().Should().HaveCount(_datas.Count(), because);

            var noScopeFilters = new FilterList(FilterListType.And) {
                FilterFactory<TestEntity, int>(x => x.Id, FilterOperator.LESS_THAN, 0), // blocker filter, should not match anything.
            };
            Filterer.ApplyFilters(_datas, null, null, noScopeFilters).Should().HaveCount(0, "scope filters should still work");
        }

        [Fact]
        public void ScopeFiltersOverrideNormalFilters()
        {
            // search for an item we know exists.
            var filters = new FilterList(FilterListType.And) {
                FilterFactory<TestEntity, byte>(x => x.NumberByte, FilterOperator.GREATER_THAN, 0)
            };
            var filteredUnscoped = Filterer.ApplyFilters(_datas, filters, TestEntityWhiteList(), scopeFilters: null).ToList();
            filteredUnscoped.Should().NotBeNullOrEmpty();
            filteredUnscoped.FirstOrDefault(x => x.NumberByte == Number12).Should().NotBeNull("item should be in list, scope does not exclude it.");

            // now run same query with a scope that excludes the item we just got back.
            var scopeFilters = new FilterList(FilterListType.And) {
                FilterFactory<TestEntity, byte>(x => x.NumberByte, FilterOperator.DOES_NOT_CONTAIN, Number12),
            };
            var filteredScoped = Filterer.ApplyFilters(_datas, filters, TestEntityWhiteList(), scopeFilters).ToList();
            filteredScoped.Should().NotBeNullOrEmpty();
            filteredScoped.FirstOrDefault(x => x.NumberByte == Number12).Should().BeNull("item should be excluded from scope even tho filterList requested it.");
        }

        [Fact]
        public void NumberByteContains()
        {
            var filterList = new FilterList(FilterListType.And) {
                FilterFactory<TestEntity, byte>(x => x.NumberByte, FilterOperator.CONTAINS, Numbers)
            };
            var filteredData = Filterer.ApplyFilters(_datas, filterList, TestEntityWhiteList(), scopeFilters: null).ToList();
            filteredData.Should().NotBeEmpty();
        }

        [Fact]
        public void NumberByteContainsNot()
        {
            var filterer = new LinqFilter();
            var filterList = new FilterList(FilterListType.And) {
                FilterFactory<TestEntity, byte>(x => x.NumberByte, FilterOperator.CONTAINS, 234)
            };
            var filteredData = filterer.ApplyFilters(_datas, filterList, TestEntityWhiteList(), scopeFilters: null).ToList();
            filteredData.Should().BeEmpty();
        }

        [Fact]
        public void NumberByteEquals()
        {
            var filterList = new FilterList(FilterListType.And) {
                FilterFactory<TestEntity, byte>(x => x.NumberByte, FilterOperator.EQUALS, Number12)
            };
            var filtered = Filterer.ApplyFilters(_datas, filterList, TestEntityWhiteList(), scopeFilters: null).ToList();
            filtered.Should().NotBeNullOrEmpty();
            filtered.Where(x => x.NumberByte != Number12).Should().BeEmpty();
        }

        /* unsupported yet: testing for a null value explictly, filter construction fails.
        [Fact]
        public void NullableEquals() {
            var filterList = new FilterList(FilterListType.And) {
                FilterFactory<TestEntity, byte?>(x => x.NumberByte, FilterOperator.EQUALS, default(byte?))
            };
            var filtered = filterer.ApplyFilters(_datas, filterList, TestEntityWhiteList, scopeFilters: null).ToList();
            filtered.Should().NotBeNullOrEmpty();
            filtered.Where(x => x.NumberByteNullable != null).Should().BeEmpty();
        }
        */

        [Fact]
        public void AllFilterTypes()
        {
            var number = Number11;
            var filterList = new FilterList(FilterListType.And) {
                FilterFactory<TestEntity, string>(x => x.String, FilterOperator.EQUALS, "some string"),
                FilterFactory<TestEntity, string>(x => x.String, FilterOperator.DOES_NOT_EQUAL, "some other string"),
                FilterFactory<TestEntity, string>(x => x.String, FilterOperator.CONTAINS, "string"),
                FilterFactory<TestEntity, string>(x => x.String, FilterOperator.DOES_NOT_CONTAIN, "strung"),
                FilterFactory<TestEntity, string>(x => x.String, FilterOperator.STARTS_WITH, "some"),
                FilterFactory<TestEntity, string>(x => x.String, FilterOperator.ENDS_WITH, "string"),
                FilterFactory<TestEntity, byte>(x => x.NumberByte, FilterOperator.EQUALS, number),
                FilterFactory<TestEntity, byte?>(x => x.NumberByteNullable, FilterOperator.EQUALS, number),
                FilterFactory<TestEntity, short>(x => x.NumberShort, FilterOperator.EQUALS, number),
                FilterFactory<TestEntity, short?>(x => x.NumberShortNullable, FilterOperator.EQUALS, number),
                FilterFactory<TestEntity, int>(x => x.NumberInt, FilterOperator.EQUALS, number),
                FilterFactory<TestEntity, int?>(x => x.NumberIntNullable, FilterOperator.EQUALS, number),
                FilterFactory<TestEntity, long>(x => x.NumberLong, FilterOperator.EQUALS, number),
                FilterFactory<TestEntity, long?>(x => x.NumberLongNullable, FilterOperator.EQUALS, number),
                FilterFactory<TestEntity, float>(x => x.NumberFloat, FilterOperator.EQUALS, number),
                FilterFactory<TestEntity, float?>(x => x.NumberFloatNullable, FilterOperator.EQUALS, number),
                FilterFactory<TestEntity, double>(x => x.NumberDouble, FilterOperator.EQUALS, number),
                FilterFactory<TestEntity, double?>(x => x.NumberDoubleNullable, FilterOperator.EQUALS, number),
                FilterFactory<TestEntity, decimal>(x => x.NumberDecimal, FilterOperator.EQUALS, number),
                FilterFactory<TestEntity, decimal?>(x => x.NumberDecimalNullable, FilterOperator.EQUALS, number),
            };

            /*
            // DbFunctions not testable without mock dbcontext or switching out DbFunctions for an in-memory implementation https://stackoverflow.com/questions/28312094/unit-test-error-this-function-can-only-be-invoked-from-linq-to-entities
            filterList.Add(FilterFactory<TestEntity, DateTime>(x => x.DateTime, FilterOperator.ON, dateTime21));
            filterList.Add(FilterFactory<TestEntity, DateTime>(x => x.DateTime, FilterOperator.ON, dateTime22));
            */

            var datas = Filterer.ApplyFilters(_datas, filterList, TestEntityWhiteList(), scopeFilters: null).ToList();
            datas.Should().NotBeNullOrEmpty();
        }

        [Fact]
        public void NestedFilters()
        {
            var filterList = new FilterList(FilterListType.And) {
                FilterFactory<TestEntity, string>(x => x.String, FilterOperator.CONTAINS, "string"),
                FilterFactory<TestEntity, string>(x => x.Nested.String, FilterOperator.STARTS_WITH, "some"),
                FilterFactory<TestEntity, string>(x => x.Nested.String, FilterOperator.CONTAINS, "string"),
            };

            var whiteList = GetWhiteListFromEntity<TestEntity>(
                // include single level of nested property names
                alsoNames: GetWhiteListNamesFromEntity<TestEntity>().Select(n => $"{nameof(TestEntity.Nested)}.{n}")
            );

            var query = Filterer.ApplyFilters(_datas, filterList, whiteList, scopeFilters: null);
            var datas = query.ToList();
            datas.Should().NotBeNullOrEmpty();
        }

        // TODO add lots more!
        #endregion

        #region Test data
        private static TestEntity TestEntity1NotNull => TestEntityFactory(Number11, "some string", DateTime11);
        private static TestEntity TestEntity2NotNull => TestEntityFactory(Number12, "some other string", DateTime21);
        private static TestEntity TestEntity3NullNestedPropertyAndNullsEverywhere => TestEntityWithNullNullablesFactory(Number13, "some third string", DateTime22);
        private static TestEntity TestEntity4HasNestedPropertyButNullsEverywhereElse => TestEntityFactory(Number14, null, "some fourth string", DateTime22, null,
            nestedEntity: TestEntityWithNullNullablesFactory(Number14, "some fourth nested string", DateTime22));

        private static TestEntity TestEntityFactory(byte number, string str, DateTime dateTime) =>
            TestEntityFactory(number, number, str, dateTime, dateTime, nestedEntity:
                TestEntityFactory(number, number, str, dateTime, dateTime, nestedEntity: null)
                );
        private static TestEntity TestEntityWithNullNullablesFactory(byte number, string str, DateTime dateTime) =>
            TestEntityFactory(number, null, str, dateTime, null, nestedEntity: null);
        private static TestEntity TestEntityFactory(byte number, byte? nullableNumber, string str, DateTime dateTime, DateTime? dateTimeNullable, TestEntity nestedEntity) =>
            new TestEntity {
                Id = _id++,
                String = str,
                DateTime = dateTime,
                DateTimeNullable = dateTimeNullable,
                NumberByte = number, NumberDouble = number, NumberInt = number, NumberLong = number, NumberShort = number, NumberFloat = number, NumberDecimal = number,
                NumberByteNullable = nullableNumber, NumberDoubleNullable = nullableNumber, NumberIntNullable = nullableNumber, NumberLongNullable = nullableNumber, NumberShortNullable = nullableNumber, NumberFloatNullable = nullableNumber, NumberDecimalNullable = nullableNumber,
                Nested = nestedEntity,
            };
        private static int _id;

        private static DateTime DateTime11 => new DateTime(2010, 01, 15, 6, 0, 30, DateTimeKind.Local);
        private static DateTime DateTime21 => new DateTime(2012, 02, 26, 5, 1, 30, DateTimeKind.Local);
        private static DateTime DateTime22 => new DateTime(2012, 02, 26, 7, 30, 45, DateTimeKind.Local);
        private static byte Number11 => 11;
        private static byte Number12 => (byte) (Number11 + 2);
        private static byte Number13 => (byte) (Number12 + 4);
        private static byte Number14 => (byte) (Number13 + 7);
        private static IEnumerable<byte> Numbers => new List<byte> {Number11, Number12, Number13};
        #endregion

        #region Helpers
        private static IFilter FilterFactory<T, TValue>(Expression<Func<T, TValue>> selector, FilterOperator filterOperator, TValue value) =>
            FilterFactory(selector, filterOperator, new List<TValue> {value});
        private static IFilter FilterFactory<T, TValue>(Expression<Func<T, TValue>> selector, FilterOperator filterOperator, IEnumerable<TValue> values)
        {
            return new Filter
            {
                Attribute = selector.MemberPath(),
                DataType = selector.GetFilterDataTypeName(),
                Operator = filterOperator,
                Values = values.Select(x =>
                {
                    if (x is string xString)
                        return xString;
                    return x.ToString();
                }).ToList(),
            };
        }

        private IFilterWhitelist GetWhiteListFromEntity<T>(string name = "whitelist", IEnumerable<string> exceptNames = null, IEnumerable<string> alsoNames = null) {
            var names = GetWhiteListNamesFromEntity<T>();
            if (null != exceptNames)
                names = names.Except(exceptNames);
            if (null != alsoNames)
                names = names.Concat(alsoNames);
            var whitelist = new FilterWhitelist(name);
            whitelist.AddRange(names);
            return whitelist;
        }

        private IEnumerable<string> GetWhiteListNamesFromEntity<T>() =>
            typeof(T).GetProperties().Select(p => p.Name);

        private IFilterWhitelist TestEntityWhiteList(IEnumerable<string> exceptNames = null) => GetWhiteListFromEntity<TestEntity>(
            // include single level of nested property names
            alsoNames: GetWhiteListNamesFromEntity<TestEntity>().Select(n => $"{nameof(TestEntity.Nested)}.{n}"),
            exceptNames: exceptNames
        );

        protected class TestEntity
        {
            public int Id { get; set; }
            public string String { get; set; }
            public byte NumberByte { get; set; }
            public byte? NumberByteNullable { get; set; }
            public short NumberShort { get; set; }
            public short? NumberShortNullable { get; set; }
            public int NumberInt { get; set; }
            public int? NumberIntNullable { get; set; }
            public long NumberLong { get; set; }
            public long? NumberLongNullable { get; set; }
            public float NumberFloat { get; set; }
            public float? NumberFloatNullable { get; set; }
            public double NumberDouble { get; set; }
            public double? NumberDoubleNullable { get; set; }
            public decimal NumberDecimal { get; set; }
            public decimal? NumberDecimalNullable { get; set; }
            public DateTime DateTime { get; set; }
            public DateTime? DateTimeNullable { get; set; }

            public TestEntity Nested { get; set; }
        }
        #endregion
    }

    internal static class TestExtensions
    {
        /// <summary>
        /// Convert an actual type into <see cref="Filter.DataType"/> magic string.
        /// </summary>
        /// <typeparam name="TModel">class</typeparam>
        /// <typeparam name="TValue">property</typeparam>
        /// <param name="this">Property/field selector into TModel</param>
        /// <returns><see cref="Filter.DataType"/> string of TValue's type.</returns>
        /// <exception cref="NotImplementedException">Unsupported type.</exception>
        internal static string GetFilterDataTypeName<TModel, TValue>(this Expression<Func<TModel, TValue>> @this)
        {
            var t = typeof(TValue);
            if (t == typeof(string))
                return "string";
            if (t == typeof(DateTime) || t == typeof(DateTime?))
                return "datetime";
            if (t == typeof(bool) || t == typeof(bool?))
                return "boolean";
            if (t == typeof(byte) || t == typeof(byte?))
                return "byte";
            if (t == typeof(short) || t == typeof(short?))
                return "short";
            if (t == typeof(int) || t == typeof(int?))
                return "int";
            if (t == typeof(long) || t == typeof(long?))
                return "long";
            if (t == typeof(float) || t == typeof(float?))
                return "float";
            if (t == typeof(double) || t == typeof(double?))
                return "double";
            if (t == typeof(decimal) || t == typeof(decimal?))
                return "decimal";

            throw new NotImplementedException($"unsupported type: {t}");
        }
    }
}
