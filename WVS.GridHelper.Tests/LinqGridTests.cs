﻿using System.Collections.Generic;
using System.Linq;

using FluentAssertions;

using WVS.GridHelper.Interfaces;
using Xunit;

namespace WVS.GridHelper.Tests
{
    public class LinqGridTests
    {
        /*
        [Fact]
        public void NmpGridConstruction() {
            var grid = NmpGridFactory();
            grid.Columns.Should().NotBeNullOrEmpty();
            var firstCol = grid.Columns.First();
            firstCol.Meta.Should().NotBeNull();
            var firstColMetadata = firstCol.Meta;
            firstColMetadata.Title.Should().NotBeNullOrWhiteSpace();
            firstColMetadata.Name.Should().NotBeNullOrWhiteSpace();
            ;
            var pascalCasePropName = nameof(NmpWithPlannersOperators.ProgramYear);
            var programYearColMeta = grid.Columns.FirstOrDefault(x => x?.Metadata?.Name == nameof(NmpWithPlannersOperators.ProgramYear))?.Metadata;
            programYearColMeta.Should().NotBeNull();
            programYearColMeta.Title.Should().NotBe(pascalCasePropName);

            IGrid NmpGridFactory() => new GridBuild<NmpWithPlannersOperators>()
                .Col(x => x.ID, new ColumnMetadata { IsKey = true, Visible = false, Width = 50 })
                .Col(x => x.Plan_ID, new ColumnMetadata { Visible = false, Width = 50 })
                .Col(x => x.PlanIdentifier, "Plan #", new ColumnMetadata { Width = 100 })
                .Col(x => x.PrimaryNmpTypeName, "Type", new ColumnMetadata { Visible = false, Width = 90 })
                .Col(x => x.SubTypeName, "SubType", new ColumnMetadata { Visible = false, Width = 120 })
                .Col(x => x.ProgramYear, "Program Year", new ColumnMetadata { Visible = false, Width = 70 })
                .Col(x => x.CurrentNmpStep, "Status", new ColumnMetadata { Width = 85 })
                .Col(x => x.CountyNames, "Counties", new ColumnMetadata { Width = 115 })
                .Col(x => x.CountyName, "County", new ColumnMetadata { Visible = false, Width = 90 })
                .Col(x => x.CountyFIPS, "County FIPS", new ColumnMetadata { Visible = false, Width = 90 })
                .Col(x => x.FarmName, "Farm Name")
                .Col(x => x.FarmAddressStreet1, "Farm Address", new ColumnMetadata { Width = 85 })
                .Col(x => x.FarmAddressStreet2, "Farm Address 2", new ColumnMetadata { Visible = false, Width = 90 })
                .Col(x => x.FarmAddressCity, "Farm City", new ColumnMetadata { Visible = false, Width = 70 })
                .Col(x => x.FarmAddressStateCode, "Farm State", new ColumnMetadata { Visible = false, Width = 50 })
                .Col(x => x.FarmAddressZipCode, "Farm Zip", new ColumnMetadata { Visible = false, Width = 70 })
                .Col(x => x.PrimaryParticipantName, "Primary Participant", new ColumnMetadata { Width = 85 })
                .Col(x => x.OwnerName, "Owner Name", new ColumnMetadata { Visible = false, Width = 90 })
                .Col(x => x.OperatorName, "Operator Name", new ColumnMetadata { Visible = false, Width = 90 })
                .Col(x => x.PlannerName, "Planner Name", new ColumnMetadata { Width = 110 })
                .Col(x => x.StartsOn, "Plan Begin Date", new ColumnMetadata { Width = 60 })
                .Col(x => x.ApprovedOn, "Approval Date", new ColumnMetadata { Width = 60 })
                .Col(x => x.ExpiresOn, "Expiration Date", new ColumnMetadata { Width = 60 })
                .Col(x => x.CreatedOn, "Created On", new ColumnMetadata { Visible = false, Width = 60 })
                .Build();
        }
        */

        [Fact]
        public void NestedEntityGridConstruction() {
            var grid = TestEntity.GridFactory();
            grid.Columns.Should().NotBeNullOrEmpty();
            var dotCol = grid.Columns.FirstOrDefault(x => x?.Meta?.Index?.Contains(".") ?? false)?.Meta;
            dotCol.Should().NotBeNull();
        }

        [Fact]
        public void GetDynamic() {
            var grid = TestEntity.GridFactory();
            var data = TestEntity.TestData
                .AsQueryable();
            var query = grid.Project(data);
            var results = query
                .Cast<dynamic>()
                .ToList();
            results.Should().NotBeNullOrEmpty();
            results.Count.ShouldBeEquivalentTo(TestEntity.TestData.Count);
        }

        [Fact]
        public void TestGetDynamicLinqSelector() {
            var oneItem = new List<string> {"only"};
            var oneResult = Column.GetDynamicLinqSelector(oneItem, typeof(string));
            oneResult.ShouldBeEquivalentTo("@only");

            var valueItem = new List<string> {"Int"};
            var valueResult = Column.GetDynamicLinqSelector(valueItem, typeof(int));
            valueResult.ShouldBeEquivalentTo("Int32?(@Int)");

            var fourItems = new List<string> {
                "first",
                "second",
                "third",
                "last",
            };
            var fourResult = Column.GetDynamicLinqSelector(fourItems, typeof(string));
            fourResult.ShouldBeEquivalentTo("@first == null ? null : @first.second == null ? null : @first.second.third == null ? null : @first.second.third.last");

            var grid = TestEntity.GridFactory();
            var gridColPaths = grid.Columns.Select(c => c.DynamicLinqSelector);
            gridColPaths.Should().NotBeNullOrEmpty();
        }

        public class TestEntity
        {
            public byte Byte { get; set; }
            public byte? ByteNullable { get; set; }
            public string String { get; set; }
            public string Unspecified { get; set; }
            public TestEntity Nested { get; set; }

            public static IList<TestEntity> TestData => new List<TestEntity> {
                new TestEntity {
                    Byte = 1,
                    ByteNullable = 1,
                    String = "one",
                    Nested = new TestEntity {
                        Byte = 1,
                        String = "nested one",
                        Nested = new TestEntity {
                            Byte = 1,
                            String = "nested one one",
                        },
                    },
                },
                new TestEntity {
                    Byte = 2,
                    ByteNullable = null,
                    String = "two",
                    Nested = null,
                },
                new TestEntity {
                    Byte = 3,
                    String = null,
                    Nested = new TestEntity {
                        Byte = 1,
                        ByteNullable = null,
                        String = "nested three",
                    },
                },
            };

            public static Grid<TestEntity> GridFactory() => new GridBuild<TestEntity>()
                .Col(x => x.Byte)
                .Col(x => x.ByteNullable)
                .Col(x => x.String)
                .Col(x => x.Nested.Byte)
                .Col(x => x.Nested.ByteNullable)
                .Col(x => x.Nested.String)
                .Build();
        }
    }
}
