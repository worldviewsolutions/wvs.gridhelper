﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Linq.Expressions;
using System.Reflection;
namespace WVS.GridHelper
{
    using static ExpressionHelpers;
    public static class ExpressionExtensions
    {
        public static string MemberName<T, TProp>(this Expression<Func<T, TProp>> @this)
        {
            var memberExpression = @this.Body as MemberExpression;
            // TODO explodes if member is a value type being boxed into object: x => Convert(x.valueType)
            return memberExpression?.Member.Name;
        }

        /// <summary>
        /// Get path of a member expression, "A.B.C"
        /// </summary>
        public static string MemberPath<TModel, TValue>(this Expression<Func<TModel, TValue>> @this) =>
            string.Join(".", @this.MemberPathNames());

        public static IEnumerable<string> MemberPathNames<TModel, TValue>(this Expression<Func<TModel, TValue>> @this) =>
            PropertyPath<TModel>.Get(@this).Select(x => x.Name);

        public static string MemberTitle<T, TProp>(this Expression<Func<T, TProp>> @this) =>
            PascalCaseConverter.WordsFromPascalCase(@this.MemberName());

        /// <summary>
        /// returns path to a property, i.e. x => x.A.B.C returns {A,B,C}
        /// thanks https://stackoverflow.com/questions/1667408/c-getting-names-of-properties-in-a-chain-from-lambda-expression.
        /// </summary>
        private static class PropertyPath<TSource>
        {
            public static IReadOnlyList<MemberInfo> Get<TResult>(Expression<Func<TSource, TResult>> expression)
            {
                var visitor = new PropertyVisitor();
                visitor.Visit(expression.Body);
                visitor.Path.Reverse();
                return visitor.Path;
            }

            private class PropertyVisitor : ExpressionVisitor
            {
                internal readonly List<MemberInfo> Path = new List<MemberInfo>();

                protected override Expression VisitMember(MemberExpression node)
                {
                    if (!(node.Member is PropertyInfo))
                    {
                        throw new ArgumentException("The path can only contain properties", nameof(node));
                    }

                    this.Path.Add(node.Member);
                    return base.VisitMember(node);
                }
            }
        }
    }
}
