﻿using System;

namespace WVS.GridHelper.TypeScript
{
    [AttributeUsage(AttributeTargets.Enum | AttributeTargets.Class | AttributeTargets.Interface)]
    public class TypeScriptAttribute : Attribute { }
}
