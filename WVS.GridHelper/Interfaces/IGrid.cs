﻿using System;
using System.Collections.Generic;
using System.Linq;

namespace WVS.GridHelper.Interfaces
{
    public interface IGrid
    {
        Type EntityType { get; }
        IEnumerable<IColumn> Columns { get; }
        IList<GridAction> Actions { get; set; }
        IList<GridActionColumn> ActionColumns { get; set; }
        string DynamicSelector { get; }
        IQueryable Project(IQueryable query);
    }
}
