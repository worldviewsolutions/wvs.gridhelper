﻿using System;
using System.Collections.Generic;
using System.Text;

namespace WVS.GridHelper.Interfaces
{
    public interface IPagingModel
    {
        int Page { get; set; }
        int RecordCount { get; set; }
        int RowsPerPage { get; set; }
        int TotalCount { get; set; }
        IEnumerable<object> RowData { get; set; }
    }
}
