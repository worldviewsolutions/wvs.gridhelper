﻿using WVS.GridHelper.TypeScript;

namespace WVS.GridHelper.Interfaces
{
    [TypeScript]
    public enum Align
    {
        Unspecified,
        Left,
        Center,
        Right,
    }
}
