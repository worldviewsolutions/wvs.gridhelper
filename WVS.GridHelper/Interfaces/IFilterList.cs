﻿using System;
using System.Collections.Generic;
using System.Text;

namespace WVS.GridHelper.Interfaces
{
    public interface IFilterList : IList<IFilter>
    {
        bool StrictWhiteList { get; set; }
        FilterListType Type { get; set; }
    }
}
