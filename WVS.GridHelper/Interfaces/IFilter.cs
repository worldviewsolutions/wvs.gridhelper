﻿using System;
using System.Collections.Generic;
using System.Text;

namespace WVS.GridHelper.Interfaces
{
    public interface IFilter
    {
        string Attribute { get; set; }
        string DataType { get; set; }
        FilterOperator Operator { get; set; }
        List<string> Values { get; set; }
        StringComparison StringComparison { get; set; }
    }
}
