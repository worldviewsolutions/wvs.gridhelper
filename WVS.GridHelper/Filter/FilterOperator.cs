﻿namespace WVS.GridHelper
{
    public enum FilterOperator
    {
        AFTER,
        BEFORE,
        BETWEEN,
        CONTAINS,
        DOES_NOT_CONTAIN,
        DOES_NOT_EQUAL,
        ENDS_WITH,
        EQUALS,
        GREATER_THAN,
        GREATER_THAN_OR_EQUAL_TO,
        LESS_THAN,
        LESS_THAN_OR_EQUAL_TO,
        ON,
        STARTS_WITH
    }
}
