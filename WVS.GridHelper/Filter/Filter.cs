﻿using System;
using System.Collections.Generic;
using WVS.GridHelper.Interfaces;

namespace WVS.GridHelper
{
    public class Filter : IFilter
    {
        public string Attribute { get; set; }
        public string DataType { get; set; }
        public FilterOperator Operator { get; set; }
        public List<string> Values { get; set; }
        public StringComparison StringComparison { get; set; }
    }
}
