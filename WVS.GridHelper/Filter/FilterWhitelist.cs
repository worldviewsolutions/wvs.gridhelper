﻿using System;
using System.Collections.Generic;
using System.Text;
using WVS.GridHelper.Interfaces;

namespace WVS.GridHelper
{
    public class FilterWhitelist : List<string>, IFilterWhitelist
    {
        public FilterWhitelist(string name) {
            Name = name;
        }
        public string Name { get; set; }
    }
}
