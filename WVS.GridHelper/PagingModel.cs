﻿using System;
using System.Collections.Generic;
using WVS.GridHelper.Interfaces;

namespace WVS.GridHelper
{
    public class PagingModel : IPagingModel
    {
        public int Page { get; set; }
        public int RecordCount { get; set; }
        public int RowsPerPage { get; set; }
        public int TotalCount { get; set; }
        public IEnumerable<object> RowData { get; set; }
        public Type RowType { get; set; }
    }
}
