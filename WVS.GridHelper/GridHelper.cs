﻿using System.Linq;
using WVS.GridHelper.Interfaces;

namespace WVS.GridHelper
{
    public class GridAction
    {
        public string Title;
        public string FontAwesomeIcon;
        public string JavascriptCallback;
        public bool UseConfirmDialog;
        public string ConditionColumnName;
        public string ContentColumnName;
        public Align? ContentAlignment;

        public string ConfirmDialogText {
            get => UseConfirmDialog
                ? _confirmDialogText
                : null;
            set => _confirmDialogText = value;
        }
        private string _confirmDialogText;

        public static GridAction Delete(string javascriptCallbackFunction = null, string description = null) => new GridAction
        {
            Title = "Delete",
            FontAwesomeIcon = "trash",
            JavascriptCallback = javascriptCallbackFunction,
            UseConfirmDialog = true,
            ConfirmDialogText = $"Are you sure you want to delete{(string.IsNullOrEmpty(description) ? string.Empty : $" {description}")}?",
        };

        public static GridAction Edit(string javascriptCallbackFunction = null, string description = null) => new GridAction
        {
            Title = "Edit",
            FontAwesomeIcon = "pencil",
            JavascriptCallback = javascriptCallbackFunction,
            UseConfirmDialog = false,
        };
    }

    public class GridActionColumn {
        public string Title { get; set; }
        /// <summary>
        /// Text displayed for every link in the column. Defaults to using <see cref="Title"/>.
        /// </summary>
        public string LinkTitle { get; set; }
        /// <summary>
        /// HTML 'target' for link, i.e. '_blank' to open new tab/window.
        /// </summary>
        public string LinkTarget { get; set; }
        /// <summary>
        /// Full name of JS callback function executed when actionlink is clicked.
        /// </summary>
        public string JavascriptCallback { get; set; }
        /// <summary>
        /// Name of Column containing href of url for the link.
        /// </summary>
        public string UrlColumnName { get; set; }
        /// <summary>
        /// Link URL will be this, with {ColumnName} style parameters replaced client side.
        /// i.e. "http://example.com/api/route/{Id}/?param={OtherId}" will use the values of Id and OtherId columns.
        /// </summary>
        public string UrlFormat { get; set; }
        public Align Alignment { get; set; } = Align.Unspecified;
    }

    public static class GridQueryExtensions
    {
        public static IQueryable ProjectToGrid(this IQueryable query, IGrid grid) =>
            grid.Project(query);
    }
}
