﻿namespace WVS.GridHelper
{
    public class GridBuilderBase<TBuilder, TGrid>
    where TBuilder : GridBuilderBase<TBuilder, TGrid>
    where TGrid : class, new()
    {
        protected TGrid Obj;
        protected TBuilder Builder;
        protected GridBuilderBase()
        {
            Obj = new TGrid();
            Builder = (TBuilder)this;
        }
        public TGrid Build()
        {
            var result = Obj;
            Obj = null;
            return result;
        }
    }
}
