﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Linq.Expressions;
using System.Reflection;
using WVS.GridHelper.Interfaces;

namespace WVS.GridHelper
{
    internal class Column<TEntity, TMember> : Column, IColumn
        where TEntity : class
    {
        string IColumn.Path => Selector.MemberPath();

        public Expression<Func<TEntity, TMember>> Selector { get; }

        public string Name => Selector.MemberPath().Replace(".", "__");

        public string DynamicLinqSelector => $"{GetDynamicLinqSelector(Selector)} as {Name}";

        public string GetDynamicLinqSelector(Expression<Func<TEntity, TMember>> selector)
        {
            var pathNames = selector.MemberPathNames();
            return GetDynamicLinqSelector(pathNames, typeof(TMember));
        }

        public Column(Expression<Func<TEntity, TMember>> selector, Column.Metadata metadata)
            : this(selector, metadata?.Title ?? selector.MemberTitle(), metadata) { }

        public Column(Expression<Func<TEntity, TMember>> selector, string title, Column.Metadata metadata)
        {
            Selector = selector;
            var meta = metadata ?? new Column.Metadata();
            meta.Name = Name;
            meta.Index = selector.MemberPath();
            meta.Title = title;
            meta.Type = typeof(TMember);
            Meta = meta;
        }
    }

    public class Column
    {
        public Metadata Meta { get; set; }

        public static string GetDynamicLinqSelector(IEnumerable<string> pathItems, Type memberType)
        {
            var items = pathItems.ToArray();
            if (!items.Any())
                return null;
            if (items.Length == 1)
                return DynamicCastIfNeeded(items.First(), memberType);
            var inner = string.Empty;
            while (true)
            {
                if (!items.Any())
                    return inner;
                var itemsString = string.Join(".", items);
                items = items.Take(items.Length - 1).ToArray();
                inner = string.IsNullOrEmpty(inner)
                    ? DynamicCastIfNeeded(itemsString, memberType)
                    : $"@{itemsString} == null ? null : {inner}";
            }
        }

        private static string DynamicCastIfNeeded(string name, Type type) {
            var typeInfo = type.GetTypeInfo();
            var typeIsNullable = null != Nullable.GetUnderlyingType(type);
            return (typeInfo.IsValueType && !typeIsNullable)
                ? $"{type.Name}?(@{name})"
                : $"@{name}";
        }

        public class Metadata
        {
            public string Name { get; set; }

            public Type Type { get; set; }

            public bool Visible { get; set; } = true;

            public bool IsKey { get; set; }

            public int? Width { get; set; }

            public Align Alignment { get; set; } = Align.Unspecified;

            public int? DecimalPlaces { get; set; }

            public bool IsSortable { get; set; } = true;

            #region Editable Properties
            public bool Editable { get; set; } = false;
            public string EditType { get; set; }
            public string EditOptions { get; set; }
            public string EditRules { get; set; }
            public bool IsRequired { get; set; }
            public float? MinValue { get; set; }
            public float? MaxValue { get; set; }
            #endregion Editable Properties
            /// <summary>
            /// For JQGrid, for dates, this is fed to formatoptions; uses PHP date format conventions i.e. '3/2012' is 'n/Y'.
            /// Can be wired into other formats besides date, but just using string for now, lacking a clean way to wrap JQGrid's supported formats.
            /// PHP date formatting options: http://php.net/manual/en/function.date.php
            /// </summary>
            public string CustomFormat { get; set; }

            /// <summary>
            /// Render values using custom hardcoded formatter that will render boolean true/false as 'Yes'/'No'.
            /// </summary>
            public bool UseYesNoFormat { get; set; }

            public string Title {
                get => _title ?? Name;
                set => _title = value;
            }
            private string _title;

            public string Index {
                get => _index ?? Name;
                set => _index = value;
            }


            private string _index;
        }
    }
}
