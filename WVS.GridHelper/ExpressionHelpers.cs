﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Linq.Expressions;
using System.Reflection;
using System.Text;
using System.Text.RegularExpressions;

namespace WVS.GridHelper
{
    public static class ExpressionHelpers
    {
        /// <summary>
        /// Dummy expression to start an expression chain.
        /// </summary>
        public static Expression<Func<T, bool>> False<T>() => x => false;

        /// <summary>
        /// values.Contains(x.Member);
        /// Use in Where to get this SQL translation: <code>where Member in (value1, value2, ...)</code>
        /// </summary>
        /// <typeparam name="T">Table mapped type.</typeparam>
        /// <typeparam name="TValue">Column datatype of list and item to compare.</typeparam>
        /// <param name="predicate">Dummy expression to carry instance parameter, i.e. just the x in 'x => blah' will be taken.</param>
        /// <param name="memberPath">Name of column to check against list.</param>
        /// <param name="values">List of values that Member will be checked against.</param>
        /// <param name="invert">Negate output, 'not In'.</param>
        /// <returns>x => values.Contains(x.Member)</returns>
        public static Expression<Func<T, bool>> ListContainsExpression<T, TValue>
            (Expression<Func<T, bool>> predicate, string memberPath, IReadOnlyCollection<TValue> values, bool invert = false)
        {
            // x
            var param = predicate.Parameters.Single();

            // x.Member
            var memberAccessInfo = GetMemberExpressionFromPath<T>(param, memberPath);

            // .Contains()
            var contains = typeof(List<TValue>).GetMethod(nameof(List<TValue>.Contains));

            // values.Contains(x.Member)
            var methodCall = Expression.Call(Expression.Constant(values), contains, memberAccessInfo.memberExpression);

            // !values.Contains(x.Member) // if invert
            var negatedExpression = invert
                ? Expression.Not(methodCall)
                : (Expression)methodCall;

            // don't explode if something in the path is null, just return false.
            var safeComparison = SafeExpression(memberAccessInfo.nullCheckExpression, negatedExpression);

            // x => values.Contains(x.Member)
            return Expression.Lambda<Func<T, bool>>(safeComparison, param);
        }

        /// <summary>
        /// Turn nested property path into MemberExpression to access it.
        /// ALSO create a null check expression for the path leading up to the member, attach it to comparison expressions to access the member safely.
        /// </summary>
        /// <typeparam name="T">Base entity class.</typeparam>
        /// <param name="param">Parameter (the 'x' parameter in the lambda). This might not need to be passed in.</param>
        /// <param name="propertyPath">Dotted.Nested.Class.PathTo.Property</param>
        /// <returns>MemberExpression for accessing a member, and 'path is not null' expression for safe access.</returns>
        public static (MemberExpression memberExpression, Expression nullCheckExpression) GetMemberExpressionFromPath<T>(Expression param, string propertyPath)
        {
            var propertyNames = propertyPath.Split('.');
            var expression = param;
            var t = typeof(T);
            var memberExpressions = new List<MemberExpression>();
            foreach (var propertyName in propertyNames)
            {
                expression = Expression.Property(expression, t, propertyName);
                t = expression.Type;
                memberExpressions.Add((MemberExpression)expression);
            }

            Expression nullCheckExpression = null;
            foreach (var memberExpression in memberExpressions)
            {
                var nullableType = Nullable.GetUnderlyingType(memberExpression.Type);
                var typeInfo = memberExpression.Type.GetTypeInfo();
                if (typeInfo.IsValueType
                    && // isn't a nullable type, we can check for those even if they're wrapping value types.
                        null == nullableType
                    )
                    break; // value types cannot be checked for null.
                var @null = Expression.Constant(null, memberExpression.Type);
                var notNull = Expression.NotEqual(memberExpression, @null);
                nullCheckExpression = null == nullCheckExpression
                    ? notNull
                    : Expression.AndAlso(nullCheckExpression, notNull);
            }

            return (expression as MemberExpression, nullCheckExpression);
        }

        /// <summary>
        /// shortcut to wrapping an expression with a pre-constructed null-checking expression
        /// </summary>
        /// <param name="nullCheckExpression"></param>
        /// <param name="thenExpression"></param>
        /// <returns></returns>
        public static Expression SafeExpression(Expression nullCheckExpression, Expression thenExpression)
        {
            return null == nullCheckExpression
                ? thenExpression
                : Expression.AndAlso(nullCheckExpression, thenExpression);
        }

        #region ParameterRebinder
        public static Expression<Func<T, bool>> And<T>(this Expression<Func<T, bool>> first, Expression<Func<T, bool>> second) =>
            ParameterRebinder.Compose(first, second, Expression.And);

        public static Expression<Func<T, bool>> Or<T>(this Expression<Func<T, bool>> first, Expression<Func<T, bool>> second) =>
            ParameterRebinder.Compose(first, second, Expression.Or);

        /// <summary>
        /// Compose expressions, combine conditions.
        /// thanks https://blogs.msdn.microsoft.com/meek/2008/05/02/linq-to-entities-combining-predicates/ https://stackoverflow.com/questions/27732752/how-to-join-multiple-conditions-for-ef-expressions
        /// </summary>
        public class ParameterRebinder : ExpressionVisitor
        {
            private readonly Dictionary<ParameterExpression, ParameterExpression> _map;

            public ParameterRebinder(Dictionary<ParameterExpression, ParameterExpression> map)
            {
                _map = map ?? new Dictionary<ParameterExpression, ParameterExpression>();
            }
            protected override Expression VisitParameter(ParameterExpression p)
            {
                if (_map.TryGetValue(p, out var replacement))
                {
                    p = replacement;
                }
                return base.VisitParameter(p);
            }

            public static Expression ReplaceParameters(Dictionary<ParameterExpression, ParameterExpression> map, Expression exp)
            {
                return new ParameterRebinder(map).Visit(exp);
            }

            public static Expression<T> Compose<T>(Expression<T> first, Expression<T> second, Func<Expression, Expression, Expression> merge)
            {
                // build parameter map (from parameters of second to parameters of first)
                var map = first.Parameters.Select((f, i) => new { f, s = second.Parameters[i] }).ToDictionary(p => p.s, p => p.f);
                // replace parameters in the second lambda expression with parameters from the first
                var secondBody = ParameterRebinder.ReplaceParameters(map, second.Body);
                // apply composition of lambda expression bodies to parameters from the first expression 
                return Expression.Lambda<T>(merge(first.Body, secondBody), first.Parameters);
            }
        }
        #endregion

        /// <summary>
        /// Get value of an expression, do null checking along the path. If B is null, then x => x.A.B.C returns null instead of exploding.
        /// Modified to box value types that are null, so if C is a byte but B is null, C returns null anyway.
        /// Not helpful for construction EF expressions, but very helpful for rendering grid data where you need null values for unreachable columns.
        /// Found on stackoverflow https://stackoverflow.com/a/4281533/3320154
        /// </summary>
        public static class NullHandlingGetValue
        {
            /// <summary>
            /// Returns the value specified by the expression or Null or the default value of the expression's type if any of the items in the expression
            /// return null. Use this method for handling long property chains where checking each intermdiate value for a null would be necessary.
            /// </summary>
            public static object GetValueOrDefault<TObject, TResult>(TObject instance, Expression<Func<TObject, TResult>> expression)
                where TObject : class
            {
                return GetValue(instance, expression.Body);
            }

            private static object GetValue(object value, Expression expression)
            {
                object result;

                if (value == null) return null;

                switch (expression.NodeType)
                {
                    case ExpressionType.Parameter:
                        return value;

                    case ExpressionType.MemberAccess:
                        var memberExpression = (MemberExpression)expression;
                        result = GetValue(value, memberExpression.Expression);

                        return result == null ? null : GetValue(result, memberExpression.Member);

                    case ExpressionType.Call:
                        var methodCallExpression = (MethodCallExpression)expression;

                        if (!SupportsMethod(methodCallExpression))
                            throw new NotSupportedException($"{methodCallExpression.Method} is not supported");

                        result = GetValue(value, methodCallExpression.Method.IsStatic
                                                     ? methodCallExpression.Arguments[0]
                                                     : methodCallExpression.Object);
                        return result == null
                                   ? null
                                   : GetValue(result, methodCallExpression.Method);

                    case ExpressionType.Convert:
                        var unaryExpression = (UnaryExpression)expression;

                        return Convert(GetValue(value, unaryExpression.Operand), unaryExpression.Type);

                    default:
                        throw new NotSupportedException($"{expression.GetType()} not supported");
                }
            }

            private static object Convert(object value, Type type)
            {
                return Expression.Lambda(Expression.Convert(Expression.Constant(value), type)).Compile().DynamicInvoke();
            }

            private static object GetValue(object instance, MemberInfo memberInfo)
            {
                if (memberInfo is FieldInfo fieldInfo)
                    return fieldInfo.GetValue(instance);
                if (memberInfo is MethodBase @base)
                    return GetValue(instance, @base);
                if (memberInfo is PropertyInfo propertyInfo)
                    return GetValue(instance, propertyInfo);
                throw new NotSupportedException($"{memberInfo} not supported");
            }

            private static object GetValue(object instance, PropertyInfo propertyInfo)
            {
                return propertyInfo.GetGetMethod(true).Invoke(instance, null);
            }

            private static object GetValue(object instance, MethodBase method)
            {
                return method.IsStatic
                           ? method.Invoke(null, new[] { instance })
                           : method.Invoke(instance, null);
            }

            private static bool SupportsMethod(MethodCallExpression methodCallExpression)
            {
                return (methodCallExpression.Method.IsStatic && methodCallExpression.Arguments.Count == 1) || (methodCallExpression.Arguments.Count == 0);
            }
        }

        /// <summary>
        /// Convert "PascalCaseStuffHTML" into "Pascal Case Stuff HTML" i.e. without mangling abbreviations.
        /// PascalCase splitting regex found on stackoverflow of course: https://stackoverflow.com/a/5796793/3320154
        /// There are a lot of solutions to this problem, most involving regex, this one seems to work pretty well.
        /// </summary>
        public static class PascalCaseConverter
        {
            public static string WordsFromPascalCase(string name)
            {
                if (string.IsNullOrEmpty(name))
                    return string.Empty;
                return SplitPascalCaseRegexOuter.Replace(
                    SplitPascalCaseRegexInner.Replace(name, "$1 $2"),
                    "$1 $2");
            }
            private static readonly Regex SplitPascalCaseRegexInner = new Regex(@"(\P{Ll})(\P{Ll}\p{Ll})", RegexOptions.Compiled);
            private static readonly Regex SplitPascalCaseRegexOuter = new Regex(@"(\p{Ll})(\P{Ll})", RegexOptions.Compiled);
        }
    }
}
