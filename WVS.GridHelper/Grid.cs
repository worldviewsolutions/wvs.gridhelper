﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Linq.Dynamic.Core;

using WVS.GridHelper.Interfaces;

namespace WVS.GridHelper
{
    public class Grid<TEntity> : IGrid
        where TEntity : class
    {
        public Type EntityType { get; } = typeof(TEntity);
        public IList<GridAction> Actions { get; set; } = new List<GridAction>();
        public IList<GridActionColumn> ActionColumns { get; set; } = new List<GridActionColumn>();
        public IEnumerable<IColumn> Columns { get; internal set; } = new List<Column<TEntity, dynamic>>();
        public string DynamicSelector =>
            $"new({string.Join(", ", Columns.Select(c => c.DynamicLinqSelector))})";

        public IQueryable Project(IQueryable query)
        {
            var selector = DynamicSelector;
            var result = query.Select(selector);
            return result;
        }
    }
}
