﻿using System.Web.Mvc;
using System.Web.WebPages;

namespace WVS.GridHelper.Front.JQGrid.MVC.View
{
    using JQGrid;

    public static class JQGridRender {
        public static HelperResult Render(IJQGrid gridConfiguration, WebViewPage pageContext, string css = null, string js = null) {
            var v = new Views.Shared.JQGridPartial_ {ViewContext = pageContext.ViewContext};
            return v.RenderJQGrid(pageContext, gridConfiguration, css, js);
        }
    }
}
