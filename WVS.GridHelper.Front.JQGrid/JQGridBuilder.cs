﻿namespace WVS.GridHelper.Front.JQGrid
{
    public class JQGridBuild<TEntity> : JQGridBuilder<JQGridBuild<TEntity>, JQGrid<TEntity>, TEntity>
        where TEntity : class
    { }

    /// <summary>
    /// Use <see cref="JQGridBuild{TEntity}"/> instead.
    /// </summary>
    // ReSharper disable once InheritdocConsiderUsage
    public class JQGridBuilder<TBuilder, TGrid, TEntity> : GridBuilder<TBuilder, TGrid, TEntity>
        where TBuilder : JQGridBuilder<TBuilder, TGrid, TEntity>
        where TGrid : JQGrid<TEntity>, new()
        where TEntity : class
    {
        public TBuilder URL(string url) {
            Obj.DataUrl = url;
            return Builder;
        }

        public TBuilder ContainerId(string containerId) {
            Obj.HtmlIdContainer = containerId;
            return Builder;
        }
    }
}
