﻿using System;
using System.Collections.Generic;
using System.Diagnostics.CodeAnalysis;

namespace WVS.GridHelper.Front.JQGrid {
    using TypeScript;
    using static Extensions;

    /// <summary>
    /// Serializes directly to json for custom actions used in JQGridHelper.cshtml.
    /// </summary>
    [SuppressMessage("ReSharper", "InconsistentNaming")]
    [TypeScript]
    public class JQGridActionModel
    {
        public string title { get; set; }
        public string fontAwesomeIcon { get; set; }
        public string javascriptCallback { get; set; }
        public bool useConfirmDialog { get; set; }
        public string confirmDialogText { get; set; }
        public string conditionColumnName { get; set; }
        public int? conditionColumnIndex { get; set; }
        public string contentColumnName { get; set; }
        public int? contentColumnIndex { get; set; }
        public int? contentAlignment { get; set; }

        internal static JQGridActionModel FromAction(IJQGrid grid, GridAction action)
        {
            var conditionColumnIndex = ColumnIndex(action, grid, x => x.ConditionColumnName);
            var contentColumnIndex = ColumnIndex(action, grid, x => x.ContentColumnName);
            var model = new JQGridActionModel
            {
                title = action.Title,
                fontAwesomeIcon = action.FontAwesomeIcon,
                javascriptCallback = action.JavascriptCallback,
                useConfirmDialog = action.UseConfirmDialog,
                confirmDialogText = action.ConfirmDialogText?.Replace("\'", string.Empty), // sanitize for JS/HTML injection. maybe some encoding would work instead of just stripping it.
                conditionColumnIndex = conditionColumnIndex,
                conditionColumnName = conditionColumnIndex.HasValue
                    ? action.ConditionColumnName
                    : null, // eat condition if we couldn't find its index. prevent conditions only working sometimes.
                contentColumnIndex = contentColumnIndex,
                contentColumnName = contentColumnIndex.HasValue
                    ? action.ContentColumnName
                    : null,
                contentAlignment = (int?) action.ContentAlignment
            };
            return model;
        }

        /// <summary>
        /// Get index of column by name from the grid model.
        /// </summary>
        private static int? ColumnIndex(GridAction gridAction, IJQGrid gridConfiguration, Func<GridAction, string> columnNameSelector) =>
            IndexOf(gridConfiguration.ColNames, columnNameSelector(gridAction));
    }
}
