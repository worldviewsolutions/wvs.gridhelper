﻿using System;
using System.Collections.Generic;
using System.Dynamic;
using System.Linq;
using System.Reflection;
using WVS.GridHelper.Interfaces;

namespace WVS.GridHelper.Front.JQGrid
{
    public static class Extensions
    {
        public static JQGridPagingModel ToJQGridFormat(this PagingModel @this, IGrid gridConfiguration)
        {
            var totalPageCount = @this.TotalCount > 0
                ? (int)Math.Ceiling(@this.TotalCount / (double)@this.RowsPerPage)
                : 0;

            // find key column
            var keyColumn = gridConfiguration.Columns.SingleOrDefault(col => col.Meta.IsKey);
            if (null == keyColumn)
                throw new InvalidOperationException("Key column not found.");

            // anon type is a pain in the butt to iterate over properties, blow it into a bunch of expandoobjects instead.
            var expandoRows = @this.RowData.Select(data =>
            {
                var expando = new ExpandoObject() as IDictionary<string, object>;
                foreach (var propertyInfo in @this.RowType.GetProperties())
                {
                    if (propertyInfo.DeclaringType != @this.RowType)
                        continue; // Linq.Dynamic.Core includes an extra 'Item' property on its anon types, exclude anything not declared by our custom type.
                    /*
                    if (propertyInfo.DeclaringType == typeof(System.Linq.Dynamic.Core.DynamicClass))
                        continue; // dunno why Linq.Dynamic.Core includes this 'Item' property on its anon types, but exclude it.
                    */
                    expando[propertyInfo.Name] = propertyInfo.GetValue(data);
                }
                return expando;
            });

            // rows and data.
            var rows = expandoRows.Select(row => new {
                id = row[keyColumn.Name],// keySelector.DynamicInvoke(s),
                cell = gridConfiguration.Columns.Select(col => row[col.Name]),//> row propSelectors.Select(p => p.Value.DynamicInvoke(s))
            });

            // return PagingModel in jqGrid format.
            return new JQGridPagingModel
            {
                page = @this.Page,
                records = @this.TotalCount, // Number of all records
                total = totalPageCount, // Total pages
                rows = rows,
            };
        }

        internal static string JQGridAlignmentTransform(Align v) {
            switch (v) {
                case Align.Left:
                    return "left";
                case Align.Center:
                    return "center";
                case Align.Right:
                    return "right";
            }
            return null;
        }
        internal static IEnumerable<KeyValuePair<string, string>> JQGridEditOptionsTransform(string eo)
        {
            var trim = eo.TrimStart('{').TrimEnd('}');
            var kvps = trim.Split(';');
            var splitKvps = new List<KeyValuePair<string, string>>();
            foreach (var kvp in kvps)
            {
                var x = kvp.Split(new[] {':'}, 2);
                splitKvps.Add(new KeyValuePair<string,string>(x[0].Trim(), x[1].Trim()));
            }
            return splitKvps;
        }
        internal static int? IndexOf(IEnumerable<string> list, string value) {
            var index = 0;
            foreach (var item in list) {
                if (item == value)
                    return index;
                index++;
            }
            return null;
        }
    }
}
