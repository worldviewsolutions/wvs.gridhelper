﻿using System;
using System.ComponentModel.DataAnnotations;
using System.Diagnostics.CodeAnalysis;
using WVS.GridHelper.TypeScript;

namespace WVS.GridHelper.Front.JQGrid
{
    /// <summary>
    /// Serializes directly to json for jqGrid colModel.
    /// </summary>
    /// <reference path="JQGridColModelFormatOptions.cs.d.ts" />
    /// <reference path="JQGridColModelEditOptions.cs.d.ts" />
    [SuppressMessage("ReSharper", "InconsistentNaming")]
    [TypeScript]
    public class JQGridColModel
    {
        [Required]
        public string name { get; set; }
        public bool? sortable { get; set; } = true;
        public bool? editable { get; set; } = false;
        public string edittype { get; set; }
        public JQGridColModelEditOptions editoptions { get; set; }
        public JQGridColModelEditRules editrules { get; set; }
        public string index { get; set; }
        public int? width { get; set; }
        public bool? @fixed { get; set; }
        public bool? hidden { get; set; } = false;
        public string align { get; set; }
        public string formatter { get; set; } // mixed: can be text or a function.
        public bool? key { get; set; }
        public JQGridColModelFormatOptions formatoptions { get; set; }
        public string defaultvalue { get; set; }
    }
}
