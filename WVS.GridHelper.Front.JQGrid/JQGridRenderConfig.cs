﻿using System;
using System.Collections.Generic;
using System.Linq;
using WVS.GridHelper.TypeScript;

namespace WVS.GridHelper.Front.JQGrid {
    [TypeScript]
    public class JQGridRenderConfig {
        public string gridMatchWidthId { get; set; }
        public string gridId { get; set; }
        public string pagerId { get; set; }
        public JQGridModel colData { get; set; }
        public  IEnumerable<JQGridActionModel> actions { get; set; }
        public IEnumerable<JQGridActionColumnModel> actionColumns { get; set; }
        public string gridKeyName { get; set; }
        public string gridDataUrl { get; set; }

        /// <summary>
        /// 'POST' | 'GET'
        /// </summary>
        public string gridDataMethod { get; set; }

        public string onRowSelect { get; set; }
        public bool hasSubGrid { get; set; }
        public string onSubGridRowExpanded { get; set; }
    }
}
