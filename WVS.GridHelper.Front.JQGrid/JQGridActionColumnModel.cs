﻿using System;
using System.Diagnostics.CodeAnalysis;
using System.Linq;

namespace WVS.GridHelper.Front.JQGrid {
    using TypeScript;
    using static Extensions;

    /// <summary>
    /// Serializes directly to json for custom actions used in JQGridHelper.cshtml.
    /// </summary>
    [SuppressMessage("ReSharper", "InconsistentNaming")]
    [TypeScript]
    public class JQGridActionColumnModel {

        public string title { get; set; }
        public string linkTitle { get; set; }
        public string linkTarget { get; set; }
        public string javascriptCallback { get; set; }
        public string urlColumnName { get; set; }
        public int? urlColumnIndex { get; set; }
        public string alignment { get; set; }
        public string urlFormat { get; set; }

        internal static JQGridActionColumnModel FromAction(IJQGrid grid, GridActionColumn c)
        {
            var urlColumnIndex = ColumnIndex(c, grid, x => x.UrlColumnName);
            var model = new JQGridActionColumnModel
            {
                title = c.Title,
                linkTitle = c.LinkTitle ?? c.Title,
                linkTarget = c.LinkTarget,
                javascriptCallback = c.JavascriptCallback,
                alignment = Extensions.JQGridAlignmentTransform(c.Alignment),
                urlColumnName = c.UrlColumnName,
                urlColumnIndex = urlColumnIndex,
                urlFormat = c.UrlFormat,
            };
            return model;
        }

        /// <summary>
        /// Get index of ActionColumn by name from the grid model.
        /// </summary>
        private static int? ColumnIndex(GridActionColumn gridActionColumn, IJQGrid gridConfiguration, Func<GridActionColumn, string> columnNameSelector) =>
            IndexOf(gridConfiguration.ColNames, columnNameSelector(gridActionColumn));
    }
}
