﻿using System;
using System.Collections.Generic;
using System.Text;
// ReSharper disable InconsistentNaming

namespace WVS.GridHelper.Front.JQGrid
{
    public class JQGridColModelEditRules
    {
        //http://www.trirand.com/jqgridwiki/doku.php?id=wiki:common_rules#editrules

        public bool required { get; set; }
        public bool number { get; set; }
        public bool integer { get; set; }
        public float? minValue { get; set; }
        public float? maxValue { get; set; }
        public bool date { get; set; }
    }
}
