﻿using System.Collections.Generic;
using System.Diagnostics.CodeAnalysis;
using WVS.GridHelper.TypeScript;

namespace WVS.GridHelper.Front.JQGrid {
    [SuppressMessage("ReSharper", "InconsistentNaming")]
    [TypeScript]
    public class JQGridPagingModel
    {
        public int page { get; set; }
        public int records { get; set; }
        public int total { get; set; }
        public IEnumerable<object> rows { get; set; }
    }
}
