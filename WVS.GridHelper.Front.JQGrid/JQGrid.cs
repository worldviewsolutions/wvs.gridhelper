﻿using WVS.GridHelper.Interfaces;

namespace WVS.GridHelper.Front.JQGrid
{
    using System;
    using System.Collections.Generic;
    using System.Linq;
    using static Extensions;

    /// <summary>
    /// Semi-mostly-automated/encapsulated jqGrid helper.
    /// To use, make a <see cref="Grid{TRow}"/> in the viewmodel and render the "JQGridHelper" partial with it.
    /// Use <see cref="Grid{TRow}.HtmlIdGrid"/> to get the html id of the grid, for use with custom javascript.
    /// Note <seealso cref="IJQGrid"/> is needed to pass the grid to the partial as a model.
    /// </summary>
    public class JQGrid<TEntity> : Grid<TEntity>, IJQGrid
        where TEntity : class
    {
        #region Public Stuff
        public string DataUrl { get; set; }
        public DataMethodType? DataMethod { get; set; } = DataMethodType.Post;
        /// <summary>
        /// Html ID of a container div that the grid will match width with. Also informs the html ids generated for the grid and its pager.
        /// </summary>
        public string HtmlIdContainer { get; set; }
        public string HtmlIdGrid => $"{HtmlIdContainer}Grid";
        public string HtmlIdGridPager => $"{HtmlIdContainer}GridPager";
        public string OnRowSelect { get; set; }
        public string OnSubGridRowExpanded { get; set; }
        #endregion
        public bool HasSubGrid { get; set; }
      
        #region JQGrid ColModel
        public IEnumerable<string> JQGridColNames()
        {
            var titles = Titles();
            if (ActionColumns.Any())
                titles = titles.Concat(ActionColumns.Select(c => c.Title));
            if (Actions.Any())
                titles = titles.Concat(new[] { "Actions" });
            return titles;
        }

        public IEnumerable<string> ColNames =>
            Columns.Select(x => x.Meta.Name);

        public IEnumerable<string> Titles() =>
            Columns.Select(x => x.Meta.Title);

        public string KeyName {
            get {
                var key = Columns.FirstOrDefault(x => x.Meta.IsKey)?
                    .Meta ?? Columns.FirstOrDefault()?.Meta;
                return key?.Name;
            }
        }

        public IEnumerable<JQGridActionModel> JQGridActionModels() =>
            Actions.Select(action => JQGridActionModel.FromAction(this, action));

        public IEnumerable<JQGridColModel> JQGridColModels() =>
            Columns.Select(x => JQGridColModelTransform(x.Meta));

        public IEnumerable<JQGridActionColumnModel> JQGridActionColumnModels() =>
            ActionColumns.Select(x => JQGridActionColumnModel.FromAction(this, x));

        public JQGridModel JQGridModel()
        {
            var gridModel = new JQGridModel
            {
                colNames = JQGridColNames(),
                colModel = JQGridColModels(),
            };
            return gridModel;
        }

        /// <summary>
        /// Translates metadata about the column into jqGrid's colModel format.
        /// See http://www.trirand.com/jqgridwiki/doku.php?id=wiki:colmodel_options but note that this documentation refers to the newest version, and not the free version we're stuck with.
        /// </summary>
        /// <returns>Object suitable for JSON serialization and use in jqGrid colModel.</returns>
        private static JQGridColModel JQGridColModelTransform(Column.Metadata c)
        {
            var m = new JQGridColModel
            {
                name = c.Name,
                sortable = c.IsSortable,
        };

            if (c.IsKey)
                m.key = true;

            if (c.Type == typeof(DateTime) || c.Type == typeof(DateTime?) || c.Type == typeof(DateTimeOffset) || c.Type == typeof(DateTimeOffset?))
            {
                m.formatter = "date";
                m.formatoptions = new JQGridColModelFormatOptions
                {
                    srcformat = "ISO8601Long",
                    newformat = c.CustomFormat
                };
                m.align = "center";
            }

            if (c.Width.HasValue)
            {
                m.width = c.Width.Value;
                m.@fixed = true;
            }

            if (!c.Visible)
                m.hidden = true;

            if (!string.IsNullOrEmpty(c.Index))
                m.index = c.Index;

            if (c.UseYesNoFormat)
                m.formatter = "yesno";

            if (c.DecimalPlaces.HasValue)
            {
                m.formatter = "number";
                m.defaultvalue = "";
                m.formatoptions = new JQGridColModelFormatOptions
                {
                    decimalPlaces = c.DecimalPlaces.Value,
                };
            }

            if (c.Editable)
            {
                m.editable = c.Editable;
                //only worry about the rest of this if it is indeed editable
                m.edittype = c.EditType;
                m.editrules = new JQGridColModelEditRules
                {
                    integer = (c.Type == typeof(Int16) || c.Type == typeof(Int32) || c.Type == typeof(Int64)),
                    date = (c.Type == typeof(DateTime) || c.Type == typeof(DateTime?) || c.Type == typeof(DateTimeOffset) || c.Type == typeof(DateTimeOffset?)),
                    number = (c.Type == typeof(Decimal) || c.Type == typeof(Single)),
                    minValue = c.MinValue,
                    maxValue = c.MaxValue,
                    required = c.IsRequired
                };

                if (!string.IsNullOrEmpty(c.EditOptions))
                {
                    if (c.EditType == "select")
                    {
                        //TODO dataURL might be a better option than editoptions, but this is working for now
                        m.editoptions = new JQGridColModelEditOptionsSelect
                        {
                            value = c.EditOptions
                        };
                    }
                    var parseEditOptions = JQGridEditOptionsTransform(c.EditOptions);
                }
            }
            
            m.align = JQGridAlignmentTransform(c.Alignment);

            return m;
        }

        #endregion
    }
}
