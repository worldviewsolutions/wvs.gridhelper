﻿using System;
using System.Diagnostics.CodeAnalysis;
using WVS.GridHelper.TypeScript;

namespace WVS.GridHelper.Front.JQGrid
{
    /// <summary>
    /// JQ Grid column editing options.
    /// Serializes directly to json for jqGrid colModel.
    /// </summary>
    [SuppressMessage("ReSharper", "InconsistentNaming")]
    [TypeScript]
    public class JQGridColModelEditOptions
    {
        public string dataInit { get; set; }
    }
}
