﻿using System.Diagnostics.CodeAnalysis;
using WVS.GridHelper.TypeScript;

namespace WVS.GridHelper.Front.JQGrid {
    /// <summary>
    /// see http://www.trirand.com/jqgridwiki/doku.php?id=wiki:predefined_formatter
    /// </summary>
    [SuppressMessage("ReSharper", "InconsistentNaming")]
    [TypeScript]
    public class JQGridColModelFormatOptions {
        public int? decimalPlaces { get; set; }
        public string srcformat { get; set; }
        public string newformat { get; set; }
    }
}
