﻿using System.Collections.Generic;
using WVS.GridHelper.Interfaces;

namespace WVS.GridHelper.Front.JQGrid {
    public interface IJQGrid : IGrid
    {
        string DataUrl { get; set; }
        DataMethodType? DataMethod { get; set; }

        string HtmlIdContainer { get; set; }
        string OnRowSelect { get; set; }
        string OnSubGridRowExpanded { get; set; }
        string HtmlIdGrid { get; }
        string HtmlIdGridPager { get; }

        // for GridAction.ConditionColumn implementation.
        IEnumerable<string> ColNames { get; }
        string KeyName { get; }

        // jqGrid specific stuff.
        IEnumerable<string> JQGridColNames();
        IEnumerable<JQGridColModel> JQGridColModels();
        JQGridModel JQGridModel();
        IEnumerable<JQGridActionModel> JQGridActionModels();
        IEnumerable<JQGridActionColumnModel> JQGridActionColumnModels();
        //subGrid stuff
        bool HasSubGrid { get; set; }

    }
}
