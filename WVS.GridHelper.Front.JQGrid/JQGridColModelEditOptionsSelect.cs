﻿using System;
using System.Diagnostics.CodeAnalysis;
using WVS.GridHelper.TypeScript;

namespace WVS.GridHelper.Front.JQGrid
{
    /// <summary>
    /// JQ Grid column editing options.
    /// Serializes directly to json for jqGrid colModel.
    /// Edit field will be input type="text" with the properties in this model applied to the input attributes.
    /// </summary>
    [SuppressMessage("ReSharper", "InconsistentNaming")]
    [TypeScript]
    public class JQGridColModelEditOptionsSelect : JQGridColModelEditOptions
    {
        public string value { get; set; }
    }
}
