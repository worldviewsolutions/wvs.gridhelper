﻿namespace WVS.GridHelper.Front.JQGrid
{
    [TypeScript.TypeScript]
    public enum DataMethodType
    {
        Get = 0,
        Post,
    }
}
