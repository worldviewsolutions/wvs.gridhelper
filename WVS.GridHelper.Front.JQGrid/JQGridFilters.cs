﻿using System;
using System.Collections.Generic;

namespace WVS.GridHelper.Front.JQGrid
{
    /// <summary>
    /// Translate between JQGrid's filter(s) and WVS.GridHelper
    /// TODO support multiple filters (jqGrid searchGrid multipleSearch=true)
    /// </summary>
    public static class JQGridFilters
    {
        public static (string orderBy, bool orderAsc, int pageNum) TransformParams(string sidx, string sord, int? page, string orderByDefault = null) =>
            (string.IsNullOrEmpty(sidx)
                ? orderByDefault
                : sidx,
            sord?.ToLower().StartsWith("asc") ?? true,
            !page.HasValue || page < 1
                ? 1
                : page.Value);

        public static Filter TransformFilter(string searchField, string searchOper, string searchString)
        {
            if (string.IsNullOrWhiteSpace(searchField)
                || string.IsNullOrWhiteSpace(searchOper)
                || string.IsNullOrWhiteSpace(searchString)
                || !FilterOperators.ContainsKey(searchOper))
                return null;

            const string dataType = "string"; // HACK
            const StringComparison stringComparison = StringComparison.CurrentCultureIgnoreCase; // HACK
            var searchOpererator = FilterOperators[searchOper];
            return new Filter
            {
                Operator = searchOpererator,
                Attribute = searchField,
                DataType = dataType,
                Values = new List<string> { searchString },
                StringComparison = stringComparison,
            };
        }

        public static readonly Dictionary<string, FilterOperator> FilterOperators = new Dictionary<string, FilterOperator>
        {
            { "eq", FilterOperator.EQUALS },
            { "cn", FilterOperator.CONTAINS },
            { "ne", FilterOperator.DOES_NOT_EQUAL },
            { "gt", FilterOperator.GREATER_THAN },
            { "lt", FilterOperator.LESS_THAN },
            { "bw", FilterOperator.STARTS_WITH },
            { "ew", FilterOperator.ENDS_WITH },
        };
    }
}
