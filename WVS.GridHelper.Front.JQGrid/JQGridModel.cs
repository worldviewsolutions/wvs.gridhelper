﻿using System.Collections.Generic;
using System.Diagnostics.CodeAnalysis;
using WVS.GridHelper.TypeScript;

namespace WVS.GridHelper.Front.JQGrid
{
    [SuppressMessage("ReSharper", "InconsistentNaming")]
    [TypeScript]
    public class JQGridModel {
        public IEnumerable<string> colNames { get; set; }
        public IEnumerable<JQGridColModel> colModel { get; set; }
    }
}
