﻿using System;
using System.Collections.Generic;
using System.Linq;
using WVS.GridHelper;
using WVS.GridHelper.Front.JQGrid;

namespace DemoShared.DAL
{
    public class TestEntity
    {
        public int Id { get; set; }
        public string String { get; set; }
        public string LinkString { get; set; }
        public decimal? Decimal { get; set; }
        public int? Integer { get; set; }
        public DateTime? DateTime { get; set; }
        public bool? Boolean { get; set; }
        public bool? Boolean2 { get; set; }
        public string SelectString { get; set; }

        // Grid configuration.
        public static IJQGrid GridConfiguration => new JQGridBuild<TestEntity>()
            .Col(x => x.Id, new Column.Metadata {
                IsKey = true,
                Title = "ID #",
                Visible = true,
            })
            .Col(x => x.String, new Column.Metadata{Editable = true, IsSortable = false})
            .Col(x => x.LinkString, new Column.Metadata{Visible = false})
            .Col(x => x.Integer, new Column.Metadata{Editable = true})
            .Col(x => x.Boolean/*, new Column.Metadata{ UseYesNoFormat = true }*/)
            .Col(x => x.Boolean2, new Column.Metadata{ UseYesNoFormat = true })
            .Col(x => x.Decimal, new Column.Metadata { DecimalPlaces = 2, MaxValue = 200.5f, MinValue = -35.9f, Editable=true, IsRequired = true})
            .Col(x => x.DateTime, new Column.Metadata
            {
                CustomFormat = "m/d/y",
                Editable = true,
            })
            .Col(x => x.SelectString, new Column.Metadata { Editable = true, EditType = "select", EditOptions = "1:One;2:Two" })
            .Build();
    }

    public static class TestEntityFactory {
        public static IEnumerable<TestEntity> TestDataFactory() =>
            new List<TestEntity> {
                    new TestEntity {Id = 0, String = "zveroone", LinkString = "http://absoluteUrl", Decimal = 0, Integer = 0, DateTime = DateTime.UtcNow},
                    new TestEntity {Id = 1, String = "two", LinkString = "RelativeUrl"},
                    new TestEntity {Id = 7, String = "TWO", LinkString = "/RootPathUrl"},
                    new TestEntity {Id = 2, String = "three"},
                    new TestEntity {Id = 3, String = "four"},
                    new TestEntity {Id = 4, String = "five"},
                    new TestEntity {Id = 6, String = "FIVE"},
                }
               .Concat(Enumerable.Range(7, 231).Select(i => TestData()));

        private static TestEntity TestData(string @string = null, DateTime? dateTime = null, decimal? number = null) => new TestEntity
        {
            Id = testEntityId++,
            String = @string ?? $"string {testEntityId}",
            DateTime = dateTime ?? DateTime.Now.AddDays(1 - testEntityId),
            Decimal = number ?? testEntityId - 1,
            Integer = (int?)number ?? testEntityId - 1,
            Boolean = testEntityId % 2 == 0,
            Boolean2 = testEntityId % 2 == 0
        };
        private static int testEntityId = 5;
    }
}
