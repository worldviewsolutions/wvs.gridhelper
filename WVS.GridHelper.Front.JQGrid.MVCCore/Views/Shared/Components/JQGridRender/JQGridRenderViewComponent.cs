﻿using Microsoft.AspNetCore.Html;
using Microsoft.AspNetCore.Mvc;
using Microsoft.AspNetCore.Mvc.Razor;
using Microsoft.Extensions.DependencyInjection;
using Microsoft.Extensions.FileProviders;
using System.Reflection;
using System.Threading.Tasks;
using System.Web;

namespace WVS.GridHelper.Front.JQGrid.MVCCore
{
    [ViewComponent(Name = "JQGridRender")]
    public class JQGridRenderViewComponent : ViewComponent
    {
        public JQGridRenderViewComponent() {}
        public IViewComponentResult Invoke(IJQGrid gridConfiguration) =>
            View("JQGridRender", gridConfiguration);

        public static async Task<IHtmlContent> RenderAsync(IViewComponentHelper component, IJQGrid gridConfiguration) =>
            await component.InvokeAsync("JQGridRender", gridConfiguration);

        public static async Task<IHtmlContent> RenderJSAsync(Microsoft.AspNetCore.Mvc.Rendering.IHtmlHelper html) =>
            html.Raw(await Static.RawResource.GetJavascriptAsync());

        public static async Task<IHtmlContent> RenderCSSAsync(Microsoft.AspNetCore.Mvc.Rendering.IHtmlHelper html) =>
            html.Raw(await Static.RawResource.GetCSSAsync());

        public static void ConfigureProvider(IServiceCollection services) {
            var assembly = typeof(JQGridRenderViewComponent).GetTypeInfo().Assembly;
            services.Configure<RazorViewEngineOptions>(options =>
            {
                 options.FileProviders.Add(new EmbeddedFileProvider(assembly, "WVS.GridHelper.Front.JQGrid.MVCCore"));
            });
        }
    }
}
