﻿using Newtonsoft.Json;
using Microsoft.AspNetCore.Mvc;

namespace WVS.GridHelper.Front.JQGrid.MVCCore
{
    using Interfaces;

    public static class Extensions
    {
        public static string ToJQGridFormatJSON(this PagingModel @this, IGrid gridConfiguration)
        {
            var jqGridPagingModel = @this.ToJQGridFormat(gridConfiguration);
            var json = JsonConvert.SerializeObject(
                jqGridPagingModel,
                Formatting.None,
                new JsonSerializerSettings { NullValueHandling = NullValueHandling.Ignore });
            return json;
        }

        public static ActionResult ToJQGridFormatJSONResult(this PagingModel @this, IGrid gridConfiguration) =>
            new ContentResult {
                Content = @this.ToJQGridFormatJSON(gridConfiguration),
                ContentType = "application/json",
            };
    }
}
