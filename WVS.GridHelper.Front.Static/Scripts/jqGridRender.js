var __assign = (this && this.__assign) || function () {
    __assign = Object.assign || function(t) {
        for (var s, i = 1, n = arguments.length; i < n; i++) {
            s = arguments[i];
            for (var p in s) if (Object.prototype.hasOwnProperty.call(s, p))
                t[p] = s[p];
        }
        return t;
    };
    return __assign.apply(this, arguments);
};
var __rest = (this && this.__rest) || function (s, e) {
    var t = {};
    for (var p in s) if (Object.prototype.hasOwnProperty.call(s, p) && e.indexOf(p) < 0)
        t[p] = s[p];
    if (s != null && typeof Object.getOwnPropertySymbols === "function")
        for (var i = 0, p = Object.getOwnPropertySymbols(s); i < p.length; i++) if (e.indexOf(p[i]) < 0)
            t[p[i]] = s[p[i]];
    return t;
};
var JQGridRender;
(function (JQGridRender_1) {
    var JQGridRender = /** @class */ (function () {
        function JQGridRender(_a) {
            var gridMatchWidthId = _a.gridMatchWidthId, gridId = _a.gridId, pagerId = _a.pagerId, colData = _a.colData, actions = _a.actions, actionColumns = _a.actionColumns, gridKeyName = _a.gridKeyName, gridDataUrl = _a.gridDataUrl, gridDataMethod = _a.gridDataMethod, onRowSelect = _a.onRowSelect, hasSubGrid = _a.hasSubGrid, onSubGridRowExpanded = _a.onSubGridRowExpanded;
            this.$grid = $("#" + gridId);
            this.$pager = $("#" + pagerId);
            var $gridMatchWidth = gridMatchWidthId
                ? $("#" + gridMatchWidthId)
                : undefined;
            this.gridModel = {
                colNames: colData.colNames,
                colModel: toJQueryJqGridColModel(colData.colModel),
                url: gridDataUrl || undefined,
                mtype: gridDataMethod || undefined,
                datatype: gridDataUrl ? 'json' : 'local',
                width: $gridMatchWidth.width(),
                matchWidth: $gridMatchWidth,
                onSelectRow: function (rowId, status, e) {
                    if (!onRowSelect)
                        return;
                    if (typeof onRowSelect === 'function') {
                        var onRowSelectFn = onRowSelect;
                        onRowSelectFn(rowId, status, e);
                        return;
                    }
                    if (typeof onRowSelect === 'string') {
                        var fn = getFunction(onRowSelect);
                        if (typeof fn !== 'function')
                            return;
                        fn(rowId, status, e);
                    }
                },
                beforeSelectRow: function (rowId, e) {
                    // block selection if actions column was clicked.
                    var $self = $(this);
                    var iCol = $.jgrid.getCellIndex($(e.target).closest('td')[0]);
                    var gm = $self.jqGrid('getGridParam', 'colModel');
                    if (gm[iCol].name === 'actions')
                        return false;
                    return true; // TODO allow grid construction to supply a custom beforeSelectRow hook, call it here and return its result instead.
                },
                shrinkToFit: true,
                pager: this.$pager,
                height: 'auto',
                gridview: true,
                viewrecords: true,
                sortable: true,
                multiselect: false,
                subGrid: hasSubGrid,
                subGridRowExpanded: onSubGridRowExpanded ? function (subgrid_id, row_id) {
                    if (!onSubGridRowExpanded)
                        return;
                    if (typeof onSubGridRowExpanded === 'function') {
                        onSubGridRowExpanded(subgrid_id, row_id);
                        return;
                    }
                    if (typeof onSubGridRowExpanded === 'string') {
                        var fn = getFunction(onSubGridRowExpanded);
                        if (typeof fn !== 'function')
                            return;
                        fn(subgrid_id, row_id);
                    }
                } : undefined
            };
            window.jqGridHelperConfirmAction = function (callback, rowId, requireConfirmation, confirmText) {
                if (!callback || !$.isFunction(callback)) {
                    return;
                }
                // TODO replace confirm() with jq dialog.
                if (!requireConfirmation || !confirmText || confirm(confirmText)) {
                    callback(rowId);
                }
            };
            // ActionColumns are links that get their own column.
            function actionColFormatter(actionColumn, style) {
                return function (cellvalue, options, rowObject) {
                    var isArray = $.isArray(rowObject); // sometimes rowObject is an array and must use index, sometimes it is an object with named properties. wtf, i dunno. this code will return the first column if an array is found, and return row[actionkey] if it's an object.
                    var rowParam = rowObject[isArray ? 0 : gridKeyName];
                    if (!style) {
                        style = '';
                    }
                    var callbackFn = actionColumn.javascriptCallback
                        ? actionColumn.javascriptCallback + "(" + rowParam + ")"
                        : '';
                    var linkTitle = actionColumn.linkTitle;
                    var linkUrl;
                    if (actionColumn.urlColumnIndex !== undefined && actionColumn.urlColumnName) {
                        var linkParam = rowObject[isArray ? actionColumn.urlColumnIndex : actionColumn.urlColumnName];
                        if (linkParam !== null && linkParam !== undefined)
                            linkUrl = rowObject[isArray ? actionColumn.urlColumnIndex : actionColumn.urlColumnName];
                    }
                    else {
                        linkUrl = "javascript:" + callbackFn + ";";
                    }
                    var linkTarget = actionColumn.linkTarget
                        ? "target=\"" + actionColumn.linkTarget + "\""
                        : '';
                    return linkUrl
                        ? "<a href=\"" + linkUrl + "\" style=\"" + style + "\" " + linkTarget + ">" + stringDequote(linkTitle) + "</span></a>"
                        : '';
                };
            }
            if (actionColumns && actionColumns.length > 0) {
                for (var i = 0; i < actionColumns.length; i++) {
                    var col = actionColumns[i];
                    var formatterFunction = actionColFormatter(col, undefined);
                    var actionColgrid = {
                        name: col.title,
                        sortable: false,
                        editable: false,
                        formatter: formatterFunction
                    };
                    this.gridModel.colModel.push(actionColgrid);
                }
            }
            // Action column comes after ActionColumns.
            if (actions && actions.length) {
                var actionsColModel = {
                    name: 'actions',
                    sortable: false,
                    align: 'center',
                    editable: false,
                    width: actionsColModelWidth(actions),
                    fixed: true,
                    formatter: actionFormatterFn(actions, gridKeyName)
                };
                this.gridModel.colModel.push(actionsColModel);
            }
        }
        /**
         * instantiate jqGrid.
         */
        JQGridRender.prototype.init = function () {
            if (!this.$grid || !this.gridModel) {
                console.error('grid/model not found');
                return;
            }
            if (this.$grid.genericGrid && $.isFunction(this.$grid.genericGrid)) {
                this.$grid.genericGrid(this.gridModel);
                return;
            }
            if (this.$grid.jqGrid && $.isFunction(this.$grid.jqGrid)) {
                this.$grid.jqGrid(this.gridModel);
                return;
            }
            console.error('jqGrid not found');
        };
        return JQGridRender;
    }());
    JQGridRender_1.JQGridRender = JQGridRender;
    function actionFormatterFn(actions, gridKeyName) {
        // Render actions into cell. jqGrid takes a function which returns html.. which is just awesome.
        return function (cellvalue, options, rowObject) {
            var isArray = $.isArray(rowObject); // sometimes rowObject is an array and must use index, sometimes it is an object with named properties. wtf, i dunno. this code will return the first column if an array is found, and return row[actionkey] if it's an object.
            var rowParam = rowObject[isArray ? 0 : gridKeyName];
            var spacerHtml = '<div class="gridActionSpacer"></div>';
            function actionHtmlWrapper(action, condition, content) {
                if (!action)
                    return undefined;
                var innerHtml = actionInnerHtml();
                var style = '';
                // resolve conditional actions, look at another column's data to decide if this action should be available.
                if (action.conditionColumnName) {
                    style += (condition // hide action if conditional column is falsey.
                        ? '' // show.
                        : 'display: none;'); // hide.
                }
                if (content === undefined)
                    return wrapAction(innerHtml);
                var contentView = contentWrapper();
                if (action.contentAlignment === 3 /* Right */)
                    return wrapAction(innerHtml + contentView);
                return wrapAction(contentView + innerHtml);
                function wrapAction(inner) {
                    return "<div class=\"gridActionContainer\">" + link(inner) + "</div>";
                }
                function contentWrapper() {
                    return "<div class=\"" + contentWrapperClass() + "\">" + content + "</div>";
                }
                function contentWrapperClass() {
                    return 'gridActionContent ' + ((action.contentAlignment === 1 /* Left */ || action.contentAlignment === 3 /* Right */)
                        ? 'gridActionContentSide ' + (action.contentAlignment === 1 /* Left */
                            ? 'gridActionContentSideLeft'
                            : 'gridActionContentSideRight')
                        : 'gridActionContentOverlay');
                }
                function link(inner) {
                    return "<a href=\"javascript:" + callbackFn(action.javascriptCallback) + ";\" style=\"" + style + "\" title=\"" + stringDequote(action.title) + "\">" + inner + "</a>";
                }
                function actionInnerHtml() {
                    var icon = action.fontAwesomeIcon
                        ? "<i class=\"fa fa-lg fa-" + action.fontAwesomeIcon + "\"></i>"
                        : '';
                    return "<div class=\"gridAction\">" + icon + "</div>";
                }
                function callbackFn(callbackFnString) {
                    if (!callbackFnString)
                        return '';
                    return "jqGridHelperConfirmAction(" + callbackFnString + ", " + rowParam + ", " + action.useConfirmDialog + ", " + wrapConfirmText() + ")";
                    function wrapConfirmText() {
                        if (!action.confirmDialogText)
                            return '';
                        return "'" + action.confirmDialogText + "'";
                    }
                }
            }
            // render actions to HTML.
            var result = actions
                .map(function (action) {
                // resolve condition of another column's truthiness.
                var conditionVal = action.conditionColumnName
                    ? !!rowObject[isArray ? action.conditionColumnIndex : action.conditionColumnName]
                    : true;
                // resolve action 'content' transcluded from another column.
                var contentVal = action.contentColumnName
                    ? rowObject[isArray ? action.contentColumnIndex : action.contentColumnName]
                    : undefined;
                return actionHtmlWrapper(action, conditionVal, contentVal);
            })
                .reduce(function (previousValue, currentValue, currentIndex) {
                if (currentIndex)
                    return previousValue + spacerHtml + currentValue;
                return currentValue;
            }, '');
            return result; // return HTML to be inserted into action column by jqGrid.
        };
        /* results of above look kind of like this:
            var actionColFormatter = function (cellvalue, options, rowObject) {
            return '<a href="javascript:jqGridHelperConfirmAction(customcallbackfunctionname, ' + rowObject[0] + ', true, 'delete confirmation dialog msg');"><span style="cursor:pointer;"><i class="fa fa-lg fa-gear" title="View Details" style=""></i></span></a>' + '<span class="spacer"></span>'
                + '<a href="javascript:jqGridHelperConfirmAction(customcallbackfunctionname, ' + rowObject[0] + ', true, 'delete confirmation dialog msg');"><span style="cursor:pointer;"><i class="fa fa-lg fa-edit" title="Edit Plan BMP Details" style=""></i></span></a>' + '<span class="spacer"></span>'
                + '<a href="javascript:jqGridHelperConfirmAction(customcallbackfunctionname, ' + rowObject[0] + ', true, 'delete confirmation dialog msg');"><span style="cursor:pointer;"><i class="fa fa-lg fa-trash" title="Remove BMP" style=""></i></span></a>';
        }*/
    }
    function actionsColModelWidth(actions) {
        var actionsCount = actions.length;
        var buffer = 10 + 20 * actions
            .filter(function (a) { return a.contentColumnName
            && (a.contentAlignment === 1 /* Left */
                || a.contentAlignment === 3 /* Right */); })
            .length;
        return actionsCount * 35 + buffer;
    }
    function stringDequote(str) {
        return str
            .replace('\'', '')
            .replace('"', '');
    }
    function yesNoFormatter(x) {
        return function (cellvalue, options, rowdata, action) {
            if (undefined === cellvalue || null === cellvalue)
                return '';
            if (cellvalue)
                return 'Yes';
            return 'No';
        };
    }
    function numberFormatter(x) {
        var formatOptions = x.formatoptions;
        return function (cellvalue, options, rowdata, action) {
            if (undefined === cellvalue || null === cellvalue)
                return '';
            var precision = formatOptions && formatOptions.decimalPlaces
                ? formatOptions.decimalPlaces
                : 0;
            var number = parseFloat(cellvalue).toFixed(precision);
            return number.toLocaleString();
        };
    }
    function toJQueryJqGridColModel(colModels) {
        return colModels
            .map(function (col) {
            var f = !(col.formatter && col.formatter)
                ? undefined
                : col.formatter === 'number'
                    ? numberFormatter(col)
                    : col.formatter === 'yesno'
                        ? yesNoFormatter(col)
                        : col.formatter.toString();
              var formatter = col.formatter, rest = __rest(col, ["formatter"]);

              var cm = __assign({ formatter: f }, rest);

              //add datepicker for editable datetime field
              var dataInitFunction = function (el) { $(el).datepicker({ dateFormat: 'mm/dd/yy' }); };
              if (cm.editable)
                if (cm.formatter === 'date')
                  if (cm.editoptions)
                    cm.editoptions.dataInit = dataInitFunction;
                  else
                    cm.editoptions = ({ dataInit: dataInitFunction });
            return cm;
        });
    }
    function getFunction(fn) {
        var scopeSplit = fn.split('.');
        var scope = window;
        var i = 0;
        for (i = 0; i < scopeSplit.length - 1; i++) {
            scope = scope[scopeSplit[i]];
            if (scope === undefined)
                return undefined;
        }
        return scope[scopeSplit[scopeSplit.length - 1]];
    }
})(JQGridRender || (JQGridRender = {}));
//# sourceMappingURL=jqGridRender.js.map