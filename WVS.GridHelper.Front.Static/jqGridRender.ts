﻿import { Align } from './TypeScript/Models/Align';
import { JQGridModel } from './TypeScript/Models/JQGridModel';
import { JQGridColModel } from './TypeScript/Models/JQGridColModel';
import { JQGridActionModel } from './TypeScript/Models/JQGridActionModel';
import { JQGridActionColumnModel } from './TypeScript/Models/JQGridActionColumnModel';
import { JQGridColModelFormatOptions } from './TypeScript/Models/JQGridColModelFormatOptions';
import { DataMethodType } from './TypeScript/Models/DataMethodType';
import { JQGridRenderConfig } from './TypeScript/Models/JQGridRenderConfig';

namespace JQGridRender {

    export type IJQGridRenderModel = JQGridRenderConfig & {
        gridDataMethod: 'POST' | 'GET',
        onRowSelect: string | ((id: string, status: any, e: Event) => void),
        onSubGridRowExpanded: string | ((subgrid_id: any, row_id: any) => void),
    }

    export class JQGridRender {
        $grid: JQuery;
        $pager: JQuery;
        gridModel: JQueryJqGridOptions;

        constructor({
            gridMatchWidthId,
            gridId,
            pagerId,
            colData,
            actions,
            actionColumns,
            gridKeyName,
            gridDataUrl,
            gridDataMethod,
            onRowSelect,
            hasSubGrid,
            onSubGridRowExpanded
        }: IJQGridRenderModel) {
            this.$grid = $(`#${gridId}`);
            this.$pager = $(`#${pagerId}`);
            const $gridMatchWidth = gridMatchWidthId
                ? $(`#${gridMatchWidthId}`)
                : undefined;
               
            this.gridModel = {
                colNames: colData.colNames,
                colModel: toJQueryJqGridColModel(colData.colModel),
                url: gridDataUrl || undefined,
                mtype: gridDataMethod || undefined,
                datatype: gridDataUrl ? 'json' : 'local',
                width: $gridMatchWidth.width(),
                matchWidth: $gridMatchWidth,
                onSelectRow(rowId, status, e) {
                    if (!onRowSelect)
                        return;
                    if (typeof onRowSelect === 'function') {
                        const onRowSelectFn = ((onRowSelect as any) as ((number, string, { }) => void))
                        onRowSelectFn(rowId, status, e);
                        return;
                    }
                    if (typeof onRowSelect === 'string') {
                        const fn = getFunction(onRowSelect);
                        if (typeof fn !== 'function')
                            return;
                        fn(rowId, status, e);
                    }
                },
                beforeSelectRow: function (rowId, e) {
                    // block selection if actions column was clicked.
                    const $self = $(this);
                    const iCol = (<any>$).jgrid.getCellIndex($(e.target).closest('td')[0]);
                    const gm = $self.jqGrid('getGridParam', 'colModel');
                    if (gm[iCol].name === 'actions')
                        return false;
                    return true; // TODO allow grid construction to supply a custom beforeSelectRow hook, call it here and return its result instead.
                },
                shrinkToFit: true,
                pager: <any>this.$pager,
                height: 'auto',
                gridview: true,
                viewrecords: true,
                sortable: true,
                multiselect: false,
                subGrid: hasSubGrid,
                subGridRowExpanded: onSubGridRowExpanded ? function(subgrid_id, row_id) {
                    if (!onSubGridRowExpanded)
                        return;
                    if (typeof onSubGridRowExpanded === 'function') {
                        ((onSubGridRowExpanded as any) as ((number, string) => void))(subgrid_id, row_id);
                        return;
                    }
                    if (typeof onSubGridRowExpanded === 'string') {
                        const fn = getFunction(onSubGridRowExpanded);
                        if (typeof fn !== 'function')
                            return;
                        fn(subgrid_id, row_id);
                    }
                } : undefined
            };

            (window as any).jqGridHelperConfirmAction = (callback, rowId, requireConfirmation, confirmText) => {
                if (!callback || !$.isFunction(callback)) {
                    return;
                }
                // TODO replace confirm() with jq dialog.
                if (!requireConfirmation || !confirmText || confirm(confirmText)) {
                    callback(rowId);
                }
            }


            // ActionColumns are links that get their own column.
      function actionColFormatter(actionColumn: JQGridActionColumnModel, style) {
                return (cellvalue, options, rowObject) => {
                    const isArray = $.isArray(rowObject); // sometimes rowObject is an array and must use index, sometimes it is an object with named properties. wtf, i dunno. this code will return the first column if an array is found, and return row[actionkey] if it's an object.
                    const rowParam = rowObject[isArray ? 0 : gridKeyName];
          if (!style) {
                        style = '';
          }
          const callbackFn = actionColumn.javascriptCallback
            ? `${actionColumn.javascriptCallback}(${rowParam})`
            : '';
          const linkTitle = actionColumn.linkTitle;
          var linkUrl;
          if (actionColumn.urlColumnIndex !== undefined && actionColumn.urlColumnName) {
            const linkParam = rowObject[isArray ? actionColumn.urlColumnIndex : actionColumn.urlColumnName];
            if (linkParam !== null && linkParam !== undefined)
              linkUrl = rowObject[isArray ? actionColumn.urlColumnIndex : actionColumn.urlColumnName];
          } else {
            linkUrl = `javascript:${callbackFn};`;
          }
          const linkTarget = actionColumn.linkTarget
            ? `target="${actionColumn.linkTarget}"`
            : '';
          return linkUrl
            ? `<a href="${linkUrl}" style="${style}" ${linkTarget}>${stringDequote(linkTitle)}</span></a>`
            : '';
                }
            }

            if (actionColumns && actionColumns.length > 0) {
                for (let i = 0; i < actionColumns.length; i++) {
                    const col = actionColumns[i];
          const formatterFunction = actionColFormatter(col, undefined);
                    const actionColgrid = {
                        name: col.title
                        , sortable: false
                        , editable: false
                        , formatter: formatterFunction
                    };
                    this.gridModel.colModel.push(actionColgrid);
                }
            }


            // Action column comes after ActionColumns.
            if (actions && actions.length) {
                const actionsColModel: JQueryJqGridColumn = {
                    name: 'actions'
                    , sortable: false
                    , align: 'center'
                    , editable: false
                    , width: actionsColModelWidth(actions)
                    , fixed: true
                    , formatter: actionFormatterFn(actions, gridKeyName)
                };
                this.gridModel.colModel.push(actionsColModel);
            }
        }

        /**
         * instantiate jqGrid.
         */
        init() {
            if (!this.$grid || !this.gridModel) {
                console.error('grid/model not found');
                return;
            }
            if (this.$grid.genericGrid && $.isFunction(this.$grid.genericGrid)) {
                this.$grid.genericGrid(this.gridModel);
                return;
            }
            if (this.$grid.jqGrid && $.isFunction(this.$grid.jqGrid)) {
                this.$grid.jqGrid(this.gridModel);
                return;
            }
            console.error('jqGrid not found');
        }
    }

    function actionFormatterFn(actions: JQGridActionModel[], gridKeyName: string): ((cellvalue: any, options: { rowId: any, colModel: any }, rowObject: any) => any) {
        // Render actions into cell. jqGrid takes a function which returns html.. which is just awesome.
        return (cellvalue, options, rowObject) => {
            var isArray = $.isArray(rowObject); // sometimes rowObject is an array and must use index, sometimes it is an object with named properties. wtf, i dunno. this code will return the first column if an array is found, and return row[actionkey] if it's an object.
            var rowParam = rowObject[isArray ? 0 : gridKeyName];
            var spacerHtml = '<div class="gridActionSpacer"></div>';
            function actionHtmlWrapper(action: JQGridActionModel, condition, content) {
                if (!action)
                    return undefined;
                const innerHtml = actionInnerHtml();

                var style = '';
                // resolve conditional actions, look at another column's data to decide if this action should be available.
                if (action.conditionColumnName) {
                    style += (condition // hide action if conditional column is falsey.
                        ? '' // show.
                        : 'display: none;'); // hide.
                }

                if (content === undefined)
                    return wrapAction(innerHtml);

                const contentView = contentWrapper();

                if (action.contentAlignment === Align.Right)
                    return wrapAction(innerHtml + contentView);

                return wrapAction(contentView + innerHtml);

                function wrapAction(inner) {
                    return `<div class="gridActionContainer">${link(inner)}</div>`;
                }
                function contentWrapper() {
                    return `<div class="${contentWrapperClass()}">${content}</div>`;
                }
                function contentWrapperClass() {
                    return 'gridActionContent ' + ((action.contentAlignment === Align.Left || action.contentAlignment === Align.Right)
                        ? 'gridActionContentSide ' + (action.contentAlignment === Align.Left
                            ? 'gridActionContentSideLeft'
                            : 'gridActionContentSideRight')
                        : 'gridActionContentOverlay');
                }
                function link(inner) {
                    return `<a href="javascript:${callbackFn(action.javascriptCallback)};" style="${style}" title="${stringDequote(action.title)}">${inner}</a>`;
                }
                function actionInnerHtml() {
                    var icon = action.fontAwesomeIcon
                        ? `<i class="fa fa-lg fa-${action.fontAwesomeIcon}"></i>`
                        : '';
                    return `<div class="gridAction">${icon}</div>`;
                }
                function callbackFn(callbackFnString: string) {
                    if (!callbackFnString)
                        return '';
                    return `jqGridHelperConfirmAction(${callbackFnString}, ${rowParam}, ${action.useConfirmDialog}, ${wrapConfirmText()})`;
                    function wrapConfirmText() {
                        if (!action.confirmDialogText)
                            return '';
                        return `'${action.confirmDialogText}'`;
                    }
                }
            }
            // render actions to HTML.
            const result = actions
                .map(action => {

                    // resolve condition of another column's truthiness.
                    var conditionVal = action.conditionColumnName
                        ? !!rowObject[isArray ? action.conditionColumnIndex : action.conditionColumnName]
                        : true;

                    // resolve action 'content' transcluded from another column.
                    var contentVal = action.contentColumnName
                        ? rowObject[isArray ? action.contentColumnIndex : action.contentColumnName]
                        : undefined;

                    return actionHtmlWrapper(
                        action,
                        conditionVal,
                        contentVal);
                })
                .reduce((previousValue, currentValue, currentIndex) => {
                    if (currentIndex)
                        return previousValue + spacerHtml + currentValue;
                    return currentValue;
                }, '');

            return result; // return HTML to be inserted into action column by jqGrid.
        };
        /* results of above look kind of like this:
            var actionColFormatter = function (cellvalue, options, rowObject) {
            return '<a href="javascript:jqGridHelperConfirmAction(customcallbackfunctionname, ' + rowObject[0] + ', true, 'delete confirmation dialog msg');"><span style="cursor:pointer;"><i class="fa fa-lg fa-gear" title="View Details" style=""></i></span></a>' + '<span class="spacer"></span>'
                + '<a href="javascript:jqGridHelperConfirmAction(customcallbackfunctionname, ' + rowObject[0] + ', true, 'delete confirmation dialog msg');"><span style="cursor:pointer;"><i class="fa fa-lg fa-edit" title="Edit Plan BMP Details" style=""></i></span></a>' + '<span class="spacer"></span>'
                + '<a href="javascript:jqGridHelperConfirmAction(customcallbackfunctionname, ' + rowObject[0] + ', true, 'delete confirmation dialog msg');"><span style="cursor:pointer;"><i class="fa fa-lg fa-trash" title="Remove BMP" style=""></i></span></a>';
        }*/
    }

    function actionsColModelWidth(actions: JQGridActionModel[]) {
        const actionsCount = actions.length;
        const buffer = 10 + 20 * actions
            .filter(a => a.contentColumnName
                && (a.contentAlignment === Align.Left
                    || a.contentAlignment === Align.Right))
            .length;
        return actionsCount * 35 + buffer;
    }

    function stringDequote(str) {
        return str
            .replace('\'', '')
            .replace('"', '');
    }

    function yesNoFormatter(x: JQGridColModel) {
        return (cellvalue, options, rowdata, action) => {
            if (undefined === cellvalue || null === cellvalue)
                return '';
            if (cellvalue)
                return 'Yes';
            return 'No';
        };
    }

    function numberFormatter(x: JQGridColModel) {
        const formatOptions = x.formatoptions;
        return (cellvalue, options, rowdata, action) => {
            if (undefined === cellvalue || null === cellvalue)
                return '';
            const precision = formatOptions && formatOptions.decimalPlaces
                ? formatOptions.decimalPlaces
                : 0;
            var number = parseFloat(cellvalue).toFixed(precision);
            return number.toLocaleString();
        }
    }

    function toJQueryJqGridColModel(colModels: JQGridColModel[]): JQueryJqGridColumn[] {
        return colModels
            .map(col => {
                const f = !(col.formatter && col.formatter as string)
                  ? undefined
                  : col.formatter === 'number'
                    ? numberFormatter(col)
                    : col.formatter === 'yesno'
                      ? yesNoFormatter(col)
                      : col.formatter.toString()
                    ;
                const { formatter, ...rest } = col;
                const cm = {
                    formatter: f,
                    ...rest
                };
                return cm as JQueryJqGridColumn;
            });
    }

    function getFunction(fn: string) {
        const scopeSplit = fn.split('.');
        let scope = window;
        let i = 0;
        for (i = 0; i < scopeSplit.length - 1; i++) {
            scope = scope[scopeSplit[i]];
            if (scope === undefined)
                return undefined;
        }
        return scope[scopeSplit[scopeSplit.length - 1]];
    }
}
