﻿using System.IO;
using System.Reflection;
using System.Threading.Tasks;

namespace WVS.GridHelper.Front.Static
{
    public static class RawResource
    {
        private const string JSPath = "WVS.GridHelper.Front.Static.Scripts.jqGridRender.js";
        public static string GetJavascript() => GetResource(JSPath);
        public static async Task<string> GetJavascriptAsync() => await GetResourceAsync(JSPath);

        private const string CSSPath = "WVS.GridHelper.Front.Static.Content.JQGrid.css";
        public static string GetCSS() => GetResource(CSSPath);
        public static async Task<string> GetCSSAsync() => await GetResourceAsync(CSSPath);

        private static string GetResource(string resourceName)
        {
            using (var s = GetResourceStream(resourceName))
            using (var r = new StreamReader(s))
            {
                s.Position = 0;
                return r.ReadToEnd();
            }
        }

        public static async Task<string> GetResourceAsync(string resourceName)
        {
            using (var s = GetResourceStream(resourceName))
            using (var r = new StreamReader(s))
            {
                s.Position = 0;
                return await r.ReadToEndAsync();
            }
        }

        private static Stream GetResourceStream(string resourceName) =>
            typeof(RawResource).GetTypeInfo().Assembly.GetManifestResourceStream(resourceName) ?? new MemoryStream();
    }
}
