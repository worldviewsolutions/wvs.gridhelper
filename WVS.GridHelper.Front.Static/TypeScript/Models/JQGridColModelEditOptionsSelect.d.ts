﻿import { JQGridColModelEditOptions } from './JQGridColModelEditOptions';
export interface JQGridColModelEditOptionsSelect extends JQGridColModelEditOptions {
    value?: string;
}
