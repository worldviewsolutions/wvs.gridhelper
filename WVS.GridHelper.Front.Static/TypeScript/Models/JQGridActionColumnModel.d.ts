﻿
export interface JQGridActionColumnModel {
    title?: string;
    linkTitle?: string;
    linkTarget?: string;
    javascriptCallback?: string;
    urlColumnName?: string;
    urlColumnIndex?: number;
    alignment?: string;
    urlFormat?: string;
}
