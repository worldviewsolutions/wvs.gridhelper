﻿import { any } from './any';
export interface JQGridPagingModel {
    page: number;
    records: number;
    total: number;
    rows?: any[];
}
