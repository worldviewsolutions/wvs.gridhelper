﻿
export interface JQGridActionModel {
    title?: string;
    fontAwesomeIcon?: string;
    javascriptCallback?: string;
    useConfirmDialog: boolean;
    confirmDialogText?: string;
    conditionColumnName?: string;
    conditionColumnIndex?: number;
    contentColumnName?: string;
    contentColumnIndex?: number;
    contentAlignment?: number;
}
