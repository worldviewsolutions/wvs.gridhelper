﻿import { JQGridModel } from './JQGridModel';
import { JQGridActionModel } from './JQGridActionModel';
import { JQGridActionColumnModel } from './JQGridActionColumnModel';import { JQGridModel } from './JQGridModel';import { JQGridActionModel } from './JQGridActionModel';import { JQGridActionColumnModel } from './JQGridActionColumnModel';
export interface JQGridRenderConfig {
    gridMatchWidthId?: string;
    gridId?: string;
    pagerId?: string;
    colData?: JQGridModel;
    actions?: JQGridActionModel[];
    actionColumns?: JQGridActionColumnModel[];
    gridKeyName?: string;
    gridDataUrl?: string;
    gridDataMethod?: string;
    onRowSelect?: string;
    hasSubGrid: boolean;
    onSubGridRowExpanded?: string;
}
