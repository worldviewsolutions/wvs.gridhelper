﻿${// More info: http://frhagn.github.io/Typewriter/
    using Typewriter.Extensions.Types;
    using static System.Char;

    Template(Settings settings)
    {
        settings.OutputExtension = ".d.ts";
    }

    bool PropertyIsRequired(Property p) => p.Attributes.Any(a => a.name.Equals("required"));

    string NormalizedName(Type type) => NormalizedName(SemanticName(type.Name));

    string NormalizedName(string n) =>
        n.Substring(0, n.LastIndexOf('.') != -1 ? n.LastIndexOf('.') : n.Length)
        .Select((c, i) => (c: c, i: i))
        .Aggregate(string.Empty, (name, ci) => 
        {
            var (c, i) = ci;
            return $"{name}{(i != 0 && IsUpper(c) ? "-" : "")}{ToLower(c)}";
        });

    string RequestModelSuffix(Class @class) => "Request";

    IEnumerable<Type> PropertyTypes(Class @class) => 
        from property in @class.Properties 
        let type = property.Type
        let unwrappedType = 
            type.IsGeneric && type.TypeArguments.Count == 1
            ? type.Unwrap()
            : type.ToString().Contains("[key: string]")
            ? property.Type.TypeArguments.Last()
            : type
        where unwrappedType?.IsPrimitive == false
            && !unwrappedType.name.Equals("any")
        group unwrappedType by unwrappedType.name into g select g.First();

    string PropertyTypeImplementation(Property property, string mappedTypeName = null, string objectTypeSuffix = null) => SemanticName(
        property.Type.ToString().Contains("[key: string]")
        ? $"{(mappedTypeName != null ? mappedTypeName + "<" : "")}{{[id: string]: {property.Type.TypeArguments.LastOrDefault().Name}}}{(mappedTypeName != null ? ">" :"")}"
        : property.Type.IsGeneric
        ? $"{(mappedTypeName != null ? "Array<" + mappedTypeName + "<" : "")}{property.Type.TypeArguments.First().Name}{(objectTypeSuffix != null ? "." + objectTypeSuffix : "")}{(mappedTypeName != null ? ">" :"")}{(mappedTypeName != null ? ">" : "[]")}"
        : !property.Type.IsPrimitive
        ? $"{property.Type.Name}"
        : property.Type.Name
    );

    string PropertyTypeName(Property property) => PropertyTypeImplementation(property);

    string PartialMappedPropertyTypeName(Property property) => PropertyTypeImplementation(
        property: property, 
        mappedTypeName: "Partial",
        objectTypeSuffix: "Request"
        );

    string SemanticName(Class @class) => SemanticName(@class.Name);
    
    string SemanticName(Type type) => SemanticName(type.Name);

    string SemanticName(string name) => name
        .Replace("Date", "string | Date");

    string Optionality(Property property) => false
        || (property.Type.name.Equals("string") && !PropertyIsRequired(property))
        || property.Type.IsNullable
        || (!property.Type.IsPrimitive && !PropertyIsRequired(property))
        ? "?"
        : "";

    bool HasImports(Class @class) => PropertyTypes(@class).Any();

	string Inherit(Class c)
	{
	if (c.BaseClass!=null)
		return " extends " + c.BaseClass.ToString();
	  else
		 return  "";
	}

	string Imports(Class c){
		List<string> neededImports = c.Properties
			.Where(p => !p.Type.IsPrimitive)
			.Select(p => "import { " + p.Type.Name.TrimEnd('[',']') + " } from './" + p.Type.Name.TrimEnd('[',']') + "';")
			.ToList();
		if (c.BaseClass != null) { 
			neededImports.Add("import { " + c.BaseClass.Name +" } from './" + c.BaseClass.Name + "';");
		}
		return String.Join("\n", neededImports.Distinct());
	}

    // TypeScript is TypeScriptAttribute provided by WVS.GridHelper. Stick this attribute on a class to generate d.ts

}$Classes([TypeScript])[$Imports$PropertyTypes[import { $SemanticName } from './$SemanticName';]
export interface $SemanticName$Inherit {$Properties[
    $Name$Optionality: $Type;]
}]$Enums([TypeScript])[export const enum $Name {
    $Values[$Name = $Value][,
    ]
}
]
