﻿import { JQGridColModelEditOptions } from './JQGridColModelEditOptions';
export interface JQGridColModelEditOptionsText extends JQGridColModelEditOptions {
    size?: number;
    maxlength?: number;
}
