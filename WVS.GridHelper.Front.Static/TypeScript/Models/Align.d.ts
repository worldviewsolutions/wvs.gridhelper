﻿export const enum Align {
    Unspecified = 0,
    Left = 1,
    Center = 2,
    Right = 3
}

