﻿import { JQGridColModelEditOptions } from './JQGridColModelEditOptions';
import { JQGridColModelFormatOptions } from './JQGridColModelFormatOptions';import { JQGridColModelEditOptions } from './JQGridColModelEditOptions';import { JQGridColModelFormatOptions } from './JQGridColModelFormatOptions';
export interface JQGridColModel {
    name: string;
    sortable?: boolean;
    editable?: boolean;
    editoptions?: JQGridColModelEditOptions;
    index?: string;
    width?: number;
    fixed?: boolean;
    hidden?: boolean;
    align?: string;
    formatter?: string;
    key?: boolean;
    formatoptions?: JQGridColModelFormatOptions;
    defaultvalue?: string;
}
