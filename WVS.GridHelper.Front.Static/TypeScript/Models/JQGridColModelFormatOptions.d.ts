﻿
export interface JQGridColModelFormatOptions {
    decimalPlaces?: number;
    srcformat?: string;
    newformat?: string;
}
