﻿/// <reference path="../Models/JQGridColModel.d.ts"/>
/// <reference path="jquery/jquery.d.ts" />
/// <reference path="jqgrid/jqgrid.d.ts" />
export { };
declare global {
  interface JQGridColModel {
    formatter: string; // mixed: can be text or a function.
    //cellattr: any; // function.
  }

  interface JQueryJqGridOptions {
    matchWidth: string | JQuery;
    beforeSelectRow: any;
      sortable: boolean;
      subGrid: boolean;
      subGridRowExpanded: ((subgrid_id: any, row_id: any) => void)
  }

  interface JQueryJqGridColumn {
      fixed?: boolean;
      edittype?: 'text' | 'textarea' | 'select' | 'checkbox' | 'password' | 'button' | 'image' | 'file' | 'custom';
      editoptions?: {
          size?: number,
          maxlength?: number,
          rows?: number,
          cols?: number,
          value?: string | {
              [name: string]: string;
          }
      };
  }

  interface JQuery {
    genericGrid(gridModel: JQueryJqGridOptions);
  }
}
