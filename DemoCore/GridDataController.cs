﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Mvc;
using DemoShared.DAL;
using WVS.GridHelper;
using WVS.GridHelper.Interfaces;
using WVS.GridHelper.Back.EFCore;
using WVS.GridHelper.Front.JQGrid;
using WVS.GridHelper.Front.JQGrid.MVCCore;

// For more information on enabling Web API for empty projects, visit https://go.microsoft.com/fwlink/?LinkID=397860

namespace DemoCore.Back
{
    [Route("api/[controller]")]
    public class GridDataController : Controller
    {
        // Grid Configuration.
        private static IJQGrid GridFactory() =>
            TestEntity.GridConfiguration;

        // Data controller.
        [HttpPost]
        public async Task<ActionResult> SearchTestEntities(GridQuery query) =>
            await GetTestEntitiesGridDataAsync(query);

        // Manager
        private async Task<ActionResult> GetTestEntitiesGridDataAsync(GridQuery query) =>
            await GetGridDataAsync(GridFactory(), TestEntities.AsQueryable(), query.Sidx ?? nameof(TestEntity.Id), query.Sord, query.Rows, query.Page);


        // Shared
        public class GridQuery
        {
            public string Sidx { get; set; }
            public string Sord { get; set; } = "asc";
            public int Rows { get; set; } = 25;
            public int Page { get; set; } = 1;
        }

        // Translate odata into GetGridData. Query to VM.
        private async Task<ActionResult> GetGridDataAsync<T>(IGrid gridConfiguration, IQueryable<T> query, string sidx, string sord = "asc", int rows = 25, int page = 1, IFilterList filters = null)
            where T : class =>
                (await GetGridDataAsync(gridConfiguration, query, sidx, sord.StartsWith("asc", StringComparison.OrdinalIgnoreCase), rows, page, filters))
                .ToJQGridFormatJSONResult(gridConfiguration);

        // Data layer.
        private async Task<PagingModel> GetGridDataAsync<T>
            (IGrid gridConfiguration, IQueryable<T> query, string orderBy, bool orderAscending = true, int rowsPerPage = 25, int page = 1, IFilterList filters = null)
            where T : class =>
                await
                new LinqFilter().ApplyFilters(query, filters)
                .GetGridDataAsync(
                    gridConfiguration,
                    orderBy,
                    orderAscending,
                    rowsPerPage: rowsPerPage < 1 ? 1 : rowsPerPage > 1000 ? 1000 : rowsPerPage,
                    page: page < 1 ? 1 : page,
                    filters: filters);

        // Database.
        private static IEnumerable<TestEntity> TestEntities =>
            TestEntityFactory.TestDataFactory();
    }
}
