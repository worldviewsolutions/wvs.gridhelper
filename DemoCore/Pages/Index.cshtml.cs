﻿using Microsoft.AspNetCore.Mvc.RazorPages;
using Microsoft.AspNetCore.Mvc;
using WVS.GridHelper.Front.JQGrid;
using DemoShared.DAL;
using WVS.GridHelper.Interfaces;
using WVS.GridHelper.Tests;

namespace DemoCore.Pages
{
    public class GridModel : PageModel
    {
        // ViewModel.
        public IJQGrid Grid { get; } = TestEntity.GridConfiguration;

        // View controller.
        public void OnGet() {
            Grid.DataUrl = Url.Action("SearchTestEntities", "GridData");
            Grid.DataMethod = DataMethodType.Post;
        }
    }
}
