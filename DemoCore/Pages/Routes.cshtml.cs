﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Mvc.RazorPages;
using Microsoft.AspNetCore.Mvc.Infrastructure;
using Newtonsoft.Json;
using Microsoft.AspNetCore.Mvc.ActionConstraints;
using Microsoft.AspNetCore.Mvc.Abstractions;

namespace DemoCore.Pages
{
    public class RoutesModel : PageModel
    {
        private readonly IActionDescriptorCollectionProvider _actionDescriptorCollectionProvider;

        public RoutesModel(IActionDescriptorCollectionProvider actionDescriptorCollectionProvider)
        {
            this._actionDescriptorCollectionProvider = actionDescriptorCollectionProvider;
        }

        public List<RouteInfo> Routes { get; set; }

        public void OnGet()
        {
            const string page = "page";
            const string action = "Action";
            const string controller = "Controller";
            var routes = _actionDescriptorCollectionProvider.ActionDescriptors.Items;
            Routes = routes.Select(x => new RouteInfo {
                Page = TryGet(x, page),
                Action = TryGet(x, action),
                Controller = TryGet(x, controller),
                Name = x.AttributeRouteInfo.Name,
                Template = x.AttributeRouteInfo.Template,
                Constraint = x.ActionConstraints == null ? "" : JsonConvert.SerializeObject(x.ActionConstraints)
            })
                .OrderBy(r => r.Template)
                .ToList();

            string TryGet(ActionDescriptor route, string key) =>
                route.RouteValues.ContainsKey(key)
                    ? route.RouteValues[key]
                    : null;

        }

        public class RouteInfo
        {
            public string Page { get; set; }
            public string Template { get; set; }
            public string Name { get; set; }
            public string Controller { get; set; }
            public string Action { get; set; }
            public string Constraint { get; set; }
        }
    }
}
