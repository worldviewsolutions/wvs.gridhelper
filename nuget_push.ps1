#########
# NuGet-Push
## Push packages to a nuget server.
## Skips silently if package exists (409 CONFLICT).
#########
# TeamCity config:
## -targetPath:%teamcity.build.checkoutDir%
## -source:%teamcity.nuget.feed.server%
## -apiKey:%teamcity.nuget.feed.api.key%
## -nugetToolPath:%teamcity.tool.NuGet.CommandLine.DEFAULT%
## -pattern:"^WVS.GridHelper(?:\..+){0,1}.[\d]+.[\d]+.[\d]+(?:-.+){0,1}(?:.symbols){0,1}.nupkg$"

#Requires -Version 3 # Require minimum PowerShell version 3.0. Not actually a comment, this is enforced by PS.

param(
	[Parameter(Mandatory=$true)][string]$targetPath,
	[Parameter(Mandatory=$true)][string]$source,
	[Parameter(Mandatory=$true)][string]$apiKey,
	[string]$nugetToolPath,
	[string]$pattern = "^.+.nupkg$")

$global:nugetExePath = $null

function main {
	logMessage("TargetPath: " + $targetPath)
	logMessage("Pattern: " + $pattern)
	logMessage("Source: " + $source)
	findNuget
	pushFolder
}

function pushSingle {
	param([System.IO.FileInfo]$nupkg)

	# check if exists on source
	logMessage("Checking " + $nupkg.Name)
	$latestrelease = &($global:nugetExePath) list $nupkg.Name -source $source | Select-String -Pattern $package.Name

	if(!$latestrelease) {
		$srvpkg = $null
	} else {
		$split = $latestrelease.Line.split(" ")
		$srvpkg = $split[0] + "." + $split[1] + ".nupkg"
	}
	if($srvpkg -eq $nupkg.Name) {
		logMessage("Skipped " + $nupkg.Name)
		return;
	}

	# push
	try {
		&($global:nugetExePath) push $nupkg.FullName -source $source -ApiKey $apiKey
	} catch {
		if ($_.Exception.Message -contains "409") {
			logMessage("Precheck should have caught this. Is it easier just to try and catch anyway and lose the pre-flight check?")
			logMessage("Skipped " + $nupkg.Name)
			return
		}
		throw
	}
	logMessage("Pushed " + $nupkg.Name)
}

function pushMany {
	param([System.IO.FileInfo[]]$packages)
	foreach ($package in $packages) {
		pushSingle $package
	}
}

function pushFolder {
	logMessage "Pushing nugets in $targetPath"

	# get files matching nuget pattern.
	$files = gci -File -Path $targetPath | ? -FilterScript {$_.Name -match $pattern}
	if (!$files -or $files.Length -eq 0) {
		errorMessage "No nupkgs matched."
		exit
	}
	logMessage "Matched nupkgs: $files"
	pushMany $files
}

function findNuget {
	if ($global:nugetExePath) {
		return
	}
	$nugetExePath = $nugetToolPath + "\tools\NuGet.exe"
	if ((Get-Command $nugetExePath -ErrorAction SilentlyContinue) -ne $null) {
		$global:nugetExePath = $nugetExePath
	} elseif ((Get-Command "NuGet.exe" -ErrorAction SilentlyContinue) -ne $null) {
		$global:nugetExePath = "NuGet.exe"
	} elseif ((Get-Command ".\NuGet.exe" -ErrorAction SilentlyContinue) -ne $null) {
		$global:nugetExePath = ".\NuGet.exe"
	} else {
		errorMessage("Cannot find nuget, looked in . and $nugetExePath")
		exit
	}

	logMessage("Using NuGet found at " + $global:nugetExePath)
}

function errorMessage {
	param([Parameter(Mandatory=1)][string]$msg)
	if (!$msg) {
		logMessage("Empty error message")
		return
	}
	logMessage -msg $msg -status "ERROR"
}

function logMessage { param(
	[Parameter(Mandatory=$true, Position=0)][string]$msg,
	[Parameter(Mandatory=$false, Position=1)][string]$status = "NORMAL")
	#Write-Host $msg # write to console if running manually

	Write-Host "##teamcity[message text='$msg' status='$status']"
	##teamcity[message text='<message text>' errorDetails='<error details>' status='<status value>']
	# The status attribute may take following values: NORMAL, WARNING, FAILURE, ERROR. The default value is NORMAL. This message fails the build in case its status is ERROR.
	# The errorDetails attribute is used only if status is ERROR, in other cases it is ignored.
}

main
